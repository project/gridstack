<?php

namespace Drupal\Tests\gridstack\Traits;

/**
 * A Trait common for GridStack tests.
 */
trait GridStackKernelTrait {

  /**
   * The gridstack admin service.
   *
   * @var \Drupal\gridstack\Form\GridStackAdminInterface
   */
  protected $gridstackAdmin;

  /**
   * The gridstack formatter service.
   *
   * @var \Drupal\gridstack\GridStackFormatterInterface
   */
  protected $gridstackFormatter;

  /**
   * The gridstack manager service.
   *
   * @var \Drupal\gridstack\GridStackManagerInterface
   */
  protected $gridstackManager;

  /**
   * The gridstack settings form.
   *
   * @var \Drupal\gridstack_ui\Form\GridStackForm
   */
  protected $gridstackForm;

  /**
   * The gridstack manager service.
   *
   * @var \Drupal\gridstack\Skin\GridStackSkinManagerInterface
   */
  protected $gridstackSkinManager;

}
