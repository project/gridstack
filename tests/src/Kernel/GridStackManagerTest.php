<?php

namespace Drupal\Tests\gridstack\Kernel;

use Drupal\Tests\blazy\Kernel\BlazyKernelTestBase;
use Drupal\Tests\gridstack\Traits\GridStackKernelTrait;
use Drupal\Tests\gridstack\Traits\GridStackUnitTestTrait;
use Drupal\gridstack\Entity\GridStack;
use Drupal\gridstack\GridStackDefault;
use Drupal\gridstack_ui\Form\GridStackForm;

/**
 * Tests the GridStack manager methods.
 *
 * @coversDefaultClass \Drupal\gridstack\GridStackManager
 *
 * @group gridstack
 */
class GridStackManagerTest extends BlazyKernelTestBase {

  use GridStackUnitTestTrait;
  use GridStackKernelTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'system',
    'user',
    'field',
    'file',
    'filter',
    'image',
    'media',
    'node',
    'text',
    'blazy',
    'gridstack',
    'gridstack_ui',
    'gridstack_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installConfig([
      'field',
      'node',
      'views',
      'blazy',
      'gridstack',
    ]);

    $bundle = $this->bundle;

    $this->fileSystem          = $this->container->get('file_system');
    $this->messenger           = $this->container->get('messenger');
    $this->gridstackAdmin      = $this->container->get('gridstack.admin');
    $this->blazyAdminFormatter = $this->gridstackAdmin;
    $this->gridstackFormatter  = $this->container->get('gridstack.formatter');
    $this->gridstackManager    = $this->container->get('gridstack.manager');

    $this->gridstackForm = GridStackForm::create($this->container);

    $this->testPluginId  = 'gridstack_image';
    $this->testFieldName = 'field_gridstack_image';
    $this->maxItems      = 7;
    $this->maxParagraphs = 2;

    $settings['fields']['field_text_multiple'] = 'text';
    $this->setUpContentTypeTest($bundle, $settings);
    $this->setUpContentWithItems($bundle);
    $this->setUpRealImage();

    $this->display = $this->setUpFormatterDisplay($bundle);
    $this->formatterInstance = $this->getFormatterInstance();

    // Enable Boostrap support.
    $this->gridstackManager->configFactory()->getEditable('gridstack.settings')->set('framework', 'bootstrap')->save();
  }

  /**
   * Tests cases for various methods.
   *
   * @covers ::attach
   */
  public function testGridStackManagerMethods() {
    $manager = $this->gridstackManager;

    $settings = [
      // 'use_js'      => TRUE,
      'skin'        => 'selena',
      'width'       => 11,
      'breakpoints' => ['lg' => ['column' => 11]],
      // @todo '_engine'     => 'gridstack_js',
    ] + $this->getFormatterSettings();

    // The gridstack is moved into plugin, it is no longer loaded by default.
    $attachments = $manager->attach($settings);
    $this->assertArrayNotHasKey('gridstack', $attachments['drupalSettings']);
  }

  /**
   * Tests for GridStack build.
   *
   * @param bool $items
   *   Whether to provide items, or not.
   * @param array $settings
   *   The settings being tested.
   * @param mixed|bool|string $expected
   *   The expected output.
   *
   * @covers ::build
   * @covers ::preRenderGridStack
   * @dataProvider providerTestGridStackBuild
   */
  public function testBuild($items, array $settings, $expected) {
    $manager = $this->gridstackManager;
    $defaults = $this->getFormatterSettings() + GridStackDefault::htmlSettings();
    $settings = array_merge($defaults, $settings) + GridStackDefault::imageSettings();

    $settings['optionset'] = 'test';

    $build = $this->display->build($this->entity);

    $items = !$items ? [] : $build[$this->testFieldName]['#build']['items'];
    $build = [
      'items'     => $items,
      '#settings'  => $settings,
      '#optionset' => GridStack::loadSafely($settings['optionset']),
    ];

    $gridstack = $manager->build($build);
    $this->assertEquals($expected, !empty($gridstack));
  }

  /**
   * Provide test cases for ::testBuild().
   *
   * @return array
   *   An array of tested data.
   */
  public static function providerTestGridStackBuild() {
    $data[] = [
      FALSE,
      [],
      FALSE,
    ];
    $data[] = [
      TRUE,
      [
        'skin' => 'selena',
      ],
      TRUE,
    ];

    return $data;
  }

}
