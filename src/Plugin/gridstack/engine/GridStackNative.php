<?php

namespace Drupal\gridstack\Plugin\gridstack\engine;

/**
 * Provides a GridStack native CSS Grid layout engine.
 *
 * @GridStackEngine(
 *   id = "gridstack_native",
 *   group = "gridstack",
 *   hidden = "true",
 *   version = "2",
 *   label = @Translation("GridStack native CSS Grid")
 * )
 */
class GridStackNative extends GridStackBase {

  /**
   * Native CSS Grid layout doesn't need DOM rect positions, just dimensions.
   *
   * {@inheritdoc}
   */
  protected $nodes = ['w', 'h', 'region'];

  /**
   * {@inheritdoc}
   */
  protected $containerClasses = ['gridstack--native'];

  /**
   * {@inheritdoc}
   */
  public function attach(array &$load, array $attach, $config): void {
    parent::attach($load, $attach, $config);

    $load['library'][] = 'gridstack/native';
  }

}
