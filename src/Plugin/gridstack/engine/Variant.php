<?php

namespace Drupal\gridstack\Plugin\gridstack\engine;

use Drupal\gridstack\Engine\GridStackEnginePluginBase;
use Drupal\gridstack\Entity\GridStackVariant;

/**
 * Provides a shadow variant layout engine.
 *
 * @GridStackEngine(
 *   id = "variant",
 *   group = "gridstack",
 *   hidden = "true",
 *   version = "2",
 *   label = @Translation("Variant engine")
 * )
 */
class Variant extends GridStackEnginePluginBase {

  /**
   * Allows layout variants to override the original optionset.
   */
  public function override(&$optionset, array &$settings) {
    $variant = NULL;

    if ($vid = ($settings['vid'] ?? NULL)) {
      $variant = GridStackVariant::load($vid);
      if ($variant && $breakpoints = $variant->getBreakpoints()) {
        $optionset->setOptions(['breakpoints' => $breakpoints]);
        $settings['_variant'] = $variant->label();
      }
    }
    return $variant;
  }

}
