<?php

namespace Drupal\gridstack\Plugin\gridstack\engine;

/**
 * Provides a GridStack Foundation6 layout engine.
 *
 * @GridStackEngine(
 *   id = "foundation6",
 *   group = "foundation",
 *   hidden = "false",
 *   version = "6",
 *   label = @Translation("Foundation 6")
 * )
 *
 * @todo Foundation 6:
 * https://get.foundation/sites/docs/xy-grid.html
 * https://get.foundation/sites/docs/grid.html
 * Top container: grid-container full|fluid, one level above .gridstack.
 * Container/ row: grid-x|y grid-margin-x|y grid-padding-x|y
 * Container/ row: small-up-2 medium-up-4 large-up-6 medium-margin-collapse
 * Container/ row: grid-y grid-frame medium-grid-frame
 * Cell: cell auto shrink small-6 medium-8 large-2 large-auto
 * Cell: .[size]-auto or [size]-shrink .[size]-[gutter-type]-collapse
 */
class Foundation6 extends FoundationBase {

  /**
   * {@inheritdoc}
   */
  protected $containerClasses = ['grid-x'];

  /**
   * {@inheritdoc}
   */
  protected $nestedContainerClasses = ['grid-x'];

}
