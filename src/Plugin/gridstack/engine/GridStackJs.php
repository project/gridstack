<?php

namespace Drupal\gridstack\Plugin\gridstack\engine;

/**
 * Provides a GridStack JS layout engine.
 *
 * @GridStackEngine(
 *   id = "gridstack_js",
 *   group = "gridstack",
 *   hidden = "true",
 *   version = "2",
 *   label = @Translation("GridStack JS")
 * )
 */
class GridStackJs extends GridStackBase {

  /**
   * Provides specific classes for this layout.
   *
   * The .is-gs-layout can be used dynamically during Isotope filtering when
   * native CSS Grid is enabled to support them both at the same time as needed.
   *
   * {@inheritdoc}
   */
  protected $containerClasses = ['gridstack--gs', 'is-gs-layout'];

  /**
   * {@inheritdoc}
   */
  public function attach(array &$load, array $attach, $config): void {
    parent::attach($load, $attach, $config);

    // Allows to not use GridStack libraries, yet still re-use the same markups.
    if ($config->use('gridstack')) {
      $load['library'][] = 'gridstack/load';

      $column = $config->get('column');
      if ($column && $column < 12) {
        $load['library'][] = 'gridstack/gridstack.' . $column;
      }

      // Breakpoints: xs sm md lg xl requires separate CSS files.
      if ($columns = array_unique(array_values($this->columns))) {
        foreach ($columns as $column) {
          if ($column < 12) {
            $load['library'][] = 'gridstack/gridstack.' . $column;
          }
        }
      }
    }
  }

}
