<?php

namespace Drupal\gridstack\Plugin\gridstack\engine;

/**
 * Provides a GridStack Bootstrap 4 layout engine.
 *
 * @GridStackEngine(
 *   id = "bootstrap4",
 *   group = "bootstrap",
 *   hidden = "false",
 *   version = "4",
 *   label = @Translation("Bootstrap 4")
 * )
 */
class Bootstrap4 extends BootstrapBase {

  /**
   * {@inheritdoc}
   */
  protected function baseColors() {
    $colors = parent::baseColors();
    $colors4 = ['secondary', 'light', 'dark'];
    return array_merge($colors, $colors4);
  }

  /**
   * {@inheritdoc}
   */
  protected function colors() {
    $colors = parent::colors();
    $colors4 = ['white', 'transparent'];
    return array_merge($colors, $colors4);
  }

  /**
   * Sets the optional plugin engine classes for options, row, hard-coded.
   */
  protected function setRowClassOptions() {
    if (!isset($this->setRowClassOptions)) {
      $gutters[] = 'no-gutters';
      $flex_direction = [
        'flex-row',
        'flex-column',
        'flex-row-reverse',
        'flex-column-reverse',
      ];

      $flex_wrap = [
        'flex-wrap',
        'flex-nowrap',
        'flex-wrap-reverse',
      ];

      $justify_content = [
        'justify-content-start',
        'justify-content-end',
        'justify-content-center',
        'justify-content-between',
        'justify-content-around',
      ];

      $align_items = [
        'align-items-start',
        'align-items-end',
        'align-items-center',
        'align-items-baseline',
        'align-items-stretch',
      ];

      $align_content = [
        'align-content-start',
        'align-content-end',
        'align-content-center',
        'align-content-between',
        'align-content-around',
        'align-content-stretch',
      ];

      $align_self = [
        'align-self-auto',
        'align-self-start',
        'align-self-end',
        'align-self-center',
        'align-self-baseline',
        'align-self-stretch',
      ];

      $this->setRowClassOptions = [
        'gutters' => $gutters,
        'flex_direction' => $flex_direction,
        'flex_wrap' => $flex_wrap,
        'justify_content' => $justify_content,
        'align_items' => $align_items,
        'align_content' => $align_content,
        'align_self' => $align_self,
      ];
    }
    return $this->setRowClassOptions;
  }

  /**
   * {@inheritdoc}
   */
  protected function getVersionClasses() {
    $classes = parent::getVersionClasses();
    $gradient = [];

    foreach ($this->baseColors() as $type) {
      $gradient[] = "bg-gradient-$type";
    }

    $rounded = ['rounded', 'rounded-0'];
    foreach (['bottom', 'circle', 'left', 'lg', 'pill', 'right', 'sm', 'top'] as $key) {
      $rounded[] = 'rounded-' . $key;
    }

    $shadow = ['shadow'];
    foreach (['lg', 'none', 'sm'] as $key) {
      $shadow[] = 'shadow-' . $key;
    }

    $classes['gradient'] = $gradient;
    $classes['rounded'] = $rounded;
    $classes['shadow'] = $shadow;

    $classes['text_color'][] = 'text-black-50';
    $classes['text_color'][] = 'text-white-50';
    $classes['utility'][] = 'text-monospace';
    $classes['visibility'][] = 'invisible';
    $classes['visibility'][] = 'visible';

    return $classes;
  }

}
