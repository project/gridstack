<?php

namespace Drupal\gridstack\Plugin\gridstack\engine;

use Drupal\Component\Utility\Html;
use Drupal\gridstack\Engine\GridStackEnginePluginBase;
use Drupal\gridstack\Entity\GridStack;

/**
 * Provides the base class for two-dimensional layout engines.
 */
abstract class GridStackBase extends GridStackEnginePluginBase {

  /**
   * The GridStack nodes.
   *
   * @var array
   */
  protected $nodes = ['x', 'y', 'w', 'h', 'region'];

  /**
   * {@inheritdoc}
   */
  public function attach(array &$load, array $attach, $config): void {
    parent::attach($load, $attach, $config);

    // Only load libraries if not destroyed, nor using Bootstrap/ Foundation.
    if ($config->use('gridstack')) {
      $load['drupalSettings']['gridstack'] = GridStack::defaultSettings();
    }

    $load['library'][] = 'gridstack/layout';
  }

  /**
   * {@inheritdoc}
   */
  public function build(array &$build, array &$element): void {
    parent::build($build, $element);

    // Provides dummy item to fix layout aspect ratio.
    $settings = $build['#settings'];
    $config = $settings['gridstacks'];
    $delta = count($build['items']) + 1;

    if ($config->get('use.gridstack')) {
      $build['postscript']['dummy'] = $this->buildDummyItem($delta, $settings);
    }
  }

  /**
   * Returns the dummy box to measure cell height to fix aspect ratio.
   */
  protected function buildDummyItem($delta, array $settings) {
    $config = $settings['gridstacks'];
    $item['attributes']['class'][] = 'gridstack__sizer is-nixbox';
    $item['attributes']['gs-h'] = 1;
    $item['attributes']['gs-w'] = 1;

    // @todo remove for v4:
    $item['attributes']['data-gs-height'] = 1;
    $item['attributes']['data-gs-width'] = 1;

    $config->set('use.inner', TRUE)
      ->set('dummy', TRUE);

    $this->manager->reset($settings, TRUE);

    return $this->buildItem($delta, $settings, $item);
  }

  /**
   * {@inheritdoc}
   */
  protected function itemAttributes(array &$attributes, array &$settings) {
    parent::itemAttributes($attributes, $settings);

    $config = $settings['gridstacks'];
    $id     = $config->get('delta', 0);
    $nid    = $config->get('nested.delta', -1);
    $root   = $config->get('_root', 'grids');
    $grids  = $config->get('_grids') ?: $this->getOptionset()->getLastBreakpoint($root);
    $gids   = $grids[$id] ?? [];
    $nested = $config->get('nested', []);

    // @todo nested grids for js-driven/ native Grid layouts, respects 0.
    if (isset($nested['delta']) && $root == 'nested') {
      $node = isset($gids[$nid]) ? $this->getOptionset()->getNode($gids[$nid], FALSE) : [];
    }
    else {
      // The root element grids.
      $node = $gids ? $this->getOptionset()->getNode($gids, FALSE) : [];
    }

    if ($node) {
      $this->nodeAttributes($attributes, $node, $settings);
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function nodeAttributes(array &$attributes, array $node, array $settings) {
    $config = $settings['gridstacks'];
    $ns = $config->get('nameshort', 'gs');

    foreach ($this->nodes as $key) {
      if (isset($node[$key])) {
        if ($key == 'region' && $node[$key]) {
          $attributes['class'][] = 'box--' . Html::cleanCssIdentifier($node[$key]);
        }
        else {
          $attributes[$ns . '-' . $key] = (int) $node[$key];

          // @todo remove after update.
          $attributes['class'][] = 'grid-stack-item';

          // @todo remove for v4:
          $attributes['data-' . $ns . '-' . $key] = (int) $node[$key];
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function containerAttributes(array &$attributes, array &$settings) {
    parent::containerAttributes($attributes, $settings);
    $config = $settings['gridstacks'];

    $attributes['class'][] = 'gridstack--js';

    // Responsive breakpoint related data-attributes helpers.
    $optionset = $this->getOptionset();
    $attributes['data-gs-min-w'] = $this->minWidth;

    if ($optionset->getSetting('noMargin', FALSE)) {
      $attributes['class'][] = 'is-gs-nomargin';
    }

    if ($vm = $optionset->getSetting('margin')) {
      $attributes['data-gs-vm'] = $vm;
    }

    // Do not proceed if gridstack is disabled.
    if ($config->is('ungridstack')) {
      $attributes['class'][] = 'ungridstack';
      return;
    }

    // Use the customized gridstack static without dnd if so configured.
    if ($config->use('gridstatic') && !$config->use('gridnative')) {
      $attributes['class'][] = 'gridstack--static';
    }

    // JS-enabled layouts, including native Grid.
    // Cannot rely on the library grid-stack-static, since not always there
    // such as when using native Grid which doesn't load GridStack library.
    if (!$config->is('ipe')) {
      $attributes['class'][] = 'is-gs-packing';
    }

    $attributes['class'][] = 'is-gs-enabled';

    // Adds attributes for js-driven or native Grid layouts so to be responsive.
    // Supports dynamic like Isotope to have both native CSS and JS layouts.
    $attributes['data-gs-column']  = $optionset->getLastColumn();
    $attributes['data-gs-columns'] = $optionset->getJson('breakpoints');
    $attributes['data-gs-config']  = $optionset->getJson('settings');
    $attributes['data-gs-data']    = $optionset->getData();
  }

  /**
   * {@inheritdoc}
   */
  protected function itemContentAttributes(array &$attributes, array &$settings) {
    parent::itemContentAttributes($attributes, $settings);

    // Provides configurable content attributes via Layout Builder.
    $this->attributes($attributes, $settings);
  }

}
