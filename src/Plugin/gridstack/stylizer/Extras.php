<?php

namespace Drupal\gridstack\Plugin\gridstack\stylizer;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\gridstack\GridStackDefault;

/**
 * Provides the extras styles.
 *
 * @GridStackStylizer(
 *   id = "extras",
 *   label = @Translation("Extras")
 * )
 */
class Extras extends Wrapper {

  /**
   * Returns utilities settings.
   */
  public static function options() {
    return [
      'background'    => 'BG (image as background)',
      'contentless'   => 'BG/ Image only mode (contentless)',
      'ete'           => 'BG/ Image spans edge to egde (EtE)',
      'overlay'       => 'BG color overlays BG/ Image',
      'shrink'        => 'BG/ Image shrink (not for EtE)',
      'parallax'      => 'BG/ Image simple parallax',
      'parallax-fs'   => 'Make the above parallax full screen',
    ];
  }

  /**
   * {@inheritdoc}
   *
   * @todo implements when having enough consistency.
   */
  public function defaultConfiguration() {
    $config = [];
    foreach (array_keys(self::options()) as $key) {
      $config[$key] = '';
    }
    return $config;
  }

  /**
   * {@inheritdoc}
   */
  public function attach(array &$load, array $attach, $config): void {
    parent::attach($load, $attach, $config);

    if ($this->getStyle('parallax', $attach) && !$config->is('ipe')) {
      $load['library'][] = 'gridstack/parallax';
    }
  }

  /**
   * Provides extras form elements.
   */
  protected function extrasElement($optionset, FormStateInterface $form_state, array $settings, array $extras = []) {
    $context = $settings['_scope'];
    $options = [];

    foreach (self::options() as $key => $title) {
      if ($this->isWideRegionOption($key) && empty($settings['_fullwidth'])) {
        continue;
      }
      if (in_array($key, ['shrink']) && !$optionset->isFramework()) {
        continue;
      }

      $options[$key] = $this->t('@title', ['@title' => $title]);
    }

    if ($context != GridStackDefault::ROOT) {
      unset($options['parallax'], $options['parallax-fs']);
    }

    $css_classes = [
      'form-wrapper--extras',
      'is-collapsible',
      'is-collapsed',
      'is-gs-fieldset',
    ];

    $values = $settings['extras'];
    return [
      '#type'          => 'checkboxes',
      '#options'       => $options,
      '#title'         => $this->t('Options for image above, not blocks'),
      '#default_value' => empty($values) ? [] : array_values($values),
      '#attributes'    => ['class' => $css_classes],
      '#after_build'   => [[$this, 'afterBuildExtras']],
      '#region'        => $context,
      '#settings'      => $settings,
    ];
  }

  /**
   * Checks if an option should be for 12 column wide.
   */
  protected function isWideRegionOption($key) {
    return in_array($key, ['ete', 'contentless', 'parallax', 'parallax-fs']);
  }

  /**
   * Provides preview classes.
   */
  public function afterBuildExtras(array $element) {
    $settings = $element['#settings'];
    $use_background = $this->getStyle('background', $settings);

    $removed = [
      'form-wrapper--extras',
      'is-collapsible',
      'is-collapsed',
      'is-gs-fieldset',
    ];

    foreach (Element::children($element) as $key) {
      $element[$key]['#attributes']['data-gs-extras-region'] = $element['#region'];
      $element[$key]['#attributes']['data-gs-extras-region-class'] = 'is-gs-' . str_replace('_', '-', $key);
      $element[$key]['#attributes']['data-gs-bg-selector'] = $use_background ? '.b-gs .media__element' : '.b-gs';

      if ($key == 'parallax') {
        $element[$key]['#description'] = $this->t('Recipes: BG, Min height, EtE at odd rows, non-EtE even, or vice versa, for simple parallax. Use non-BG for regular transitions. Plus few more adjustments.');
      }

      // Unfortunately container classes are also inherited by each item.
      if (isset($element[$key]['#attributes']['class'])) {
        $classes = $element[$key]['#attributes']['class'];
        $classes = array_diff($classes, $removed);
        $element[$key]['#attributes']['class'] = $classes;
      }
    }
    return $element;
  }

}
