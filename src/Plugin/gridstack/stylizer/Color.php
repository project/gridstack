<?php

namespace Drupal\gridstack\Plugin\gridstack\stylizer;

use Drupal\Core\Form\FormStateInterface;
use Drupal\gridstack\GridStackDefault;

/**
 * Provides the color styles.
 *
 * @GridStackStylizer(
 *   id = "color",
 *   label = @Translation("Color")
 * )
 */
class Color extends Range {

  /**
   * Returns the text colors grouped by the given key.
   */
  protected function getTextColor(array $settings) {
    $build = [];

    if ($colors = $this->getColors($settings)) {
      $data = $this->getSelector($settings);
      $selector = $data['selector'];

      // The text element has unique key as their own selector.
      foreach (GridStackDefault::textElements() as $key) {
        if ($key != 'text' && !empty($colors[$key])) {
          $selector = $selector ? $selector . ' ' . $key : $key;

          // @fixme.
          $selector = str_replace('a a:', 'a:', $selector);
          $build[$selector] = 'color:' . $colors[$key] . ';';
        }
      }
    }

    return $build;
  }

  /**
   * Provides extras form elements.
   */
  protected function colorElement($optionset, FormStateInterface $form_state, array $settings, array $extras = []) {
    $element = ['#type' => 'container'];

    foreach (GridStackDefault::colorElements() as $key) {
      $value = $settings['colors'][$key] ?? '';
      $element[$key] = $this->styleElement($key, $value, $settings);
    }

    return $element;
  }

  /**
   * Return the color palette element.
   */
  protected function paletteElement(array $palettes) {
    $element = [
      '#type'        => 'details',
      '#open'        => FALSE,
      '#tree'        => TRUE,
      '#title'       => $this->t('Color palettes'),
      '#description' => $this->t('Use these as guidelines to select colors from with the color pickers.'),
      '#attributes'  => ['class' => ['form-wrapper--color-palettes']],
      '#weight'      => 50,
    ];

    $items = [];
    foreach ($palettes as $group => $colors) {
      $groups = [];
      foreach ($colors as $delta => $color) {
        $title = '';
        if ($delta == 0) {
          $title = '<h3>' . $group . '</h3>';
        }
        $groups[] = [
          '#type' => 'item',
          '#title' => $title,
          [
            '#type' => 'html_tag',
            '#tag' => 'span',
            '#value' => $color,
            '#attributes' => [
              'class' => ['gs-color'],
              'style' => 'background-color: ' . $color,
            ],
          ],
        ];
      }

      $items[] = $groups;
    }

    $element['palettes'] = [
      '#theme' => 'item_list',
      '#items' => $items,
    ];
    return $element;
  }

}
