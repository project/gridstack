<?php

namespace Drupal\gridstack\Plugin\gridstack\stylizer;

/**
 * Provides the common styles.
 *
 * @GridStackStylizer(
 *   id = "style",
 *   label = @Translation("Style")
 * )
 */
class Style extends Extras {

  /**
   * Parses the given color strings keyed by index or .box__content or selector.
   */
  public function getStyles(array $settings) {
    // Provides background colors keyed by .box__content, or top selector.
    $backgrounds = $this->getBackgroundColor($settings, TRUE);

    // Provides text colors keyed by their own selectors.
    $colors = $this->getTextColor($settings);

    return array_merge($backgrounds, $colors);
  }

}
