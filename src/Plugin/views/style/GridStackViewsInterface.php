<?php

namespace Drupal\gridstack\Plugin\views\style;

use Drupal\blazy\Views\BlazyStylePluginInterface;

/**
 * Provides an interface defining GridStack Views.
 */
interface GridStackViewsInterface extends BlazyStylePluginInterface {

  /**
   * Returns the gridstack manager.
   *
   * @return \Drupal\gridstack\GridStackManagerInterface
   *   The gridstack manager.
   */
  public function manager();

  /**
   * Returns the gridstack admin.
   *
   * @return \Drupal\gridstack\Form\GridStackAdminInterface
   *   The gridstack admin.
   */
  public function admin();

  /**
   * Returns extra row/ element content such as Isotope filters, sorters, etc.
   */
  public function buildElementExtra(array &$box, $row, $delta);

}
