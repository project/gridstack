<?php

namespace Drupal\gridstack\Plugin\Layout;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginFormInterface;

/**
 * Provides an interface defining GridStack stylizer.
 */
interface GridStackLayoutInterface extends ContainerFactoryPluginInterface, PluginFormInterface {}
