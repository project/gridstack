<?php

namespace Drupal\gridstack\Plugin\Layout;

use Drupal\Core\Render\Element;
use Drupal\gridstack\GridStackDefault as Defaults;

/**
 * Provides a GridStack class for Layout plugins.
 */
class GridStackLayout extends GridStackLayoutBase {

  /**
   * {@inheritdoc}
   */
  public function build(array $regions): array {
    ksort($regions);

    $config     = $this->getConfiguration();
    $settings   = $config;
    $definition = $this->getPluginDefinition();
    $name       = $definition->get('optionset');
    $optionset  = $this->manager->loadSafely($name);

    $settings['optionset'] = $name;
    $this->manager->adminSettings($settings);

    $gridstacks = $settings['gridstacks'];
    $gridstacks->set('use.stylizer', TRUE)
      ->set('lb', $config);

    $this->manager->engineSettings($settings, $optionset);

    $variant = $this->manager
      ->getEngine($settings, 'variant')
      ->override($optionset, $settings);

    // Provides build.
    $build = [
      'items'      => $this->interpolateItems($optionset, $settings, $regions),
      '#optionset' => $optionset,
      '#settings'  => $settings,
      '#layout'    => $definition,
      '#variant'   => $variant,
    ];

    // Still a check in case it is removed by another alter.
    // No use no longer work since D9.5.10.
    // if ($library = $this->pluginDefinition->getLibrary()) {
    // $build['#attached']['library'][] = $library;
    // }
    // Must pass the layout blueprint to Layout Builder regardless empty.
    return $this->manager->build($build);
  }

  /**
   * Interpolate data from Layout Builder to match original construct.
   *
   * We do this because we don't manually put regions into templates.
   * Instead structured with nested grids, if any, to have one template for any
   * known layout possibility: one or two dimensional layouts.
   * With everything being cached at D8, this shouldn't make much different than
   * hard-coded layouts at YML files which still need parsing anyway.
   */
  protected function interpolateItems(
    $optionset,
    array &$settings,
    array $regions,
  ): array {
    $id     = 'box';
    $items  = [];
    $config = $settings['regions'] ?? [];

    // unset($settings['regions']);.
    foreach (array_keys($optionset->getLastBreakpoint()) as $delta) {
      $rid = Defaults::regionId($delta);
      $box = [];

      // Remove top level settings to avoid leaking due to similarity.
      $sets = array_diff_key($settings, Defaults::regionSettings());
      $sets = $this->manager->merge($config[$rid] ?? [], $sets);

      $this->manager->reset($sets);
      $box['settings'] = $sets;

      if ($grids = $optionset->getNestedGridsByDelta($delta)) {
        foreach (array_keys($grids) as $nid) {
          $rid = Defaults::regionId($delta . '_' . $nid);

          // @todo recheck $box['settings'] = array_diff_key($box['settings'], Defaults::regionSettings());
          $sets = $box['settings'];
          $this->manager->reset($sets, TRUE);

          // Preserves indices even if empty so to layout for Layout Builder.
          $region = $regions[$rid] ?? NULL;
          $box[$id][$nid][$id] = $region && !Element::isEmpty($region) ? $region : [];

          $box[$id][$nid]['#settings'] = $this->manager->merge($config[$rid] ?? [], $sets);

        }
      }
      else {
        // Preserves indices even if empty so to layout for Layout Builder.
        $region = $regions[$rid] ?? NULL;
        $box[$id] = $region && !Element::isEmpty($region) ? $region : [];
      }

      $items[] = $box;
      unset($box);
    }
    return $items;
  }

}
