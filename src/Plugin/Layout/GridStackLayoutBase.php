<?php

namespace Drupal\gridstack\Plugin\Layout;

use Drupal\Component\Utility\Unicode;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformStateInterface;
use Drupal\Core\Layout\LayoutDefault;
use Drupal\gridstack\GridStackDefault as Defaults;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a GridStack base class for Layout plugins.
 */
abstract class GridStackLayoutBase extends LayoutDefault implements GridStackLayoutInterface {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The gridstack manager service.
   *
   * @var \Drupal\gridstack\GridStackManagerInterface
   */
  protected $manager;

  /**
   * The gridstack stylizer service.
   *
   * @var \Drupal\gridstack\Style\GridStackStylizerInterface
   */
  protected $stylizer;

  /**
   * The gridstack stylizer form plugin.
   *
   * @var \Drupal\gridstack\Style\GridStackStylizerPluginInterface
   */
  protected $styleForm;

  /**
   * The gridstack admin service.
   *
   * @var \Drupal\gridstack\Form\GridStackAdminInterface
   */
  protected $admin;

  /**
   * The field name to store media.
   *
   * @var string
   */
  protected $fieldName;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static(
      $configuration,
      $plugin_id,
      $plugin_definition
    );

    $instance->currentUser = $container->get('current_user');
    $instance->manager = $container->get('gridstack.manager');
    $instance->stylizer = $instance->manager->stylizer();

    $config = [];
    foreach (['field_name', 'optionset'] as $key) {
      if (isset($configuration[$key])) {
        $config[$key] = $configuration[$key];
      }
    }
    $instance->styleForm = $instance->stylizer->form($config);

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return Defaults::layoutSettings() + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    $regions = [];
    if ($values = $form_state->getValue('regions')) {
      foreach ($values as $name => &$region) {
        $this->styleForm->cleanupStyles($region['styles']);

        foreach ($region as $key => &$value) {
          $this->massageValues($value, $key);

          if (in_array($key, ['preset_classes', 'preset_row_classes'])) {
            continue;
          }
          $regions[$name][$key] = $value;
        }
      }
    }

    $this->configuration['regions'] = $regions;

    if ($settings = $form_state->getValue('settings')) {
      $this->styleForm->cleanupStyles($settings['styles']);

      if (isset($settings['global'])) {
        foreach ($settings['global'] as $key => $value) {
          $settings[$key] = $value;
        }
        unset($settings['global']);
      }

      unset(
        $settings['current_selection'],
        $settings['preset_classes'],
        $settings['preset_row_classes']
      );

      foreach ($settings as $key => &$value) {
        $this->massageValues($value, $key);

        $this->configuration[$key] = $value;
      }
    }

    unset($this->configuration['global'], $this->configuration['settings']);
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state): void {
    parent::validateConfigurationForm($form, $form_state);

    $this->styleForm->validateConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    // This form may be loaded as a subform Layout Builder, etc.
    // More info: #2536646, #2798261, #2774077, #2897557.
    $form_state2 = $form_state instanceof SubformStateInterface
      ? $form_state->getCompleteFormState() : $form_state;
    $form        = parent::buildConfigurationForm($form, $form_state2);
    $access_ui   = $this->currentUser->hasPermission('administer gridstack');
    $config      = $this->getConfiguration();
    $definition  = $this->getPluginDefinition();
    $name        = $definition->get('optionset');
    $optionset   = $this->manager->loadSafely($name);
    $entity_form = isset($form_state2->getBuildInfo()['callback_object']) ? $form_state2->getFormObject() : NULL;
    $extras      = $entity_form ? $this->styleForm->getEntityData($entity_form) : [];

    if (isset($form['label'])) {
      $name_nice = Unicode::ucfirst($name);
      $form['label']['#attributes']['placeholder'] = $this->t('@name', ['@name' => $name_nice]);
      $form['label']['#wrapper_attributes']['class'][] = 'is-gs-aside';
      $form['label']['#description'] = $this->t('A region has direct contents. A container contains multiple regions.');
      $default = empty($config['label']) ? str_replace('_', ' ', $name_nice) : $config['label'];
      $form['label']['#default_value'] = $form_state2->getValue('label', $default);
    }

    foreach (Defaults::layoutSettings() as $key => $value) {
      // $default = $config[$key] ?? $value;
      $config[$key] = $config[$key] ?? $value;
      // $form_state2->getValue(['settings', $key], $default);
    }

    $this->manager->verifySafely($config);
    $this->manager->getEngine($config, 'variant')->override($optionset, $config);

    // Allows regions being modified by variants.
    $regions                = $optionset->prepareRegions(FALSE);
    $config['_fullwidth']   = TRUE;
    $config['_container']   = TRUE;
    $config['_scope']       = Defaults::ROOT;
    $config['_delta']       = 0;
    $this->fieldName        = $config['field_name'];
    $field_options          = $extras ? $this->styleForm->getLayoutFieldOptions() : [];
    $config['access_media'] = $access_media = $this->currentUser->hasPermission('access media overview');

    if (empty($config['field_name']) && $field_options) {
      $this->fieldName = $config['field_name'] = reset($field_options);
    }

    $extras['field_options'] = $field_options;
    $style_config = ['field_name' => $this->fieldName, 'optionset' => $name];

    $this->styleForm->setConfiguration($style_config);
    $this->styleForm->setFieldName($this->fieldName);

    $description = $this->t('Options require saving the form first.');
    if ($this->manager->moduleExists('gridstack_ui') && $access_ui) {
      $description .= '<br>' . $this->t('[<a href=":url" class="is-gs-edit-link">Edit @id</a>]', [
        ':url' => $optionset->toUrl('edit-form')->toString(),
        '@id' => Xss::filter($optionset->label()),
      ]);
    }

    $form['settings'] = [
      '#type'        => 'details',
      '#tree'        => TRUE,
      '#open'        => TRUE,
      '#weight'      => 30,
      '#title'       => $this->t('Global settings'),
      '#description' => $description,
      '#attributes'  => [
        'class' => ['form-wrapper--gs', 'is-gs-main-settings'],
        'data-gs-delta'  => 0,
        'data-gs-region' => $config['_scope'],
      ],
      '#field_name'  => $this->fieldName,
      '#parents'     => ['layout_settings', 'settings'],
    ];

    $form['settings'] = array_merge(
      $form['settings'],
      $this->styleForm->buildConfigurationForm($optionset, $form_state2, $config, $extras)
    );

    $form['regions'] = [
      '#type'    => 'container',
      '#tree'    => TRUE,
      '#parents' => ['layout_settings', 'regions'],
    ];

    // Reset settings. The delta is faked for Media Library where 0 is
    // reserved by the non-region, top-level settings. Also to support the
    // styling of non-region, aka. container. The rid is faked, too.
    $delta = 1;
    $rid   = 0;

    foreach ($regions as $region => $info) {
      foreach (Defaults::regionSettings() as $key => $value) {
        $config['regions'][$region][$key] = $config['regions'][$region][$key] ?? $value;
      }

      $form['regions'][$region] = [
        '#type'       => 'details',
        '#title'      => $this->t('@type: <em>@label</em>', [
          '@type'  => $info['type'],
          '@label' => $info['label'],
        ]),
        '#open'       => FALSE,
        '#tree'       => TRUE,
        '#attributes' => [
          'data-gs-delta'  => $delta,
          'data-gs-region' => $region,
          'class'          => ['form-wrapper--gs'],
        ],
        '#field_name' => $this->fieldName,
        '#parents'    => ['layout_settings', 'regions', $region],
      ];

      if ($info['type'] == Defaults::CONTAINER) {
        $form['regions'][$region]['#attributes']['class'][] = 'form-wrapper--cn';
        unset($form['regions'][$region]['#open']);
      }

      if (isset($info['cid'])) {
        $form['regions'][$region]['#attributes']['data-gs-region-container'] = $info['cid'];
      }

      $subsets = &$config['regions'][$region];

      $subsets['field_name']   = $this->fieldName;
      $subsets['access_media'] = $access_media;
      $subsets['_fullwidth']   = !empty($info['_fw']);
      $subsets['_rid']         = $rid;
      $subsets['_scope']       = $region;
      $subsets['_delta']       = $delta;
      $subsets['_container']   = $info['type'] == Defaults::CONTAINER;

      $this->manager->verifySafely($subsets);
      $blazies = $subsets['blazies']->reset($subsets);
      $blazies->set('delta', $delta);

      $form['regions'][$region] = array_merge(
        $form['regions'][$region],
        $this->styleForm->buildConfigurationForm($optionset, $form_state2, $subsets, $extras)
      );

      if ($info['type'] == 'Region') {
        $rid++;
      }
      $delta++;
    }

    // Provides color palettes and assets.
    $this->styleForm->closingForm($form, $config);

    return $form;
  }

  /**
   * Massage form values.
   */
  protected function massageValues(&$value, $key) {
    if ($key == 'styles') {
      foreach (['alpha', 'opacity'] as $k) {
        if (isset($value[$k]) && $value[$k] == '1') {
          $value[$k] = '';
        }
      }
    }

    $value = is_string($value) ? trim($value) : $value;
    $value = is_array($value) ? array_filter($value) : $value;
  }

}
