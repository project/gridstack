<?php

namespace Drupal\gridstack\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;

/**
 * Plugin implementation of the 'GridStack Paragraphs Media' formatter.
 */
class GridStackParagraphsMediaFormatter extends GridStackMediaFormatter {

  /**
   * {@inheritdoc}
   */
  public function prepareView(array $entities_items) {
    // Entity revision loading currently has no static/persistent cache and no
    // multiload. As entity reference checks _loaded, while we don't want to
    // indicate a loaded entity, when there is none, as it could cause errors,
    // we actually load the entity and set the flag.
    foreach ($entities_items as $items) {
      foreach ($items as $item) {

        if ($item->entity) {
          $item->_loaded = TRUE;
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    $storage = $field_definition->getFieldStorageDefinition();

    // Excludes host, prevents complication with multiple nested paragraphs.
    $paragraph = $storage->getTargetEntityTypeId() === 'paragraph';
    return $paragraph
      && $storage->isMultiple()
      && $storage->getSetting('target_type') === 'paragraph';
  }

  /**
   * {@inheritdoc}
   */
  protected function getPluginScopes(): array {
    $types  = ['image', 'entity_reference'];
    $stages = $this->getFieldOptions($types);

    return [
      'grid_form' => FALSE,
      'style'     => FALSE,
      'images'    => $stages,
      'vanilla'   => FALSE,
    ] + parent::getPluginScopes();
  }

}
