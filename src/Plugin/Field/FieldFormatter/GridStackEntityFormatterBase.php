<?php

namespace Drupal\gridstack\Plugin\Field\FieldFormatter;

use Drupal\blazy\Field\BlazyEntityVanillaBase;
use Drupal\gridstack\GridStackDefault;

/**
 * Base class for gridstack entity reference formatters without field details.
 */
abstract class GridStackEntityFormatterBase extends BlazyEntityVanillaBase {

  use GridStackFormatterTrait;

  /**
   * {@inheritdoc}
   */
  protected static $namespace = 'gridstack';

  /**
   * {@inheritdoc}
   */
  protected static $itemId = 'box';

  /**
   * {@inheritdoc}
   */
  protected static $itemPrefix = 'box';

  /**
   * {@inheritdoc}
   */
  protected static $captionId = 'caption';

  /**
   * {@inheritdoc}
   */
  protected static $fieldType = 'entity';

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $settings = GridStackDefault::baseSettings();
    $settings['view_mode'] = '';

    return $settings + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  protected function pluginSettings(&$blazies, array &$settings): void {
    $this->traitPluginSettings($blazies, $settings);
    $blazies->set('is.vanilla', TRUE);

    // @todo remove.
    $settings['vanilla'] = TRUE;
  }

}
