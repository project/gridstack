<?php

namespace Drupal\gridstack;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Plugin\PluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides base class for all gridstack plugins.
 */
abstract class GridStackPluginBase extends PluginBase implements GridStackPluginInterface {

  /**
   * The gridstack optionset.
   *
   * @var \Drupal\gridstack\Entity\GridStack
   */
  protected $optionset;

  /**
   * The library attachments.
   *
   * @var array
   */
  protected $attachments = [];

  /**
   * The layout breakpoints.
   *
   * @var array
   */
  protected $breakpoints = [];

  /**
   * The breakpoint columns.
   *
   * @var array
   */
  protected $columns = [];

  /**
   * The optionset min-width.
   *
   * @var int
   */
  protected $minWidth;

  /**
   * The optionset cell height.
   *
   * @var int
   */
  protected $cellHeight;

  /**
   * The optionset vertical margin.
   *
   * @var int
   */
  protected $margin;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The gridstack manager service.
   *
   * @var \Drupal\gridstack\GridStackManagerInterface
   */
  protected $manager;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition,
  ) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->currentUser = $container->get('current_user');
    $instance->manager = $container->get('gridstack.manager');

    $gridstack = $configuration['gridstacks'] ?? $instance->manager->settings();
    if (!isset($configuration['gridstacks'])) {
      $configuration['gridstacks'] = $gridstack;
    }
    $instance->setConfiguration($configuration);

    if ($optionset = $configuration['optionset'] ?? NULL) {
      $instance->setOptionset($optionset);
    }

    $instance->attachments = $instance->setAttachments($configuration, $gridstack);
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    return $this->pluginDefinition['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function get($key) {
    return $this->pluginDefinition[$key] ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configs) {
    $defaults = $this->configuration + $this->defaultConfiguration();

    // @todo use $this->manager->mergeSettings($keys, $defaults, $configs); post 2.17.
    foreach (['blazies', 'gridstacks'] as $key) {
      $object = $defaults[$key] ?? NULL;
      $oldies = $object ? $object->storage() : [];

      if (!isset($configs[$key]) && $object) {
        $configs[$key] = $object;
      }

      if ($newbies = $configs[$key] ?? NULL) {
        $data = $newbies->storage();
        $data = $oldies ? NestedArray::mergeDeepArray([$oldies, $data], TRUE) : $data;
        $configs[$key]->setData($data);
      }
    }

    $this->configuration = NestedArray::mergeDeep(
      $defaults,
      $configs
    );
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function attach(array &$load, array $attach, $config): void {
    if ($attachments = $this->attachments) {
      $load = $this->manager->merge($attachments, $load);
    }
  }

  /**
   * Sets the optional plugin attachments.
   *
   * @todo implement, or remove this post 2.12.
   */
  public function setAttachments(array $attach, $config): array {
    return [];
  }

  /**
   * Returns gridstack config shortcut.
   */
  protected function config($key = '', $settings = 'gridstack.settings') {
    return $this->manager->config($key, $settings);
  }

  /**
   * {@inheritdoc}
   */
  public function getSetting($key, $default = NULL) {
    return $this->configuration[$key] ?? $default;
  }

  /**
   * {@inheritdoc}
   */
  public function setSetting($key, $value) {
    $this->configuration[$key] = $value;
    return $this;
  }

  /**
   * Sets the optionset.
   */
  public function setOptionset($optionset) {
    $this->optionset = is_string($optionset)
      ? $this->manager->loadSafely($optionset) : $optionset;

    $this->breakpoints = $this->optionset->breakpointsToArray();
    $this->columns     = $this->optionset->getColumns();
    $this->minWidth    = (int) $this->optionset->getSetting('minW', 481);
    $this->cellHeight  = $this->optionset->getSetting('cellHeight', 80);
    $this->margin      = $this->optionset->getSetting('margin', 0);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOptionset() {
    if (!isset($this->optionset)) {
      $this->optionset = $this->manager->loadSafely($this->getSetting('optionset', 'default'));
    }
    return $this->optionset;
  }

  /**
   * Returns the module or theme path.
   */
  protected function getPath($type, $name, $absolute = TRUE): ?string {
    return $this->manager->getPath($type, $name, $absolute);
  }

  /**
   * Returns gridstacks object.
   */
  protected function gridstacks(array $settings) {
    return $this->manager->gridstacks($settings);
  }

}
