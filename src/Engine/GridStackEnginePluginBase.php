<?php

namespace Drupal\gridstack\Engine;

use Drupal\Component\Serialization\Json;
use Drupal\blazy\Blazy;
use Drupal\gridstack\GridStackDefault as Defaults;
use Drupal\gridstack\GridStackPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides base class for all gridstack layout engines.
 */
abstract class GridStackEnginePluginBase extends GridStackPluginBase implements GridStackEnginePluginInterface {

  /**
   * The prefix class dependent on framework/ versions: col, cell, columns, etc.
   *
   * @var string
   */
  protected $colClass = 'col';

  /**
   * The last prefix dependent on framework/ versions: col-, large, etc.
   *
   * @var string
   */
  protected $colPrefix = 'col-';

  /**
   * The layout sizes.
   *
   * @var array
   */
  protected $sizes = [];

  /**
   * The layout CSS classes for options.
   *
   * @var array
   */
  protected $classOptions = [];

  /**
   * The layout CSS row classes for options.
   *
   * @var array
   */
  protected $rowClassOptions = [];

  /**
   * The container classes, actually refers to row classes, not the outmost.
   *
   * @var array
   */
  protected $containerClasses = [];

  /**
   * The nested container classes.
   *
   * @var array
   */
  protected $nestedContainerClasses = [];

  /**
   * The item classes, .box.
   *
   * @var array
   */
  protected $itemClasses = [];

  /**
   * The item content classes, .box__content.
   *
   * @var array
   */
  protected $itemContentClasses = [];

  /**
   * The admin regions.
   *
   * @var array
   */
  protected $regions = [];

  /**
   * The above-fold CSS inline styles as recommended by lighthouse.
   *
   * @var array
   */
  protected $styles = [];

  /**
   * The stylizer service.
   *
   * @var \Drupal\gridstack\Style\GridStackStylizerInterface
   */
  protected $stylizer;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition,
  ) {
    $instance = parent::create(
      $container,
      $configuration,
      $plugin_id,
      $plugin_definition
    );
    $instance->stylizer = $container->get('gridstack.stylizer');
    $instance->sizes = $instance->setSizes(Defaults::breakpoints());
    $instance->classOptions = $instance->setClassOptions();
    $instance->rowClassOptions = $instance->setRowClassOptions();

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function classOptions(): array {
    return $this->classOptions;
  }

  /**
   * Sets the plugin engine classes for options, container or item, hard-coded.
   */
  protected function setClassOptions() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function rowClassOptions(): array {
    return $this->rowClassOptions;
  }

  /**
   * Sets the optional plugin engine classes for options, row, hard-coded.
   */
  protected function setRowClassOptions() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function containerClasses(): array {
    return $this->containerClasses;
  }

  /**
   * Sets the optional plugin engine container classes, configurable.
   */
  protected function setContainerClasses(array $classes = []) {
    $this->containerClasses = array_merge($this->containerClasses, $classes);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function sizes(): array {
    return $this->sizes;
  }

  /**
   * Sets the sizes.
   */
  protected function setSizes(array $sizes) {
    return $sizes;
  }

  /**
   * {@inheritdoc}
   */
  public function styles(): array {
    return $this->styles;
  }

  /**
   * Sets the styles, might be string, or array.
   */
  protected function setStyles(array $data) {
    $id = $this->getPluginId() . '-' . $this->getOptionset()->id();
    $this->styles[$this->manager->getHtmlId('gridstack', $id)][$data['context']] = $data['rules'];
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function attach(array &$load, array $attach, $config): void {
    parent::attach($load, $attach, $config);

    if ($config->get('ui.debug')) {
      $load['library'][] = 'gridstack/debug';
    }
  }

  /**
   * {@inheritdoc}
   */
  public function build(array &$build, array &$element): void {
    $this->manager->hashtag($build);
    $this->manager->hashtag($build, 'optionset');

    $settings = &$build['#settings'];

    // Allows a layout variant to modify the established optionset.
    // Since render array output is cached, nothing to lose, just flexibility.
    if (!$this->optionset || !empty($settings['_variant'])) {
      $this->setOptionset($build['#optionset']);
    }

    // Defines stuffs early to be processed by ::buildItems.
    $this->prepare($element, $settings);
    $contentless = $this->stylizer->getStyle('contentless', $settings);
    $element['#items'] = $contentless ? [] : $this->buildItems($build);

    // Attached layout inline styles if so configured.
    // Runs after ::buildItems to aggregate styles.
    if ($styles = $this->styles()) {
      $this->stylizer->rootStyles($element, $styles, $settings);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getIconBreakpoint(): string {
    $keys = array_keys($this->sizes);
    return end($keys);
  }

  /**
   * {@inheritdoc}
   */
  public function getSmallestBreakpoint(): string {
    $keys = array_keys($this->sizes);
    return $keys[0];
  }

  /**
   * Prepares the settings, selector and active styles.
   */
  private function prepare(array &$element, array &$settings) {
    $config     = $settings['gridstacks'];
    $attributes = $content_attributes = [];
    $settings   = array_merge($settings, $this->stylizer->getStyle('all', $settings));
    $selector   = '.gridstack--' . str_replace('_', '-', $settings['optionset']);

    // @todo remove.
    $settings['_level'] = Defaults::LEVEL_ROOT;
    $settings['rid'] = $settings['_context'] = Defaults::ROOT;

    $config->set('_level', Defaults::LEVEL_ROOT)
      ->set('_context', Defaults::ROOT)
      ->set('rid', Defaults::ROOT);

    // Unique class per layout which can be many similar on a page.
    if ($gid = $settings['gid'] ?? NULL) {
      $selector .= '.is-gs-' . str_replace(['_', ':'], '-', $gid);
    }

    // @todo $settings['_ungrid'] = !$config->is('ipe') && $this->stylizer->getStyle('parallax', $settings);
    if ($check = $this->containerClasses()[0] ?? NULL) {
      $selector .= '.' . $check;
    }

    $settings['_selector'] = $selector;
    $config->set('_selector', $selector);
    $this->setConfiguration($settings);

    // Provides attributes and background media if so configured.
    // Runs before ::containerAttributes to pass the media settings.
    $this->stylizer->prepare($element, $attributes, $settings, $this->getOptionset());

    // Runs after ::media to process the media settings.
    $this->containerAttributes($attributes, $settings);
    $this->contentAttributes($content_attributes, $settings);

    // Pass attributes and items to templates.
    $_attrs  = '#attributes';
    $_cattrs = '#content_attributes';
    $_sets   = '#settings';

    $element[$_attrs]  = $this->manager->merge($attributes, $element, $_attrs);
    $element[$_cattrs] = $this->manager->merge($content_attributes, $element, $_cattrs);
    $element[$_sets]   = $this->manager->merge($settings, $element, $_sets);
  }

  /**
   * {@inheritdoc}
   */
  public function buildItems(array $build): array {
    $settings = $this->manager->toHashtag($build);
    $settings = array_diff_key($settings, Defaults::regionSettings());
    $grids    = $this->optionset->getLastBreakpoint();
    $items    = [];
    $regions  = $this->optionset->prepareRegions(FALSE);

    // Cleans up top level settings.
    foreach ($build['items'] as $delta => $item) {
      // Skips if more than we can chew, otherwise broken grid anyway.
      if (!isset($grids[$delta])) {
        continue;
      }

      $rid     = Defaults::regionId($delta);
      $region  = $regions[$rid] ?? [];
      $subsets = $this->manager->toHashtag($item);
      $subsets = $this->manager->mergeSettings(['blazies', 'gridstacks'], $settings, $subsets);
      $config  = $this->manager->reset($subsets, TRUE);

      $config->set('region', $region)
        ->set('rid', $rid)
        ->set('delta', $delta)
        ->set('root_delta', $delta);

      foreach (['_level', '_context', '_root'] as $key) {
        $config->set($key, $region[$key] ?? FALSE);
      }

      if ($subregions = $config->get('lb.regions')) {
        $config->set('region.config', $subregions[$rid]);
      }

      $subsets['gridstacks'] = $config;
      $items[] = $this->buildItem($delta, $subsets, $item, $regions);
    }

    return $items;
  }

  /**
   * Provides nested items if so configured.
   */
  protected function buildNestedItems($delta, array &$settings, array $item, array $grids, array $regions = []) {
    $items = [];
    $index = ($delta + 1);
    $gridstack = $settings['gridstacks'];

    foreach (array_keys($grids) as $gid) {
      $rid     = Defaults::regionId($delta . '_' . $gid);
      $region  = $regions[$rid] ?? [];
      $nested  = $item[$gid] ?? [];
      $subsets = $this->manager->toHashtag($nested);
      $subsets = $this->manager->mergeSettings(['blazies', 'gridstacks'], $settings, $subsets);
      $config  = $this->manager->reset($subsets, TRUE);

      $config->set('region', $region)
        ->set('rid', $rid)
        ->set('nested.delta', $gid)
        ->set('nested.id', $region['_context'] ?? NULL)
        ->set('use.inner', TRUE);

      foreach (['_level', '_context', '_root'] as $key) {
        $config->set($key, ($region[$key] ?? FALSE));
      }

      if ($subregions = $config->get('lb.regions', [])) {
        if ($subregion = $subregions[$rid] ?? NULL) {
          $config->set('region.config', $subregion);
        }
      }

      $subsets['gridstacks'] = $config;
      $items[] = $this->buildItem($gid, $subsets, $nested, $regions);
    }

    // Provides nested gridstack, gridstack within gridstack, if so configured.
    $ungrid = $gridstack->is('ungrid') || !empty($settings['_ungrid']);
    $gridstack->set('_root', 'grids')
      ->set('_level', Defaults::LEVEL_NESTED)
      ->set('_context', Defaults::NESTED . $index)
      ->set('ungrid', $ungrid);

    // Update box with nested boxes.
    return [
      '#theme'      => 'gridstack',
      '#items'      => $items,
      '#optionset'  => $this->optionset,
      '#settings'   => $settings,
      '#attributes' => $this->nestedContainerAttributes($settings),
    ];
  }

  /**
   * Modifies item content and attributes.
   */
  protected function modifyItem(
    $delta,
    array &$settings,
    array &$content,
    array &$attributes,
    array &$content_attributes,
    array $regions = [],
  ) {
    $config = $settings['gridstacks'];
    $contentless = $this->stylizer->getStyle('contentless', $settings);

    $config->set('is.contentless', $contentless);

    $this->itemAttributes($attributes, $settings);
    $this->itemContentAttributes($content_attributes, $settings);

    // Provides nested items if so configured.
    if ($config->get('use.nested')) {
      $this->modifyNestedItem($delta, $settings, $content, $attributes, $content_attributes, $regions);
    }

    // Allows stylizer to modify contents and attributes.
    if ($config->get('use.stylizer')) {
      $this->stylizer->modifyItem($delta, $settings, $content, $attributes, $content_attributes, $regions);
    }
  }

  /**
   * Modifies nested item contents and attributes.
   */
  protected function modifyNestedItem(
    $delta,
    array &$settings,
    array &$content,
    array &$attributes,
    array &$content_attributes,
    array $regions = [],
  ) {
    // Provides configurable content attributes via Layout Builder.
    $this->attributes($content_attributes, $settings);
    $config = $settings['gridstacks'];

    // Nested grids with preserved indices even if empty so to layout.
    // Only CSS Framework has nested grids for now, not js-driven layouts.
    if (isset($content['box'][0], $content['box'][0]['box'])
      && $nesteds = $this->optionset->getNestedGridsByDelta($delta)) {

      $config->set('is.nested', TRUE)
        ->set('is.root', FALSE)
        ->set('use.inner', FALSE);

      // Overrides with a sub-theme_gridstack() containing nested boxes.
      $content['box'] = $this->buildNestedItems($delta, $settings, $content['box'], $nesteds, $regions);
      $attributes['class'][] = 'box--nester';
    }

    if ($config->get('_root') == 'nested') {
      $attributes['class'][] = 'box--nested';
    }
  }

  /**
   * Returns an individual item.
   */
  protected function buildItem(
    $delta,
    array &$settings,
    array $item = [],
    array $regions = [],
  ) {
    $config = $settings['gridstacks'];
    $dummy = $config->is('dummy');
    $use_inner = $config->get('use.inner');

    $config->set('is.dummy', $dummy)
      ->set('use.inner', $use_inner);

    unset($settings['_dummy']);

    $content_attributes = [];
    $attributes = $this->manager->toHashtag($item, 'attributes');
    unset($item['attributes']);

    // @todo recheck upstream why paragraphs empty, specific for vanilla.
    // See https://www.drupal.org/project/gridstack/issues/3080356.
    $fallback = isset($item['#settings']) ? [$item] : [];

    $content = [
      'box'     => $this->manager->toHashtag($item, 'box') ?: $fallback,
      'caption' => $this->manager->toHashtag($item, 'caption'),
      'preface' => $this->manager->toHashtag($item, 'preface'),
    ];

    if (!$dummy) {
      $this->modifyItem($delta, $settings, $content, $attributes, $content_attributes, $regions);
    }

    return [
      '#theme'              => 'gridstack_box',
      '#item'               => $content,
      '#delta'              => $delta,
      '#attributes'         => $attributes,
      '#content_attributes' => $content_attributes,
      '#settings'           => $settings,
    ];
  }

  /**
   * Modifies the .box attributes.
   */
  protected function itemAttributes(array &$attributes, array &$settings) {
    $this->setConfiguration($settings);
    $config = $settings['gridstacks'];

    $classes = (array) ($attributes['class'] ?? []);
    $attributes['class'] = $classes
      ? array_merge($classes, $this->itemClasses)
      : $this->itemClasses;

    if ($config->is('ipe') && $config->get('_level') != Defaults::LEVEL_NESTED) {
      $attributes['data-gslb-region'] = $config->get('rid');
    }

    // Provides configurable content attributes via Layout Builder.
    // @todo recheck contentless.
    $this->attributes($attributes, $settings);

    // Provides configurable item attributes.
    if ($this->stylizer->getStyle('ete', $settings)) {
      $attributes['class'][] = 'box--ete';
    }
  }

  /**
   * Modifies the .box__content attributes.
   */
  protected function itemContentAttributes(array &$attributes, array &$settings) {
    $classes = (array) ($attributes['class'] ?? []);
    $attributes['class'] = $classes
      ? array_merge($classes, $this->itemContentClasses)
      : $this->itemContentClasses;
  }

  /**
   * Returns the .gridstack container attributes.
   */
  public function containerAttributes(array &$attributes, array &$settings) {
    $config = $settings['gridstacks'];

    $attributes['class'] = $config->is('ungrid') ? [] : $this->containerClasses();

    // Provides configurable attributes via Layout Builder.
    $this->attributes($attributes, $settings);

    // Add debug class for admin usages.
    if ($config->get('ui.debug') || $config->is('ipe')) {
      $attributes['class'][] = 'is-gs-debug';

      if ($config->is('ipe')) {
        $attributes['class'][] = 'is-gs-lb';
        $attributes['data-gslb-region'] = Defaults::ROOT;

        // Prevents parallax from messing around the admin page.
        // We'll toggle as needed via JS if doable instead.
        if (in_array('is-gs-parallax', $attributes['class'])) {
          $removed = ['is-gs-parallax', 'is-gs-parallax-fs'];
          $attributes['class'] = array_diff($attributes['class'], $removed);
        }
      }
      if ($config->is('lbux')) {
        $attributes['class'][] = 'is-gs-lbux';
      }
    }

    // Empty it after being processed to not leak to children due to similarity.
    unset(
      $settings['attributes'],
      $settings['wrapper_classes'],
      $settings['fw_classes']
    );

    // Unique class per layout which can be many similar on a page.
    if ($gid = $this->getSetting('gid')) {
      $attributes['class'][] = 'is-gs-' . str_replace(['_', ':'], '-', $gid);
    }

    // Unique class per layout variant which can be many similar on a page.
    if ($variant = $this->getSetting('_variant')) {
      $attributes['class'][] = $this->stylizer->style()->getVariantClass($variant);
    }

    // Adds .blazy container to manage lazy load, or lightboxes per layout.
    Blazy::containerAttributes($attributes, $settings);

    // Provides configurable item attributes.
    if ($this->stylizer->getStyle('parallax-fs', $settings)) {
      $attributes['class'][] = 'is-b-scroll';

      // @todo remove post blazy:2.1+.
      // Old bLazy, not IO, needs scrolling container to lazyload correctly.
      // With fullscreen parallax, the body overflow is hidden for parallax
      // container own scrollbar, hence we need to provide the option.
      $blazy = empty($attributes['data-blazy']) ? [] : Json::decode($attributes['data-blazy']);
      $blazy['container'] = '.is-b-scroll';
      $attributes['data-blazy'] = Json::encode($blazy);
    }
  }

  /**
   * Provides the .gridstack__inner container attributes.
   *
   * @todo this is not implemented, yet, only advanced usages with ungridding.
   * The idea is to destroy all grids, and still take advantage of GridStack
   * advanced and flexible stylings such as at Layout Builder pages. One sample
   * valid usage is a parallax page which doesn't need grids, but needs styling.
   * Since this introduces complications and requires custom theming, no UI is
   * provided for now. Anyone who knows theming should be easily adding _ungrid
   * via a hook_alter, and theme away as needed.
   */
  protected function contentAttributes(array &$attributes, array &$settings) {
    $config = $settings['gridstacks'];
    if ($config->is('ungrid')
      && $config->get('_level') == Defaults::LEVEL_ROOT
      && $config->use('framework')) {
      // @todo $attributes['class'] = $this->containerClasses();
      $attributes['class'][] = 'gridstack__inner';
    }
  }

  /**
   * Returns the .gridstack nested container attributes.
   */
  protected function nestedContainerAttributes(array &$settings) {
    $config = $settings['gridstacks'];
    $attributes['class'] = $this->nestedContainerClasses;

    $index = $config->get('delta', 0) + 1;

    // Required by dynamic styling.
    $attributes['class'][] = 'gridstack--' . $index;

    // Optional, not really important.
    if (!$config->get('ui.no_classes')) {
      $attributes['class'][] = 'gridstack--nested';
    }

    // Provides configurable attributes via Layout Builder.
    $this->attributes($attributes, $settings);

    // This one is a container, not a registered region for layouts. Yet
    // provides the consistent selectors for editor live preview styling.
    if ($config->is('ipe') && $config->get('_level') == Defaults::LEVEL_NESTED) {
      $attributes['data-gslb-region'] = $config->get('rid');
    }
    return $attributes;
  }

  /**
   * Provides both CSS grid and js-driven attributes configurable via UI.
   *
   * This is the root container containing `.gridstack` element.
   */
  protected function attributes(array &$attributes, array $settings) {
    $config = $settings['gridstacks'];

    // Bail out if not configurable via Layout Builder alike.
    if ($config->get('use.stylizer')) {
      $this->stylizer->attributes($attributes, $settings);

      // Provides styles per item to be aggregated at top level.
      if ($styles = $this->stylizer->styles($attributes, $settings)) {
        $this->setStyles($styles);
      }
    }
  }

  /**
   * Returns the module feature CSS classes, not available at CSS frameworks.
   */
  protected function getVersionClasses() {
    return $this->stylizer->getInternalClasses();
  }

  /**
   * Returns options which make sense for preview at Layout Builder page.
   */
  public function previewOptions() {
    return [];
  }

}
