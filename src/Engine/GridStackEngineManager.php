<?php

namespace Drupal\gridstack\Engine;

use Drupal\gridstack\GridStackPluginManagerBase;

/**
 * Provides GridStack engine manager.
 *
 * @todo recheck https://github.com/mglaman/phpstan-drupal/issues/113
 * @phpstan-ignore-next-line
 */
class GridStackEngineManager extends GridStackPluginManagerBase implements GridStackEngineManagerInterface {

  /**
   * {@inheritdoc}
   */
  protected static $path = 'Plugin/gridstack/engine';

  /**
   * {@inheritdoc}
   */
  protected static $interface = 'Drupal\gridstack\Engine\GridStackEnginePluginInterface';

  /**
   * {@inheritdoc}
   */
  protected static $annotation = 'Drupal\gridstack\Annotation\GridStackEngine';

  /**
   * {@inheritdoc}
   */
  protected static $key = 'gridstack_engine';

  /**
   * The active CSS framework.
   *
   * @var object
   */
  protected $framework;

  /**
   * The gridstack layout CSS classes applicable to .row or .box__content.
   *
   * @var array
   */
  protected $classOptions;

  /**
   * {@inheritdoc}
   */
  public function attach(array &$load, array $attach, $config): void {
    if ($config->get('engine')) {
      $this->checkAttachments($load, $attach, $config);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function framework(array $config = []): ?object {
    if (!isset($this->framework)) {
      if ($framework = $this->config('framework')) {
        $this->framework = $this->load($framework, $config);
      }
    }
    return $this->framework;
  }

  /**
   * {@inheritdoc}
   */
  public function getClassOptions($type = 'generic', array $config = []): array {
    if (!isset($this->classOptions[$type])) {
      if ($framework = $this->framework($config)) {
        $this->classOptions[$type] = $type == 'row'
          ? $framework->rowClassOptions()
          : $framework->classOptions();
      }
    }
    return $this->classOptions[$type] ?: [];
  }

}
