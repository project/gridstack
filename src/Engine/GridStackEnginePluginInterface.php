<?php

namespace Drupal\gridstack\Engine;

use Drupal\gridstack\GridStackPluginInterface;

/**
 * Provides an interface defining GridStack layout engines.
 */
interface GridStackEnginePluginInterface extends GridStackPluginInterface {

  /**
   * Alters GridStack build.
   *
   * @param array $build
   *   An associative array containing:
   *   - items: An array of gridstack contents: text, image or media.
   *   - options: An array of key:value pairs of custom JS options.
   *   - optionset: The cached optionset object to avoid multiple invocations.
   *   - settings: An array of key:value pairs of HTML/layout related settings.
   * @param array $element
   *   The render element being modified.
   */
  public function build(array &$build, array &$element): void;

  /**
   * Returns the layout engine classes for select options.
   *
   * @return array
   *   The array of the defined classes.
   */
  public function classOptions(): array;

  /**
   * Returns the layout engine container classes.
   *
   * @return array
   *   The array of the container classes.
   */
  public function containerClasses(): array;

  /**
   * Returns the layout engine classes for select options.
   *
   * @return array
   *   The array of the defined row class options.
   */
  public function rowClassOptions(): array;

  /**
   * Builds GridStack boxes to support nested grids for Bootstrap/ Foundation.
   *
   * The nested grids require extra tools like DS, Panelizer, or Widget, to
   * arrange them into their relevant container, e.g.: DS region, Widget block.
   *
   * @param array $build
   *   An associative array containing:
   *   - items: An array of gridstack contents: text, image or media.
   *   - options: An array of key:value pairs of custom JS options.
   *   - optionset: The cached optionset object to avoid multiple invocations.
   *   - settings: An array of key:value pairs of HTML/layout related settings.
   *
   * @return array
   *   The renderable array of a GridStack instance, or empty array.
   */
  public function buildItems(array $build): array;

  /**
   * Returns available sizes.
   */
  public function sizes(): array;

  /**
   * Returns available styles.
   */
  public function styles(): array;

  /**
   * Return the icon breakpoint to generate icon from.
   *
   * Foundation or Bootstrap 3 has `lg` for the largest.
   * Bootstrap 4 and all js|css-driven has `xl` for the largest.
   */
  public function getIconBreakpoint(): string;

  /**
   * Returns the smallest breakpoint, xs or sm.
   */
  public function getSmallestBreakpoint(): string;

}
