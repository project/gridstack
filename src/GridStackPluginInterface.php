<?php

namespace Drupal\gridstack;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Provides an interface defining GridStack plugins.
 */
interface GridStackPluginInterface extends ConfigurableInterface, ContainerFactoryPluginInterface {

  /**
   * Returns the plugin label.
   *
   * @return string
   *   The plugin label.
   */
  public function label();

  /**
   * Provides gridstack skins and libraries.
   */
  public function attach(array &$load, array $attach, $config): void;

}
