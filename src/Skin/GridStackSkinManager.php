<?php

namespace Drupal\gridstack\Skin;

use Drupal\gridstack\GridStackPluginManagerBase;

/**
 * Provides GridStack skin manager.
 *
 * @todo recheck https://github.com/mglaman/phpstan-drupal/issues/113
 * @phpstan-ignore-next-line
 */
class GridStackSkinManager extends GridStackPluginManagerBase implements GridStackSkinManagerInterface {

  /**
   * Static cache for the skin definition.
   *
   * @var array
   */
  protected $skinDefinition;

  /**
   * Static cache for the skin options.
   *
   * @var array
   */
  protected $skinOptions;

  /**
   * The library info definition.
   *
   * @var array
   */
  protected $libraryInfoBuild;

  /**
   * {@inheritdoc}
   */
  public function getSkins(): array {
    if (!isset($this->skinDefinition)) {
      $this->skinDefinition = $this->getData(['skins'], TRUE);
    }
    return $this->skinDefinition ?: [];
  }

  /**
   * {@inheritdoc}
   */
  public function getSkinOptions(): array {
    if (!isset($this->skinOptions)) {
      $this->skinOptions = $this->getDataOptions($this->getSkins());
    }
    return $this->skinOptions ?: [];
  }

  /**
   * {@inheritdoc}
   */
  public function attach(array &$load, array $attach, $config): void {
    // Skins may be available for JS, or CSS layouts, or even ungridstack.
    if ($skin = $attach['skin'] ?? NULL) {
      $skins = $this->getSkins();
      $provider = $skins[$skin]['provider'] ?? 'gridstack';
      $load['library'][] = 'gridstack/' . $provider . '.' . $skin;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function libraryInfoBuild(): array {
    if (!isset($this->libraryInfoBuild)) {
      $libraries = [];
      foreach ($this->getSkins() as $key => $skin) {
        $provider = $skin['provider'] ?? 'gridstack';
        $id = $provider . '.' . $key;

        foreach (['css', 'js', 'dependencies'] as $property) {
          if (isset($skin[$property]) && is_array($skin[$property])) {
            $libraries[$id][$property] = $skin[$property];
          }
        }
        $libraries[$id]['dependencies'][] = 'gridstack/skin';
      }

      foreach (range(1, 12) as $key) {
        $libraries['gridstack.' . $key] = [
          'css' => [
            'layout' => ['css/layout/grid-stack-' . $key . '.css' => []],
          ],
          'dependencies' => ['gridstack/library'],
        ];
      }

      $this->libraryInfoBuild = $libraries;
    }
    return $this->libraryInfoBuild ?: [];
  }

}
