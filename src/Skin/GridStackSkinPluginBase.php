<?php

namespace Drupal\gridstack\Skin;

use Drupal\gridstack\GridStackPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides base class for all gridstack skins.
 */
abstract class GridStackSkinPluginBase extends GridStackPluginBase implements GridStackSkinPluginInterface {

  /**
   * The gridstack skin definitions.
   *
   * @var array
   */
  protected $skins;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition,
  ) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);

    $instance->skins = $instance->setSkins();
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function skins() {
    return $this->skins;
  }

  /**
   * Sets the required plugin skins.
   */
  abstract protected function setSkins();

}
