<?php

namespace Drupal\gridstack;

use Drupal\Core\Render\Element;
use Drupal\blazy\BlazyManagerBase;
use Drupal\gridstack\Engine\GridStackEngineManagerInterface;
use Drupal\gridstack\Entity\GridStack;
use Drupal\gridstack\Skin\GridStackSkinManagerInterface;
use Drupal\gridstack\Style\GridStackStylizerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides GridStack manager.
 */
class GridStackManager extends BlazyManagerBase implements GridStackManagerInterface {

  /**
   * {@inheritdoc}
   */
  protected static $namespace = 'gridstack';

  /**
   * {@inheritdoc}
   */
  protected static $itemId = 'box';

  /**
   * {@inheritdoc}
   */
  protected static $itemPrefix = 'box';

  /**
   * The GridStack layout engine.
   *
   * @var \Drupal\gridstack\Engine\GridStackEnginePluginInterface
   */
  protected $engine;

  /**
   * The GridStack layout engine plugin ID.
   *
   * @var string
   */
  protected $engineId;

  /**
   * The GridStack optionset.
   *
   * @var \Drupal\gridstack\Entity\GridStack
   */
  protected $optionset;

  /**
   * The GridStack layout engine manager service.
   *
   * @var \Drupal\gridstack\Engine\GridStackEngineManagerInterface
   */
  protected $engineManager;

  /**
   * The GridStack skin manager service.
   *
   * @var \Drupal\gridstack\Skin\GridStackSkinManagerInterface
   */
  protected $skinManager;

  /**
   * The GridStack stylizer service.
   *
   * @var \Drupal\gridstack\Style\GridStackStylizerInterface
   */
  protected $stylizer;

  /**
   * The gridstack layout CSS classes.
   *
   * @var array
   */
  protected $mergedClasses;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->setEngineManager($container->get('gridstack.engine_manager'));
    $instance->setSkinManager($container->get('gridstack.skin_manager'));
    $instance->setStylizer($container->get('gridstack.stylizer'));
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks() {
    return ['preRenderGridStack'];
  }

  /**
   * {@inheritdoc}
   */
  public function engineManager() {
    return $this->engineManager;
  }

  /**
   * {@inheritdoc}
   */
  public function setEngineManager(GridStackEngineManagerInterface $engine_manager) {
    $this->engineManager = $engine_manager;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function skinManager() {
    return $this->skinManager;
  }

  /**
   * {@inheritdoc}
   */
  public function setSkinManager(GridStackSkinManagerInterface $skin_manager) {
    $this->skinManager = $skin_manager;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function stylizer() {
    return $this->stylizer;
  }

  /**
   * {@inheritdoc}
   */
  public function setStylizer(GridStackStylizerInterface $stylizer) {
    $this->stylizer = $stylizer;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getEngineId() {
    if (!isset($this->engineId)) {
      $this->engineId = 'gridstack_js';
    }
    return $this->engineId;
  }

  /**
   * {@inheritdoc}
   */
  protected function setEngineId($engine_id) {
    $this->engineId = $engine_id;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getEngine(array $settings, $engine = NULL): ?object {
    if (!isset($this->engine) || $engine) {
      if (empty($engine)) {
        $config = $settings['gridstacks'];
        $engine = $config->get('engine');
        $engine = $engine ?: $this->getEngineId();
      }
      $this->engine = $this->engineManager->load($engine, $settings);
    }
    return $this->engine;
  }

  /**
   * {@inheritdoc}
   */
  public function getMergedClasses($flatten = FALSE): array {
    if (!isset($this->mergedClasses[$flatten])) {
      $classes = $this->engineManager->getClassOptions();
      $this->mergedClasses[$flatten] = $this->stylizer->getMergedClasses($flatten, $classes);
    }
    return $this->mergedClasses[$flatten] ?: [];
  }

  /**
   * {@inheritdoc}
   */
  public function engineSettings(array &$settings, $optionset = NULL): void {
    $this->optionset = $optionset ?: $this->optionset;
    $this->prepareSettings($settings);
  }

  /**
   * {@inheritdoc}
   */
  public function gridstacks(array &$settings) {
    $this->verifySafely($settings);
    return $settings['gridstacks'];
  }

  /**
   * {@inheritdoc}
   */
  public function loadSafely($name): GridStack {
    return GridStack::loadSafely($name);
  }

  /**
   * {@inheritdoc}
   *
   * @todo add return type hint void after outlayer.
   */
  public function prepareSettings(array &$settings) {
    $this->verifySafely($settings);

    $blazies     = $settings['blazies'];
    $gridstacks  = $settings['gridstacks'];
    $optionset   = $this->optionset;
    $framework   = $settings['framework'] ?? '';
    $engine      = $settings['_engine'] ?? '';
    $engine      = $gridstacks->get('engine', $engine);
    $gridnative  = $settings['gridnative'] ?? FALSE;
    $gridnative  = $gridstacks->use('gridnative', $gridnative);
    $column      = $optionset->getSetting('column', 12);
    $ungridstack = $settings['ungridstack'] ?? FALSE;
    $ungridstack = $ungridstack ?: $gridstacks->is('ungridstack');

    // The use_nested is not for js or native Grid, _yet, but CSS framework.
    $use_framework = $framework && $optionset->getOption('use_framework');
    $use_native    = !$use_framework && $gridnative;

    // Provides configurable layout engines.
    if ($use_framework) {
      $settings['background'] = $use_gridstack = $use_js = FALSE;
      $engine = $framework;
    }
    else {
      // Provides programmatic layout engines.
      $use_js = TRUE;
      $use_gridstack = !$ungridstack;
      $fallback = $use_native ? 'gridstack_native' : 'gridstack_js';
      $engine = $engine ?: $fallback;
    }

    $id = $this->getHtmlId('gridstack-' . $settings['optionset'] . '-' . $engine, $settings['id'] ?? '');

    $blazies->set('css.id', $id);

    $gridstacks->set('id', $id)
      ->set('column', $column)
      ->set('engine', $engine)
      ->set('is.debug', $gridstacks->ui('debug'))
      ->set('is.ungridstack', $ungridstack)
      ->set('use.framework', $use_framework)
      ->set('use.gridstack', $use_gridstack)
      ->set('use.gridnative', $use_native)
      // @fixme v5.1.1 ->set('use.gridstatic', $gridstacks->ui('gridstatic'))
      ->set('use.gridstatic', FALSE)
      ->set('use.js', $use_js)
      ->set('use.inner', $use_js)
      ->set('use.nested', $use_framework)
      ->set('was.settings', TRUE);

    // @todo remove after migration.
    $settings['column'] = $column;
    $settings['_engine'] = $engine;
    $settings['id'] = $id;
    $settings['_gridstack'] = $use_gridstack;
    $settings['gridnative'] = $use_native;
    $settings['use_js'] = $use_js;
    $settings['use_nested'] = $settings['use_framework'] = $use_framework;

    $this->setEngineId($engine);
  }

  /**
   * Returns the wrapper attributes, empty to allow overrides.
   *
   * @todo add return type hint void after outlayer.
   */
  protected function prepareAttributes(array &$build) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function adminSettings(array &$settings, array $element = []): void {
    $this->verifySafely($settings);
    $this->preSettings($settings);

    $blazies    = $settings['blazies'];
    $gridstacks = $settings['gridstacks'];
    $route      = $blazies->get('route_name');
    $attributes = $element['#attributes'] ?? [];
    $prefix     = $element['#prefix'] ?? '';
    $id1        = 'data-layout-update-url';
    $lb         = strpos($route, 'layout_builder') !== FALSE || isset($attributes[$id1]);
    $lbux       = $lb && $this->moduleExists('lb_ux');
    $panels     = $prefix && strpos($prefix, 'panels-ipe-content') !== FALSE;

    // Detects layout builder, lbux, or panels.
    $infos = [
      'lb'     => $lb,
      'lbux'   => $lbux,
      'panels' => $panels,
      'ipe'    => $lb || $lbux || $panels,
    ];

    foreach ($infos as $key => $value) {
      $gridstacks->set('is.' . $key, $value);

      // @todo remove after migration.
      $settings['_' . $key] = $value;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function build(array $build): array {
    $hashtags = array_keys(GridStackDefault::hashedProperties());
    foreach (GridStackDefault::themeProperties() as $key => $default) {
      $k = in_array($key, $hashtags)
        ? "#$key" : $key;
      $build[$k] = $this->toHashtag($build, $key, $default);
    }

    $gridstack = [
      '#theme'      => 'gridstack',
      '#build'      => $build,
      '#pre_render' => [[$this, 'preRenderGridStack']],
      'items'       => [],
      '#layout'     => $build['#layout'],
    ];

    $this->moduleHandler->alter('gridstack_build', $gridstack, $build['#settings']);
    return empty($build['items']) ? [] : $gridstack;
  }

  /**
   * {@inheritdoc}
   */
  public function getUiSettings(): array {
    return array_intersect_key($this->configMultiple('gridstack.settings'), GridStackDefault::cleanUiSettings());
  }

  /**
   * {@inheritdoc}
   */
  public function preRenderGridStack($element): array {
    $build = $element['#build'];
    unset($element['#build']);

    // Build GridStack elements.
    $this->prepareBuild($build, $element);
    $settings = &$build['#settings'];

    // Provides cache, attributes, assets.
    $attributes = $this->prepareAttributes($build);

    // Adds the required elements for the template.
    $element['#optionset']  = $this->optionset;
    $element['#settings']   = $this->merge($settings, $element, '#settings');
    $element['#postscript'] = $this->merge($build['postscript'], $element, '#postscript');
    $element['#attributes'] = $this->merge($attributes, $element, '#attributes');

    $this->setAttachments($element, $settings);

    // Layout Builder is happy, safe to free up wasted children.
    foreach (Element::children($element) as $child) {
      unset($element[$child]);
    }

    unset($build);
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function reset(array &$settings, $refresh = FALSE, $key = 'gridstacks') {
    $this->verifySafely($settings);
    $this->toSettings($settings, []);

    // The settings instance must be unique per item.
    $object = $settings[$key];
    if (!$object->was('reset') || $refresh) {
      $object->reset($settings, $key);
      $object->set('was.reset', TRUE);
    }

    return $object;
  }

  /**
   * {@inheritdoc}
   */
  public function toBlazy(array &$data, array &$captions, $delta): void {
    $settings = $this->toHashtag($data);
    $this->verifySafely($settings);

    $blazies = $settings['blazies'];
    $config  = $settings['gridstacks'];
    $prefix  = 'box';

    $blazies->set('item.id', $prefix)
      ->set('item.prefix', $prefix);

    if ($config->use('inner')) {
      $data['#wrapper_attributes']['class'][] = $prefix . '__content';
      if ($attrs = $blazies->get('item.wrapper_attributes', [])) {
        $data['#wrapper_attributes'] = $this->merge($data['#wrapper_attributes'], $attrs);
      }
    }

    parent::toBlazy($data, $captions, $delta);
  }

  /**
   * {@inheritdoc}
   */
  public function verifySafely(array &$settings, $key = 'blazies', array $defaults = []) {
    GridStackDefault::verify($settings);

    return parent::verifySafely($settings, $key, $defaults);
  }

  /**
   * {@inheritdoc}
   */
  protected function attachments(array &$load, array $attach, $blazies): void {
    parent::attachments($load, $attach, $blazies);
    $this->verifySafely($attach);

    // @todo add gridstacks object.
    if (isset($this->engineManager)) {
      $config = $attach['gridstacks'];
      $this->engineManager->attach($load, $attach, $config);
      $this->skinManager->attach($load, $attach, $config);
      $this->stylizer->attach($load, $attach, $config);
    }

    $this->moduleHandler->alter('gridstack_attach', $load, $attach, $blazies);
  }

  /**
   * Prepares the layout engine.
   */
  protected function initEngine(array &$build, array &$element) {
    $settings = &$build['#settings'];
    $config = $settings['gridstacks'];

    $this->setEngine($settings, $config->get('engine'));
    $this->engine->build($build, $element);
  }

  /**
   * Prepares GridStack build.
   */
  protected function prepareBuild(&$build, array &$element) {
    $settings = &$build['#settings'];

    // Supports Blazy multi-breakpoint images if provided.
    if ($image = ($build['items'][0] ?? NULL)) {
      if (is_array($image)) {
        $this->isBlazy($settings, $image);
      }
    }

    // Provides the optionset if empty. GridStackLayout, or GridStackViews, may
    // already set this. GridStackFormatter used to set it, too.
    $this->optionset = GridStack::verifyOptionset($build, $settings['optionset']);

    // Prepares the settings, and init layout engine.
    $this->adminSettings($settings, $element);
    $this->engineSettings($settings);
    $this->initEngine($build, $element);
  }

  /**
   * Sets the layout engine.
   */
  protected function setEngine(array $settings, $engine = 'gridstack_js') {
    $this->engine = $this->engineManager->load($engine, $settings);
    return $this;
  }

  /**
   * {@inheritdoc}
   *
   * @todo remove post blazy:2.17.
   */
  public function verifyItem(array &$element, $delta): void {
    // Do nothing.
  }

}
