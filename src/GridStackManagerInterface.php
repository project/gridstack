<?php

namespace Drupal\gridstack;

use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\blazy\BlazyManagerBaseInterface;
use Drupal\gridstack\Engine\GridStackEngineManagerInterface;
use Drupal\gridstack\Entity\GridStack;
use Drupal\gridstack\Skin\GridStackSkinManagerInterface;
use Drupal\gridstack\Style\GridStackStylizerInterface;

/**
 * Defines re-usable services and functions for gridstack plugins.
 *
 * @todo remove BlazyManagerBaseInterface when phpstand sniffs inheritance.
 */
interface GridStackManagerInterface extends BlazyManagerBaseInterface, TrustedCallbackInterface {

  /**
   * Returns GridStack layout engine manager service.
   */
  public function engineManager();

  /**
   * Sets GridStack layout engine manager service.
   */
  public function setEngineManager(GridStackEngineManagerInterface $engine_manager);

  /**
   * Returns GridStack skin manager service.
   */
  public function skinManager();

  /**
   * Sets GridStack skin manager service.
   */
  public function setSkinManager(GridStackSkinManagerInterface $skin_manager);

  /**
   * Returns GridStack stylizer service.
   */
  public function stylizer();

  /**
   * Sets GridStack stylizer service.
   */
  public function setStylizer(GridStackStylizerInterface $stylizer);

  /**
   * Returns a cacheable renderable array of a single gridstack instance.
   *
   * @param array $build
   *   An associative array containing:
   *   - items: An array of gridstack contents: text, image or media.
   *   - options: An array of key:value pairs of custom JS options.
   *   - optionset: The cached optionset object to avoid multiple invocations.
   *   - settings: An array of key:value pairs of HTML/layout related settings.
   *
   * @return array
   *   The cacheable renderable array of a gridstack instance, or empty array.
   */
  public function build(array $build): array;

  /**
   * Returns GridStack ayout engine plugin ID.
   */
  public function getEngineId();

  /**
   * Returns the layout engine.
   */
  public function getEngine(array $settings, $engine = NULL): ?object;

  /**
   * Returns the merged CSS class.
   */
  public function getMergedClasses($flatten = FALSE): array;

  /**
   * Returns the common settings inherited down to each item.
   */
  public function getUiSettings(): array;

  /**
   * Returns gridstacks object.
   *
   * @param array $settings
   *   The settings containing gridstacks object.
   *
   * @return \Drupal\blazy\BlazySettings
   *   The gridstacks object.
   */
  public function gridstacks(array &$settings);

  /**
   * Load the optionset with a fallback.
   *
   * @param string $name
   *   The optionset name.
   *
   * @return \Drupal\gridstack\Entity\GridStack
   *   The optionset object.
   */
  public function loadSafely($name): GridStack;

  /**
   * Provides layout editor settings.
   */
  public function adminSettings(array &$settings, array $element = []): void;

  /**
   * Prepares the engine settings.
   */
  public function engineSettings(array &$settings, $optionset = NULL): void;

  /**
   * Prepares the HTML settings.
   */
  public function prepareSettings(array &$settings);

  /**
   * Returns pre_render elements.
   */
  public function preRenderGridStack($element): array;

  /**
   * Reset the BlazySettings per item.
   */
  public function reset(array &$settings, $refresh = FALSE, $key = 'gridstacks');

}
