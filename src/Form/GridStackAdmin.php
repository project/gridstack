<?php

namespace Drupal\gridstack\Form;

use Drupal\Component\Utility\Html;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\blazy\Form\BlazyAdminInterface;
use Drupal\gridstack\GridStackManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides reusable admin functions or form elements.
 */
class GridStackAdmin implements GridStackAdminInterface {

  use StringTranslationTrait;

  /**
   * Static cache for the optionset options.
   *
   * @var array
   */
  protected $optionsetOptions;

  /**
   * The blazy admin service.
   *
   * @var \Drupal\blazy\Form\BlazyAdminInterface
   */
  protected $blazyAdmin;

  /**
   * The gridstack manager service.
   *
   * @var \Drupal\gridstack\GridStackManagerInterface
   */
  protected $manager;

  /**
   * Constructs a GridStackAdmin object.
   *
   * @param \Drupal\blazy\Form\BlazyAdminInterface $blazy_admin
   *   The blazy admin service.
   * @param \Drupal\gridstack\GridStackManagerInterface $manager
   *   The gridstack manager service.
   */
  public function __construct(
    BlazyAdminInterface $blazy_admin,
    GridStackManagerInterface $manager,
  ) {
    $this->blazyAdmin = $blazy_admin;
    $this->manager    = $manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      // @todo use $container->get('blazy.admin.formatter'), post blazy:2.17.
      $container->get('blazy.admin.formatter'),
      $container->get('gridstack.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function blazyAdmin() {
    return $this->blazyAdmin;
  }

  /**
   * {@inheritdoc}
   */
  public function manager() {
    return $this->manager;
  }

  /**
   * {@inheritdoc}
   */
  public function buildSettingsForm(array &$form, array $definition): void {
    $definition['namespace']  = 'gridstack';
    $definition['skins']      = $this->getSkinOptions();
    $definition['style']      = !empty($definition['style']);
    $definition['grid_form']  = !empty($definition['grid_form']);
    $definition['optionsets'] = $this->getOptionsetsByGroupOptions('js');

    foreach (['background', 'caches', 'fieldable_form', 'vanilla'] as $key) {
      $definition[$key] = $definition[$key] ?? TRUE;
    }

    $definition['layouts'] = isset($definition['layouts']) ? array_merge($this->getLayoutOptions(), $definition['layouts']) : $this->getLayoutOptions();

    $this->openingForm($form, $definition);
    $this->mainForm($form, $definition);
    $this->closingForm($form, $definition);
  }

  /**
   * {@inheritdoc}
   */
  public function mainForm(array &$form, array $definition): void {
    $views = $definition['_views'] ?? FALSE;
    if (!empty($definition['image_style_form'])) {
      $this->blazyAdmin->imageStyleForm($form, $definition);
    }

    if (!empty($definition['media_switch_form'])) {
      $this->blazyAdmin->mediaSwitchForm($form, $definition);
    }

    if (!empty($definition['fieldable_form'])) {
      $this->blazyAdmin->fieldableForm($form, $definition);

      if (!empty($definition['links'])) {
        $form['category'] = [
          '#title'       => $this->t('Category'),
          '#type'        => 'select',
          '#options'     => $definition['links'],
          '#description' => $this->t('The category to display inside the box.'),
        ];
      }
    }

    if (!empty($definition['stamps'])) {
      $form['stamp'] = [
        '#title'       => $this->t('Stamp'),
        '#type'        => 'select',
        '#options'     => $definition['stamps'],
        '#description' => $this->t('Stamp is _just a unique list, <b>Html list</b>, such as Latest news, blogs, testimonials, etc. replacing one of the other boring boxes, including rich ones. Leave empty to not use stamp.'),
        '#weight'      => -67,
      ];

      $form['stamp_index'] = [
        '#title'       => $this->t('Stamp index'),
        '#type'        => 'textfield',
        '#description' => $this->t('Which index, a stamp should be inserted into.'),
        '#weight'      => -66,
      ];
    }

    // Re-purpose/use the existing option Overlay for the Rich box.
    if (isset($form['overlay'])) {
      $form['overlay']['#title'] = $this->t('Rich box');
      $form['overlay']['#description'] = $this->t('Replace <b>Main stage/ image</b> if this node/ entity has one. It can be any entity reference, like Block, Media, etc. Use block_field.module for ease of block additions. How? Create a sticky (or far future created) node or two containing a Splide/ Slick carousel, video, weather, time, donations, currency, ads, or anything as a block field or a dedicated Media field, and put it here. Be sure different from the <b>Main stage</b>. While regular boxes are updated with new contents, these rich boxes may stay the same, and sticky@views', [
        '@views' => $views ? $this->t('(Use Views with sort by sticky or desc by creation).') : '.',
      ]);
      $form['overlay']['#weight'] = -65;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function openingForm(array &$form, array &$definition): void {
    $path    = $this->manager->getPath('module', 'gridstack');
    $is_ui   = $this->manager->moduleExists('gridstack_ui');
    $is_help = $this->manager->moduleExists('help');
    $route   = ['name' => 'gridstack_ui'];
    $readme  = $is_ui && $is_help ? Url::fromRoute('help.page', $route)->toString() : Url::fromUri('base:' . $path . '/README.md')->toString();

    if (!isset($form['optionset'])) {
      $this->blazyAdmin->openingForm($form, $definition);

      if ($is_ui) {
        $route_name = 'entity.gridstack.collection';
        $form['optionset']['#description'] = $this->t('Manage optionsets at <a href=":url" target="_blank">the optionset admin page</a>.', [':url' => Url::fromRoute($route_name)->toString()]);
      }
    }

    if (isset($form['skin'])) {
      $form['skin']['#description'] = $this->t('Skins allow various layouts with just CSS. Some options below depend on a skin. Leave empty to DIY. Check out <a href=":url">docs to register skins</a>.', [':url' => $readme]);
    }

    $form['gridnative'] = [
      '#type'        => 'checkbox',
      '#title'       => $this->t('Use native CSS Grid'),
      '#description' => $this->t('<b>Experimental!</b> Check to replace any js-driven (gridstack, masonry, packery, isotope) layouts with native browser Grid layout. Check out <a href=":url">CSS Grid browser supports</a> relevant to your visitors. <br>Uncheck if any issue.', [':url' => 'https://caniuse.com/#feat=css-grid']),
    ];

    if (isset($form['background'])) {
      $form['background']['#description'] = $this->t('Only if troubled with image sizes not making the given box gapless, check this to turn the image into CSS background instead. Blazy formatter only.');
      $form['background']['#weight'] = -40;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function closingForm(array &$form, array $definition): void {
    if (!isset($form['cache'])) {
      $this->blazyAdmin->closingForm($form, $definition);
    }

    if (isset($form['cache'])) {
      $form['cache']['#weight'] = empty($definition['_views']) ? -40 : -22;
    }

    $form['#attached']['library'][] = 'gridstack/admin';
  }

  /**
   * {@inheritdoc}
   */
  public function finalizeForm(array &$form, array $definition): void {
    $this->blazyAdmin->finalizeForm($form, $definition);
  }

  /**
   * {@inheritdoc}
   */
  public function getOptionsetsByGroupOptions($group = ''): array {
    if (!isset($this->optionsetOptions[$group])) {
      $optionsets = [];
      foreach ($this->manager->loadMultiple('gridstack') as $key => $entity) {
        // Exludes Boostrap/ Foundation grids which only work for LB.
        if ($group && $group == 'js' && $entity->getOption('use_framework')) {
          continue;
        }
        $optionsets[$key] = Html::escape($entity->label());
      }

      ksort($optionsets);
      $this->optionsetOptions[$group] = $optionsets;
    }

    return $this->optionsetOptions[$group];
  }

  /**
   * {@inheritdoc}
   */
  public function getSkinOptions(): array {
    return $this->manager->skinManager()->getSkinOptions();
  }

  /**
   * {@inheritdoc}
   */
  public function getLayoutOptions(): array {
    return [
      'bottom' => $this->t('Caption bottom'),
      'center' => $this->t('Caption center'),
      'top'    => $this->t('Caption top'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getSettingsSummary(array $definition): array {
    return $this->blazyAdmin->getSettingsSummary($definition);
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldOptions(
    array $target_bundles = [],
    array $allowed_field_types = [],
    $entity_type = 'media',
    $target_type = '',
  ): array {
    return $this->blazyAdmin->getFieldOptions($target_bundles, $allowed_field_types, $entity_type, $target_type);
  }

  /**
   * {@inheritdoc}
   */
  public function getResponsiveImageOptions(): array {
    return $this->blazyAdmin->getResponsiveImageOptions();
  }

}
