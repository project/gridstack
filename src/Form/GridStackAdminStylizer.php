<?php

namespace Drupal\gridstack\Form;

/**
 * Provides GridStack admin stylizer.
 *
 * @todo bring formatter goodness into Layout Builder.
 */
class GridStackAdminStylizer extends GridStackAdmin implements GridStackAdminStylizerInterface {}
