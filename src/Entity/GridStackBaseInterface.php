<?php

namespace Drupal\gridstack\Entity;

use Drupal\blazy\Config\Entity\BlazyConfigEntityBaseInterface;

/**
 * Provides a base interface defining GridStack entity.
 */
interface GridStackBaseInterface extends BlazyConfigEntityBaseInterface {

  /**
   * Return description.
   */
  public function description(): string;

  /**
   * Returns the GridStack json suitable for HTML data-attribute.
   *
   * @param string $group
   *   The option group can be settings or grids.
   *
   * @return string
   *   The output of the GridStack json.
   */
  public function getJson($group = 'settings');

  /**
   * Returns the file icon URI to be stored in public directory.
   */
  public function buildIconFileUri($dir = 'gridstack', $extension = 'png'): string;

  /**
   * Returns the file icon URI stored in public directory.
   */
  public function getIconFileUri($dir = 'gridstack', $extension = 'png'): ?string;

  /**
   * Returns a random name.
   */
  public function randomize($rand = 4);

  /**
   * Randomize a label from ID.
   */
  public function getLabelFromId($label);

  /**
   * Returns a randomized ID.
   */
  public function getRandomizedId($rand = 4);

  /**
   * Returns the variant source aka original GridStack layout.
   */
  public function source(): string;

}
