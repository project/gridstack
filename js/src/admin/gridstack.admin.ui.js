/**
 * @file
 * Provides GridStack admin utilities.
 */

(function ($, GridStack, _doc) {

  'use strict';

  $.gsa = $.gsa || {};

  function collection(el, data) {
    var nodes = GridStack.Utils.sort(data);
    var gid = el.dataset.breakpoint;
    var collections = nodes.map(function (node, i) {
      var box = $.gsa.base.box(node, i, gid);

      return new $.gsa.models.Box(box);
    });

    return new $.gsa.Collection(collections);
  }

  /**
   * GridStack UI public methods.
   *
   * @namespace
   */
  $.gsa.ui = {
    nodesData: [],
    nestedData: [],
    domData: [],

    collect: function (el, oldData, isNested) {
      var data = $.gsa.base[oldData ? 'oldData' : 'newData'](el, isNested);

      return collection(el, data);
    },

    load: function (o) {
      var me = this;

      o.saveCallback = o.saveCallback || {
        callback: me.save.bind(me)
      };

      return new $.gsa.views.GridStack(o);
    },

    save: function (collection, options) {
      var me = this;
      var breakpoint = options.breakpoint || null;
      var icon = options.icon || null;
      var mergedData = [];
      var nodesData = [];
      var nestedData = [];
      var updateIcon = options.updateIcon || false;

      // var json = $.gsa.base.save(el.gridstack);
      $.each(collection.models, function (box, i) {
        if ($.isUnd(box.id)) {
          return false;
        }

        var node = box.getDataToSave();
        var nested = box.nested || [];
        var nestedNode = box.getDataNestedToSave();

        nodesData.push(node);
        nestedData[i] = nested.length ? nestedNode : [];

        // Flatten main and nested boxes with complete data for icon.
        if (updateIcon) {
          var boxAttributes = $.extend(box.attributes, box.changedAttributes || {});
          delete boxAttributes.nested;

          boxAttributes.id = box.id;
          boxAttributes.gid = box.gid;

          mergedData.push(boxAttributes);

          if (nested.length) {
            delete nested.nested;
            mergedData = mergedData.concat(nested);
          }
        }
      });

      me.nodesData = nodesData;
      me.nestedData = nestedData;

      // Ensures empty value is respected to add and remove existing grids.
      var storedValue = me.nodesData.length ? JSON.stringify(me.nodesData) : '';
      var elStorage = $.find(_doc, '[data-drupal-selector="' + options.storage + '"]');
      if ($.isElm(elStorage)) {
        elStorage.value = storedValue;
      }

      var nestedValue = me.nestedData.length ?
        JSON.stringify(me.nestedData) : '';

      var elNestedStorage =
        $.find(_doc, '[data-drupal-selector="' + options.nestedStorage + '"]');
      if ($.isElm(elNestedStorage)) {
        elNestedStorage.value = options.isNested ? nestedValue : '';
      }

      // Collect data needed to generate icon ny if the main largest display.
      if (updateIcon && icon && icon === breakpoint) {
        var opts = $.extend($.gsa.base.options || {}, options);
        me.domData = $.gsa.icon.save(opts, mergedData);
      }
    }

  };

})(dBlazy, GridStack, this.document);
