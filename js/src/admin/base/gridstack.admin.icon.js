/**
 * @file
 * Provides GridStack admin dBlazy extensions.
 */

(function ($, _doc) {

  'use strict';

  $.gsa = $.gsa || {};

  function drawId(ctx, id, node) {
    var h = node.h;
    var x = node.left + 20;
    var y = h > 120 ? (node.top + 70) : (node.top + 45);

    ctx.beginPath();
    ctx.font = '64px sans-serif';
    ctx.textBaseline = 'middle';
    ctx.textAlign = 'left';
    ctx.fillStyle = 'white';
    ctx.fillText(id, x, y);
  }

  function draw(canvas, domData) {
    var url;
    var data = domData || [];

    if ($.isEmpty(data)) {
      return;
    }

    $.each(data, function (node) {
      var ctx = canvas.getContext('2d');
      var x = node.left;
      var y = node.top;
      var id = node.title;

      ctx.beginPath();

      ctx.fillStyle = node.color;

      // Uniform colors.
      if (node.color !== 'transparent') {
        ctx.fillStyle = '#18bc9c';
      }

      ctx.fillRect(x, y, node.w, node.h);
      ctx.restore();

      // Text.
      drawId(ctx, id, node);
    });

    // https://developer.mozilla.org/en-US/docs/Web/API/HTMLCanvasElement.
    url = canvas.toDataURL('image/png');
    var icon = $.find(_doc, '#gridstack-icon');
    if ($.isElm(icon)) {
      icon.value = url;
    }

    // Update icon preview.
    var screenshot = $.find(_doc, '#gridstack-screenshot');
    if ($.isElm(screenshot)) {
      screenshot.style.backgroundImage = 'url(' + url + ')';
      $.addClass(screenshot, 'has-bg');
    }

    return url;
  }

  /**
   * GridStack icon public methods.
   *
   * @namespace
   */
  $.gsa.icon = {

    build: function (form, rebuild, domData) {
      var iconBreakpoint = form.dataset.icon || 'lg';
      var elMain = $.find(form, '#gridstack-' + iconBreakpoint);

      if (!$.isElm(elMain)) {
        return;
      }

      // Sets canvas dimensions.
      var canvas = $.find(form, '#gridstack-canvas');
      var cw = $.toIntCss(elMain, 'width');
      var ch = $.toIntCss(elMain, 'height');
      var url = '';

      canvas.width = cw;
      canvas.height = ch;
      canvas.style.width = cw + 'px';
      canvas.style.height = ch + 'px';

      // Rebuild icon if required.
      if (rebuild) {
        url = draw(canvas, domData);
      }

      return url;
    },

    save: function (options, mergedData) {
      var lb = $.find(_doc, '#layout-builder');
      var main = $.find(_doc, '#gridstack-' + options.icon);
      var grids = {};
      var margin = options.margin > 2 ? options.margin : 15;
      var spacing = 10;
      var totalRows = parseInt(main.dataset.gsCurrentRow, 0);

      var domData = mergedData.map(function (node) {
        if ($.isUnd(node.w)) {
          return grids;
        }

        // The main grid boxes.
        var box = $.find(main, '#' + node.id);

        // The nested ones since all these are flattened array.
        if (!$.isElm(box) && node.nid) {
          box = $.find(main, '[data-gs-bid="' + node.nid + '"]');
        }

        if (!$.isElm(box)) {
          return grids;
        }

        var rect = $.rect(box.firstElementChild);
        var currentIndex = $.index(box) + 1;
        var left = $.toIntCss(box, 'left');
        var top = $.toIntCss(box, 'top');
        var elFirstNested = $.find(box, '.gridstack--nested');
        var elBoxNested = $.find(elFirstNested, '> .is-box-nested');
        var nested = false;
        var title = $.isElm(elBoxNested) ? '' : currentIndex;
        var color = '#18bc9c';
        var width = rect.width;
        var height = rect.height;
        var boxWidth = $.toIntAttr(box, 'gs-w');
        var boxHeight = $.toIntAttr(box, 'gs-h');
        var isRhs = (boxWidth + $.toIntAttr(box, 'gs-x')) === 12;
        var isBhs = (boxHeight + $.toIntAttr(box, 'gs-y')) === totalRows;
        var isBoxRoot = $.hasClass(box, 'box--root');

        if ($.isElm(elBoxNested)) {
          color = 'transparent';
        }

        var elNested = $.closest(box, '.gridstack--nested');
        if ($.isElm(elNested)) {
          var parentBox = $.closest(elNested, '.box--root');
          var parentBoxIndex = $.index(parentBox) + 1;
          var nestedBoxWidth = $.toIntAttr(box, 'gs-w');
          var nestedBoxHeight = $.toIntAttr(box, 'gs-h');

          isRhs = ((nestedBoxWidth + $.toIntAttr(box, 'gs-x')) === 12)
            && (($.toIntAttr(parentBox, 'gs-w')
            + $.toIntAttr(parentBox, 'gs-x')) === 12);
          isBhs = (nestedBoxHeight + $.toIntAttr(box, 'gs-y')) === totalRows;

          currentIndex = $.index(box) + 1;

          left += $.toIntCss(parentBox, 'left');
          top += $.toIntCss(parentBox, 'top');
          nested = true;

          if (!$.isUnd(parentBoxIndex) && !$.isUnd(currentIndex)) {
            title = parentBoxIndex + ':' + currentIndex;
          }
          else {
            title = '';
          }

          color = 'rgba(24, 288, 156, .4)';

          if ($.isElm(elBoxNested)) {
            color = '#18bc9c';
          }
        }

        var isNoMargin = options.margin === 0;
        if (!options.isNested && isNoMargin) {
          if (!isBhs) {
            height -= spacing;
          }
        }

        if (isBoxRoot) {
          if (isRhs) {
            width = width + (spacing * 4);
          }
          else {
            if (isNoMargin) {
              width -= spacing;
            }
            else if (options.isNested) {
              width -= spacing * 1.5;
            }
          }

          if (options.isNested) {
            height -= spacing;
          }
        }
        else {
          if (isRhs) {
            width = width + (spacing * 4);
          }

          // @fixme inconsistent canvas width and height between LB and UI.
          if (nested && $.isElm(lb)) {
            height -= spacing + 10;
            width -= spacing * 1.5;
          }
        }

        grids = {
          left: left,
          top: top,
          w: width,
          h: height,
          margin: margin,
          id: node.id || node.nid || 0,
          index: currentIndex,
          nested: nested,
          title: title,
          color: color
        };

        return grids;

      }, this);

      return domData;
    }

  };

})(dBlazy, this.document);
