/**
 * @file
 * Provides GridStack admin utilities for Layout Builder modal.
 *
 * Cheaper alt than full-blown class overrides via hook_element_plugin_alter
 * aside from the fact other modules might also race overriding that class.
 */

(function ($, Drupal, _doc) {

  'use strict';

  var _dgs = 'data-gs-';
  var _offCanvas = 'drupal-off-canvas';
  var _mlSelection = 'gs-media-library-selection';
  var _lbHighligtedClass = 'is-layout-builder-highlighted';
  var _selLbConfigure = '#layout-builder-configure-section';
  var _selLbSection = '.layout-builder__section';
  var _dataGsSection = _dgs + 'section';
  var _gsRoot = '.is-gs-lb';
  var _gsRootActivated = _gsRoot + '.' + _lbHighligtedClass;
  var _dataGsRegion = _dgs + 'region';
  var _dataGsRegionSel = '[' + _dataGsRegion + ']';
  var _dataGsMediaStorage = _dgs + 'media-storage';
  var _dataGsMetadata = 'data-gs-metadata';
  var _dataGsSelected = 'data-gs-selected';
  var _mediaOverlay = '> .box__bg .media__overlay';
  var _palettes = '.form-wrapper--color-palettes';
  var _visuallyHidden = 'visually-hidden';
  var _colorPicker = 'form-color--gs-color';
  var _transparency = 'form-range--gs-color';
  var elBody = _doc.body;
  var idColorPicker = 'gsColorPicker';
  var idColorPickerOn = idColorPicker + '--on';
  var elColorPicker = '.' + _colorPicker + ':not(.' + idColorPickerOn + ')';
  var idTransparency = 'gsTransparency';
  var idTransparencyOn = idTransparency + '--on';
  var elTransparency = '.' + _transparency + ':not(.' + idTransparencyOn + ')';
  var idAnimations = 'gsAnimations';
  var idAnimationsOn = idAnimations + '--on';
  var elAnimations = '.form-wrapper--animations:not(.' + idAnimationsOn + ')';
  var elAnimationsSelector = '.form-select--gs-animation';
  var idMediaHeight = 'gsMediaHeight';
  var idMediaHeightOn = idMediaHeight + '--on';
  var elMediaHeight = '.form-range--gs-lg-height:not(.' + idMediaHeightOn + ')';
  var idFieldset = 'gsFieldsetCollapsible';
  var idFieldsetOn = idFieldset + '--on';
  var elFieldsetCollapsible = 'fieldset.is-collapsible:not(.' + idFieldsetOn + ')';
  var idPreset = 'gsPreset';
  var idPresetOn = idPreset + '--on';
  var elPreset = '.form-wrapper[' + _dgs + 'preset-region]:not(input):not(.' + idPresetOn + ')';
  var idExtras = 'gsExtras';
  var idExtrasOn = idExtras + '--on';
  var elExtras = '.form-wrapper--extras:not(input):not(.' + idExtrasOn + ')';
  var idVerticalMargin = 'gsVerticalMargin';
  var idVerticalMarginOn = idVerticalMargin + '--on';
  var elVerticalMargin = '.form-item--vm:not(.' + idVerticalMarginOn + ')';
  var _bgc = 'background-color';
  var _open = 'open';
  var observer = null;
  // var removed = false;
  var configMutationCanvas = {
    attributes: true,
    childList: true,
    subtree: true,
    attributeOldValue: true,
    characterData: false,
    attributeFilter: ['value']
  };

  /**
   * Returns GridStack region identifier specific for LB operations.
   *
   * @param {String} region
   *   Region ID.
   *
   * @return {String}
   *   The region selector.
   */
  function fnRegion(region) {
    return '[data-gslb-region="' + region + '"]';
  }

  /**
   * Returns GridStack section.
   *
   * @return {Element}
   *   The .layout-builder__section HTML element.
   */
  function fnSection() {
    var $gsRoot = $(_gsRootActivated);
    var section = $.closest($gsRoot[0], _selLbSection);
    $.attr(section, _dataGsSection, true);
    return section;
  }

  /**
   * Return RGBA of a color given an alpha.
   *
   * @param {String} color
   *   The color value.
   * @param {String} alpha
   *   The alpha value.
   *
   * @return {String}
   *   The rgba color.
   */
  function makeRgba(color, alpha) {
    return 'rgba(' + parseInt(color.slice(-6, -4), 16) + ',' + parseInt(color.slice(-4, -2), 16) + ',' + parseInt(color.slice(-2), 16) + ',' + alpha + ')';
  }

  /**
   * Add the suffix due to not output via form API when combine with prefix.
   *
   * @param {Element} el
   *   The HTML element.
   */
  function addSuffix(el) {
    var suffix = '<span class="field-suffix messages messages--error visually-hidden"></span>';
    var elSuffix = $.next(el, '.field-suffix');

    // @todo  && $elm.hasClass('form-range--gs-opacity')
    if (!$.isElm(elSuffix)) {
      $.after(el, suffix);
    }
  }

  /**
   * GridStack functions.
   *
   * @param {Element} elm
   *   The .form-color--gs HTML element.
   */
  function fnColorPicker(elm) {
    var $palettes = $(_palettes);

    $(elm).off('change.gsColorPickerChange').on('change.gsColorPickerChange', function (e) {
      if ($palettes.length && $palettes.attr(_open)) {
        $palettes.removeAttr(_open);
      }
    });

    $(elm).off('input.' + idColorPicker).on('input.' + idColorPicker, function () {
      if ($palettes.length && !$palettes.attr(_open)) {
        $palettes.attr(_open, true);
      }
      var input = this;
      $.attr(input, 'value', input.value);
    });

    $.addClass(elm, idColorPickerOn);
  }

  /**
   * GridStack functions.
   *
   * @param {Element} elm
   *   The .form-color--gs HTML element.
   */
  function updateColorPicker(elm) {
    var elSection = fnSection();
    var color = elm.value;
    var property = elm.dataset[idColorPicker];
    var isBg = property === _bgc;
    var region = elm.dataset.gsColorRegion;
    var target = elm.dataset.gsTargetSelector || false;
    var $rgba = $('[' + _dgs + 'color-picker="rgba"][' + _dgs + 'color-region="' + region + '"]');
    var $alpha = $('.form-range--gs-alpha[' + _dgs + 'color-region="' + region + '"]');
    var alpha = $alpha.length ? $alpha[0].value : -1;
    var rgba = isBg ? makeRgba(color, alpha) : color;
    var skip = color === '#000000';
    var visibleColor = skip ? '' : rgba;

    if ($.isElm(elSection)) {
      var elRegion = $.find(elSection, fnRegion(region));
      if ($.isElm(elRegion)) {
        var elOverlay = $.find(elRegion, _mediaOverlay);
        if (isBg && $.isElm(elOverlay)) {
          visibleColor = rgba;
          if ($.hasClass(elRegion, 'is-gs-overlay')) {
            target = _mediaOverlay;
          }
          else {
            target = false;
            $.css(elOverlay, property, '');
          }
        }

        if (!target) {
          $.css(elRegion, property, visibleColor);
        }
        else {
          // @fixme || target === 'a:hover'
          if (target === 'a') {
            target = target + ':not(.gridstack__action a)';
          }
          // @todo a:hover, of course.
          var $target = $.find(elRegion, target);
          $.css($target, property, visibleColor);
        }
      }
    }

    if (isBg && $rgba.length) {
      $rgba[0].value = (skip ? '' : rgba);
    }
  }

  /**
   * GridStack functions.
   *
   * @param {Element} elm
   *   The .form-wrapper--animations HTML element.
   */
  function fnAnimations(elm) {
    var $elm = $(elm);
    var elSection;
    var $animation = $(elAnimationsSelector, elm);
    var values = [];

    $.each($animation.findAll('option'), function (item) {
      values.push(item.value);
    });

    $elm.off('.' + idAnimations).on('change.' + idAnimations, elAnimationsSelector, function () {
      elSection = fnSection();

      var input = this;
      var value = input.value;
      var region = input.dataset.gsAnimationRegion;
      var target = input.dataset.gsTargetSelector || false;

      if (target && $.isElm(elSection)) {
        var elRegion = $.find(elSection, fnRegion(region) + ' ' + target);
        if ($.isElm(elRegion)) {
          $.each(values, function (val) {
            $.removeClass(elRegion, val + ' animated');
          });

          if (value) {
            $.addClass(elRegion, value + ' animated');
            $.attr(elRegion, 'data-animation', value);
          }
          else {
            $.removeClass(elRegion, value + ' animated');
            $.removeAttr(elRegion, 'data-animation');
          }
        }
      }
    });

    $elm.off('blur.gsTextAnimation').on('blur.gsTextAnimation', '.form-text--gs', function () {
      elSection = fnSection();

      var input = this;
      var value = input.value;
      var key = input.dataset.gsAnimationKey;
      var region = input.dataset.gsAnimationRegion;
      var target = input.dataset.gsTargetSelector || false;

      if (target && $.isElm(elSection)) {
        var elRegion = $.find(elSection, fnRegion(region) + ' ' + target);
        if ($.isElm(elRegion)) {
          if (value === '') {
            $.removeAttr(elRegion, 'data-animation-' + key);
            $.css(elRegion, 'animation-' + key, '');
          }
          else {
            $.attr(elRegion, 'data-animation-' + key, value);
            $.addClass(elRegion, 'animated');
            $.css(elRegion, 'animation-' + key, value);
          }
        }
      }
    });
    $.addClass(elm, idAnimationsOn);
  }

  /**
   * GridStack functions.
   *
   * @param {Element} elm
   *   The .form-range--gs-alpha HTML element.
   */
  function fnTransparency(elm) {
    addSuffix(elm);

    $(elm).off('change.' + idTransparency).on('change.' + idTransparency, function (e) {
      var input = this;
      $.attr(input, 'value', input.value);
    });

    $.addClass(elm, idTransparencyOn);
  }

  /**
   * GridStack functions.
   *
   * @param {Element} elm
   *   The .form-range--gs-alpha HTML element.
   */
  function updateTransparency(elm) {
    var input = elm;
    var type = elm.dataset[idColorPicker];
    var elSection;

    // Only applicable to BG for now.
    var alphaChannel = function () {
      elSection = fnSection();

      var region = elm.dataset.gsColorRegion;
      var $color = $('[' + _dgs + 'color-picker="background-color"][' + _dgs + 'color-region="' + region + '"]');

      var alpha = input.value;
      var color = $color[0].value;
      var $rgba = $('[' + _dgs + 'color-picker="rgba"][' + _dgs + 'color-region="' + region + '"]');
      var rgba = makeRgba(color, alpha);
      var elPrefix = $.prev(input, '.field-prefix');
      var target = false;
      var property = _bgc;
      var skip = color === '#000000';
      var visibleColor = skip ? 'transparent' : rgba;

      if ($.isElm(elSection)) {
        var elRegion = $.find(elSection, fnRegion(region));

        if ($.isElm(elRegion)) {
          var elOverlay = $.find(elRegion, _mediaOverlay);
          if ($.isElm(elOverlay)) {
            if ($.hasClass(elRegion, 'is-gs-overlay')) {
              target = _mediaOverlay;
            }
            else {
              target = false;
              // Remove pevious color, if switched.
              $.css(elOverlay, property, '');
            }
          }

          if (!target) {
            $.css(elRegion, property, visibleColor);
          }
          else {
            // @todo a:hover, of course.
            var $target = $.find(elRegion, target);
            $.css($target, property, visibleColor);
          }
        }
      }

      // Store values regardless empty selectors, likely before adding images.
      if ($rgba.length) {
        $rgba[0].value = (skip ? '' : rgba);
      }

      if (elPrefix.length) {
        elPrefix[0].textContent = alpha;
      }
    };

    var imageOpacity = function () {
      elSection = fnSection();

      var region = input.dataset.gsColorRegion;
      var alpha = input.value;
      var target = input.dataset.gsTargetSelector || false;
      var elPrefix = $.prev(input, '.field-prefix');

      if (target && $.isElm(elSection)) {
        var elRegion = $.find(elSection, fnRegion(region) + ' ' + target);
        if ($.isElm(elRegion)) {
          var elMedia = $.find(elRegion, '> .media__element');
          if ($.isElm(elMedia)) {
            $.css(elRegion, 'opacity', '');
            elRegion = elMedia;
          }
          $.css(elRegion, 'opacity', alpha);
        }
      }

      // Indicate values regardless empty, likely before adding images.
      if ($.isElm(elPrefix)) {
        elPrefix.textContnt = alpha;
      }
    };

    if (type === 'alpha') {
      alphaChannel();
    }
    else if (type === 'opacity') {
      imageOpacity();
    }
  }

  /**
   * GridStack functions.
   *
   * @param {Element} input
   *   The input HTML element.
   * @param {Integer} value
   *   The default input value for an error.
   */
  function showError(input, value) {
    var elPrefix = $.prev(input, '.field-prefix');
    var elSuffix = $.next(input, '.field-suffix');

    if ($.isElm(elSuffix)) {
      $.removeClass(elSuffix, _visuallyHidden);
      elSuffix.textContent = Drupal.t('Add image, or save the form first.');

      setTimeout(function () {
        elSuffix.textContent = '';
        $.addClass(elSuffix, _visuallyHidden);
      }, 6000);
    }

    input.value = value;

    if ($.isElm(elPrefix)) {
      elPrefix.textContent = value;
    }
  }

  /**
   * GridStack functions.
   *
   * @param {Element} elm
   *   The .form-color--gs-height HTML element.
   */
  function fnMediaHeight(elm) {
    var elSection;

    addSuffix(elm);

    var updateMediaHeight = function (e) {
      elSection = fnSection();
      if (!$.isElm(elSection)) {
        return;
      }

      var input = e.target;
      var region = input.dataset.gsColorRegion;
      var value = input.value;
      var elPrefix = $.prev(input, '.field-prefix');
      var target = input.dataset.gsTargetSelector || '';
      var elRegion = $.find(elSection, fnRegion(region) + target);

      if ($.isElm(elRegion)) {
        value = value < 100 ? '' : value;
        $.css(elRegion, 'height', value + 'px');
      }
      else {
        showError(input, 0);
      }

      if ($.isElm(elPrefix)) {
        elPrefix.textContent = value;
      }
    };

    $(elm).off('change.' + idMediaHeight).on('change.' + idMediaHeight, updateMediaHeight);
    $.addClass(elm, idMediaHeightOn);
  }

  /**
   * GridStack functions.
   *
   * @param {Element} elm
   *   The [data-gs-preset-region] HTML element.
   */
  function fnPreset(elm) {
    var $elm = $(elm);
    var values = [];
    var elSection;

    $.each($elm.findAll('input'), function (item) {
      values.push(item.value);
    });

    var updatePreset = function (e) {
      elSection = fnSection();
      if (!$.isElm(elSection)) {
        return;
      }

      var input = e.target;
      var target = input.dataset.gsTargetSelector || '';
      var value = input.value;
      var region = $.attr(input, _dgs + 'preset-region');
      var elRegion = $.find(elSection, fnRegion(region) + target);

      if ($.isElm(elRegion)) {
        // @todo var elContent = elRegion.find('> .box__content');
        // @todo if (elContent.length) {
        // @todo  elRegion = elContent;
        // @todo }
        $.each(values, function (val) {
          $.removeClass(elRegion, val);
        });

        if (value) {
          $.addClass(elRegion, value);
        }
      }
    };

    $elm.off('click.' + idPreset).on('click.' + idPreset, 'input', updatePreset);
    $.addClass(elm, idPresetOn);
  }

  /**
   * GridStack functions.
   *
   * @param {Element} elm
   *   The .form-wrapper--extras HTML element.
   */
  function fnExtras(elm) {
    var $elm = $(elm);
    var elSection;

    $elm.off('click.' + idExtras).on('click.' + idExtras, 'input', function (e) {
      elSection = fnSection();
      if (!$.isElm(elSection)) {
        return;
      }

      var input = e.target;
      var value = input.value;
      var cssClass = input.dataset.gsExtrasRegionClass;
      var region = $.attr(input, _dgs + 'extras-region');
      var elRegion = $.find(elSection, fnRegion(region));
      var $rgba = $('.form-color--gs-rgba[' + _dgs + 'color-region="' + region + '"]');
      var $bg = $('.form-color--gs-bg[' + _dgs + 'color-region="' + region + '"]');

      if ($.isElm(elRegion)) {
        var elContent = $.find(elRegion, '> .box__content');
        if ($.isElm(elContent)) {
          elRegion = elContent;
        }

        var checked = input.checked;

        $[checked ? 'addClass' : 'removeClass'](elRegion, cssClass);
        if (cssClass === 'is-gs-ete') {
          var box = $.closest(elRegion, '.box');
          $[checked ? 'addClass' : 'removeClass'](box, 'box--ete');
        }
        var elOverlay = $.find(elRegion, _mediaOverlay);
        switch (value) {
          case 'overlay':
            if ($.isElm(elOverlay)) {
              var previousColor = $rgba[0].value || $bg[0].value;
              if (previousColor) {
                if (checked) {
                  $.css(elOverlay, _bgc, previousColor);
                }
                else {
                  $.css(elRegion, _bgc, previousColor);
                }
              }

              $[checked ? 'show' : 'hide'](elOverlay);
            }
            break;

          default:
            break;
        }
      }
    });

    $.addClass(elm, idExtrasOn);
  }

  /**
   * GridStack functions.
   *
   * @param {Element} elm
   *   The .is-collapsible HTML element.
   */
  function fnFieldsetCollapsible(elm) {
    var $elm = $(elm);
    var span = $elm.find('> legend > span');

    if ($.isElm(span)) {
      $.addClass(span, 'btn button');
    }

    $elm.off('click.' + idFieldset).on('click.' + idFieldset, 'legend', function (e) {
      var fieldset = $.closest(e.target, 'fieldset');
      $.toggleClass(fieldset, 'is-collapsed');
    });
    $elm.addClass(idFieldsetOn);
  }

  /**
   * GridStack functions.
   *
   * @param {Element} elm
   *   The .form-item--vm HTML element.
   */
  function fnVerticalMargin(elm) {
    var $elm = $(elm);
    var values = [];
    var $gsRoot;

    $.each(elm.children, function (item) {
      values.push(item.value);
    });

    $elm.off('change.' + idVerticalMargin).on('change.' + idVerticalMargin, function (e) {
      $gsRoot = $(_gsRootActivated);
      if (!$gsRoot.length) {
        return false;
      }

      var select = e.target;
      var value = select.value;

      $.each(values, function (val) {
        $gsRoot.removeClass('vm-' + val);
      });
      $gsRoot.addClass('vm-' + value);
    });

    $.addClass(elm, idVerticalMarginOn);
  }

  /**
   * GridStack functions.
   *
   * @param {Element} elm
   *   The target HTML element.
   * @param {String} mid
   *   The media ID, might be empty when deleted.
   */
  function updateMid(elm, mid) {
    var elDetails = $.closest(elm, _dataGsRegionSel);

    if (!elDetails) {
      var activeRegion = $.attr(elBody, _dataGsRegion);
      if (activeRegion) {
        elDetails = $.find(elBody, _selLbConfigure + ' [' + _dataGsRegion + '="' + activeRegion + '"]');
      }
    }

    if (!elDetails && elm.name === 'layout_settings[current_selection]') {
      elDetails = elm.nextElementSibling;
    }

    if (elDetails) {
      setTimeout(function () {
        var id = $.attr(elDetails, _dataGsRegion);
        var els = $.findAll(elDetails, '[' + _dataGsMediaStorage + '="' + id + '"]');
        var metadata = $.find(elDetails, '[' + _dataGsMetadata + '="' + id + '"]');
        var selected = $.find(elDetails, '[' + _dataGsSelected + '="' + id + '"]');

        if (els.length) {
          $.each(els, function (el) {
            el.value = mid;
            // Just to be clear.
            $.attr(el, 'value', mid);
          });

          if (!mid) {
            if (metadata) {
              $.attr(metadata, 'value', '');
            }

            if (selected) {
              $.attr(selected, 'value', 'removed');
            }
          }
          else {
            if (selected) {
              $.attr(selected, 'value', 'added');
            }
          }
        }
      }, 100);
    }
  }

  /**
   * GridStack functions.
   *
   * @param {Element} el
   *   The target HTML element.
   * @param {String} css
   *   The CSS class to apply.
   *
   * @return {Bool}
   *   True if contains the class.
   */
  function is(el, css) {
    return el && $.hasClass(el, css);
  }

  /**
   * Callback function to execute when mutations are observed.
   *
   * @param {MutationRecord} mutations
   *   The recorded mutation list object.
   * @param {MutationObserver} observer
   *   The MutationObserver instance.
   *
   * @see https://dom.spec.whatwg.org/#mutationrecord
   * @see https://dom.spec.whatwg.org/#mutation-observers
   * @see https://developer.mozilla.org/en-US/docs/Web/API/MutationObserver
   */
  function observeMutation(mutations, observer) {
    $.each(mutations, function (mutation) {
      var target = mutation.target;
      if (target === null) {
        return;
      }

      // Skip useless mutations.
      if (target.nodeName && target.nodeName === 'SUMMARY') {
        return;
      }

      if (mutation.type === 'attributes') {
        if (mutation.attributeName === 'value') {
          // var name = target.getAttribute('name');
          // if (name && mutation.attributeName === 'disabled') {
          // removed = name.indexOf('media-library-remove') !== -1;
          // }
          if (is(target, _mlSelection) ||
            is(target, 'js-media-library-add-form-current-selection')) {
            updateMid(target, target.value);
          }
          else if (is(target, _colorPicker)) {
            updateColorPicker(target);
          }
          else if (is(target, _transparency)) {
            updateTransparency(target);
          }
        }
      }

      if (mutation.type === 'childList') {
        var el = mutation.previousSibling;
        // form-submit--gs-remove b…form-submit form-submit
        if (is(el, 'media-library-item__remove') ||
          is(el, 'form-submit--gs-remove')) {
          updateMid(target, '');
        }
      }
    });
  }

  function _observe() {
    var elCanvas = _doc.getElementById(_offCanvas);
    observer = new MutationObserver(Drupal.debounce(observeMutation, 200));
    if (elCanvas) {
      observer.observe(elCanvas, configMutationCanvas);
    }
  }

  /**
   * Attaches gridstack behavior to HTML element .gridstack.is-gs-lb.
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.gridStackAdminModal = {
    attach: function (context) {

      // Once for all, all for once.
      $.once(fnColorPicker, idColorPicker, elColorPicker, context);
      $.once(fnTransparency, idTransparency, elTransparency, context);
      $.once(fnAnimations, idAnimations, elAnimations, context);
      $.once(fnMediaHeight, idMediaHeight, elMediaHeight, context);
      $.once(fnFieldsetCollapsible, idFieldset, elFieldsetCollapsible, context);
      $.once(fnPreset, idPreset, elPreset, context);
      $.once(fnExtras, idExtras, elExtras, context);
      $.once(fnVerticalMargin, idVerticalMargin, elVerticalMargin, context);

      // Observe mutations.
      _observe();
    },
    detach: function (context, settings, trigger) {
      if (trigger === 'unload') {
        $.once.removeSafely(idColorPicker, elColorPicker, context);
        $.once.removeSafely(idTransparency, elTransparency, context);
        $.once.removeSafely(idAnimations, elAnimations, context);
        $.once.removeSafely(idMediaHeight, elMediaHeight, context);
        $.once.removeSafely(idFieldset, elFieldsetCollapsible, context);
        $.once.removeSafely(idPreset, elPreset, context);
        $.once.removeSafely(idExtras, elExtras, context);
        $.once.removeSafely(idVerticalMargin, elVerticalMargin, context);

        if (observer) {
          observer.disconnect();
        }
      }
    }
  };

})(dBlazy, Drupal, this.document);
