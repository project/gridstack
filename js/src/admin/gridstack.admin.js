/**
 * @file
 * Provides GridStack admin utilities.
 */

(function ($, Drupal, drupalSettings, GridStack, _win, _doc) {

  'use strict';

  var _id = 'gridstack';
  var _classId = '.' + _id;
  var _selId = '#' + _id;
  var _dgs = 'data-gs-';
  var _isgs = 'is-gs';
  var idOnce = 'gsform';
  var mounted = 'is-' + idOnce;
  var sForm = '.form--' + _id + ':not(.' + mounted + ')';
  var idRoot = 'gsroot';
  var sRoot = _classId + '--root';
  var cHidden = 'visually-hidden';
  var cMain = _id + '--main';
  var _addClass = 'addClass';
  var _removeClass = 'removeClass';
  var _ddsel = 'data-drupal-selector';
  var _click = 'click';

  $.gsa = $.gsa || {};
  $.gsa.base = $.gsa.base || {};
  $.gsa.ui = $.gsa.ui || {};

  /**
   * GridStack form public methods.
   *
   * @namespace
   */
  $.gsa.form = {
    $form: null,
    $main: null,
    form: null,
    main: null,
    gridStacks: [],
    gridStackMain: null,
    isNested: false,
    isMain: false,
    isValid: function (x) {
      return !$.isNull(x) && !$.isUnd(x);
    },

    hasGridStack: function (el) {
      return el && this.isValid((el.length ? el[0] : el).gridstack);
    },

    /**
     * load a GridStack.
     *
     * @param {Element} root
     *   The GridStack HTML element.
     * @param {bool} oldData
     *   Whether to load old data such as for a revert.
     *
     * @return {$.gsa.views.GridStack}
     *   Returns gridstack view instance.
     */
    load: function (root, oldData) {
      var me = this;
      var ui = $.gsa.ui;
      var defaults = drupalSettings.gridstack || {};
      var elForm = me.form;
      var column = parseInt(root.dataset.gsColumn, 0);
      var config = $.extend({}, defaults, $.parse(root.dataset.config));
      var isDisabled = $.hasClass(root, _isgs + '-disabled');
      var isMain = $.hasClass(root, cMain);
      var isSmallest = column === 1 || $.hasClass(root, _id + '--smallest');
      var elRegions = $.find(elForm, _selId + '-regions');
      var elNoMargin = $.find(elForm, '#edit-options-settings-nomargin');
      var noMargin = elNoMargin ? elNoMargin.checked : false;
      var regions = $.isElm(elRegions) ? elRegions.dataset.gsRegions : [];
      var isNested = me.isNested;
      var nested = config;

      // Multi-dimensional is merged, not overriden, which is bad, remove.
      delete nested.resizable;
      var configNested = $.extend({}, nested, $.parse(root.dataset.configNested));

      var data = {
        el: root,
        config: $.extend(config, {
          column: column,
          isNested: isNested,
          noMargin: noMargin
        }),
        configNested: configNested,
        options: {
          breakpoint: root.dataset.breakpoint,
          icon: elForm.dataset.icon,
          id: root.id,
          isDisabled: isDisabled,
          isMain: isMain,
          isNested: isNested,
          isSmallest: isSmallest,
          isRegion: isMain ? true : false,
          nodeConfig: $.parse(root.dataset.gsNodeConfig),
          regions: regions,
          storage: root.dataset.storage,
          nestedStorage: root.dataset.nestedStorage
        }
      };

      return ui.load(data);
    }

  };

  /**
   * GridStack form functions.
   *
   * @param {Element} form
   *   The GridStack form HTML element.
   *
   * @return {Object}
   *   Few public methods.
   */
  function gridStackForm(form) {
    var me = $.gsa.form;
    var main = $.find(form, '.' + cMain);
    var sFramework = '[' + _ddsel + '="edit-options-use-framework"]';
    var elFramework = $.find(form, sFramework);
    var checked = elFramework ? elFramework.checked : false;
    var $lastDetails = $('details.last', form);
    var hiddenDetails = '.vertical-tabs > div > details:not(.last)';

    me.form = form;
    me.main = main;
    me.$form = $(form);
    me.$main = $(main);
    me.isNested = checked;
    me.lbForm = $.closest(form, '.layout-builder-form');

    // Attempts to fix weirdness during init.
    // @todo, still not a suspect: vertical tabs display:none.
    setTimeout(function () {
      var elTab = $.find(form, '.vertical-tabs__edit-breakpoints');
      $.removeClass(elTab, cHidden);
      $.removeClass(form, _isgs + '-nojs');
    }, 1000);

    function showTabs() {
      var $tabs = $(hiddenDetails + ', ' + hiddenDetails + '> div', form);
      $tabs.addClass(cHidden).css('display', 'block');

      $lastDetails.css('display', 'block');
    }

    /**
     * GridStack layout methods applied to the top level GridStack element.
     *
     * @param {Element} root
     *   The GridStack HTML element.
     */
    function gridStackRoot(root) {
      var gridStack = me.load(root, true);

      // Render the GridStack instance.
      gridStack.render(root);

      if ($.hasClass(root, cMain)) {
        me.gridStackMain = gridStack;
      }

      // Collect GridStack instances for aggregated CRUD.
      me.gridStacks.push(gridStack);
    }

    /**
     * Build column selector.
     *
     * @param {Event} e
     *   The event triggered by a `change` event.
     */
    function onSelectColumn(e) {
      if (e.target === this) {
        var cel = this;
        var elTarget = $.find(form, cel.dataset.target);

        if (me.hasGridStack(elTarget)) {
          var instance = elTarget.gridstack;
          var column = parseInt(cel.value, 0) || parseInt(elTarget.dataset.gsColumn, 0);
          var css = $.attr(elTarget, 'class');
          var classes = (css.match(/grid-stack-(\d+)/g) || []).join(' ');

          $.removeClass(elTarget, classes);

          $.addClass(elTarget, 'grid-stack-' + column);
          $.attr(elTarget, _dgs + 'column', column);

          instance.column(column);
        }
      }
    }

    /**
     * Build width selector which affects GridStack width for correct preview.
     *
     * @param {Element} el
     *   The width input HTML element.
     */
    function updateWidth(el) {
      var updateWidthNow = function (input) {
        var elTarget = $.find(form, input.dataset.target);
        var elSubPreview = $.closest(elTarget, _classId + '-preview--sub');

        if ($.isElm(elTarget)) {
          var value = input.value;
          var w = value ?
            parseInt(value, 0) :
            parseInt(elTarget.dataset.responsiveWidth, 0);
          var width = w < 320 ? 320 : w;

          if (value !== '') {
            elTarget.style.width = width + 'px';
          }

          $[value === '' ? _addClass : _removeClass](elSubPreview, 'form-disabled');
        }
      };

      updateWidthNow(el);

      $.on(el, 'blur', function (e) {
        if (e.target === this) {
          updateWidthNow(this);
        }
      });
    }

    /**
     * Sets the framework environment.
     */
    function setFramework() {
      var elFramework = $.find(form, sFramework);
      var checked = elFramework.checked;
      me.isNested = checked || me.$form.hasClass('form--framework');
      me.$form[me.isNested ? _addClass : _removeClass]('is-framework');
    }

    /**
     * Reacts on form submission.
     *
     * @param {Event} e
     *   The event triggered by a `click` event.
     */
    function onFormSubmit(e) {
      me.$form.addClass(_isgs + '-saving');

      // Some stored values dependent on :visible pseudo will not store with
      // CSS display none, hence force them visible.
      // Claro has class is-selected, Seven selected.
      // Claro has vertical-tabs__item, Seven vertical-tabs__pane.
      // Do not rely on themes to avoid incompatibility, use direct decendants.
      $('.vertical-tabs > ul > li', form).removeClass('selected is-selected');
      var elLastTab = $.find(form, '.vertical-tabs > ul > li:last-child a');

      if ($.isElm(elLastTab)) {
        elLastTab.click();
      }

      showTabs();

      // Failsafe to generate icon if "Save & continue" button is not hit.
      var elSave = $.find(form, '.btn--gridstack[data-message="save"]');
      if ($.isElm(elSave)) {
        elSave.click();
      }
    }

    /**
     * Reacts on button click events er box: add (nested) or remove.
     *
     * @param {Event} e
     *   The event triggered by a `click` event.
     *
     * @todo move it to backbone if doable.
     */
    function itemBoxMultiple(e) {
      e.preventDefault();
      e.stopPropagation();

      if (e.target === this) {
        var btn = this;
        var message = btn.dataset.message;
        var type = btn.dataset.type || 'root';
        var boxId = btn.dataset.gsBid;

        if (message === 'remove' && type === 'root') {
          $.gsa.base.lastBoxIndex--;
        }

        $.each(me.gridStacks, function (gridStack) {

          // Do not use direct child selector (>) to respect nested boxes.
          // Do not use closest() to apply to all breakpoint boxes.
          // Nested boxes are not backbone models, want a backbone model here,
          // meaning must reference its parent box to get the model.
          // The elBox must be the actual referenced HTML box regardless models.
          // No need to check for box validity here.
          var elBox = $.find(gridStack.el, '.box[' + _dgs + 'bid="' + boxId + '"]');
          var model = gridStack.getModel(elBox);

          gridStack.emit(_id + ':' + type + ':' + message, e, model, elBox);
        });
      }
    }

    /**
     * Adds a new box to all breakpoints once on clicking `Add grid` button.
     *
     * @param {Event} e
     *   The event triggered by a `click` event.
     */
    function addBoxMultiple(e) {
      e.preventDefault();

      if (e.target === this) {
        $.gsa.base.lastBoxIndex++;

        var index = $.gsa.base.lastBoxIndex;

        $.each(me.gridStacks, function (gridStack) {
          gridStack.add(index, false);
        });
      }
    }

    /**
     * Reacts on button click events.
     *
     * @param {Event} e
     *   The event triggered by a `click` event.
     */
    function saveIcon(e) {
      e.preventDefault();

      if (e.target === this) {
        var btn = this;
        var message = btn.dataset.message;
        var root = $.closest(btn, _classId + '-preview--main');

        if (me.gridStackMain && $.isElm(root)) {
          var json = $.gsa.base.save(me.gridStackMain.gridStack);
          $.gsa.mainData = json;

          me.gridStackMain.emit(_id + ':main:' + message);

          $.gsa.icon.build(form, true, $.gsa.ui.domData);
        }
      }
    }

    /**
     * Initializes events.
     */
    function displayIcon() {
      var icon = $.find(form, _selId + '-icon');
      var storedIconUrl = $.attr(icon, 'data-url');
      var elScreenshot = $.find(form, _selId + '-screenshot');

      // Display icon if exists at public, or MODULE_NAME/images, directory.
      if (storedIconUrl && $.isElm(elScreenshot)) {
        var date = new Date();
        elScreenshot.innerHTML = '<img src="' + storedIconUrl + '?rand=' + date.getTime() + '" alt="Icon" />';
      }
    }

    /**
     * Initializes main events.
     */
    function initMainEvents() {
      var eAdd = _click + '.gsadd';
      var eBox = _click + '.gsbox';
      var eColumn = 'change.gscolumn';
      var eFramework = _click + '.gsframework';
      var eSaveIcon = _click + '.gssaveicon';
      var eSubmit = 'submit.gssubmit';

      // Run actions.
      $.each(me.$form.findAll('.form-text--width'), updateWidth);

      if ($.isElm(me.lbForm)) {
        var action = me.$form.attr('action');
        var destination = $.attr(me.lbForm, 'action');

        me.$form.attr('action', action + '?destination=' + destination);
      }

      me.$form.off(eAdd).on(eAdd, '.btn--main.btn--add', addBoxMultiple);
      me.$form.off(eBox).on(eBox, '.btn--box', itemBoxMultiple);
      me.$form.off(eColumn).on(eColumn, '.form-select--column', onSelectColumn);
      me.$form.off(eFramework).on(eFramework, sFramework, setFramework);

      me.$form.off(eSaveIcon).on(eSaveIcon, '.btn--main.btn--save[data-icon]', saveIcon);
      me.$form.on(eSubmit, onFormSubmit);
    }

    // Check if using CSS framework, or GridStack JS.
    setFramework();

    // Loop through each GridStack root instance, not nested one.
    $.once(gridStackRoot, idRoot, sRoot, form);

    displayIcon();
    initMainEvents();

    $.addClass(form, mounted);

    return {
      addBoxMultiple: addBoxMultiple,
      saveIcon: saveIcon
    };
  }

  /**
   * Attaches gridstack behavior to HTML element .form--gridstack.
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.gridStackAdmin = {
    attach: function (context) {

      $.once(gridStackForm, idOnce, sForm, context);
    },
    detach: function (context, settings, trigger) {
      if (trigger === 'unload') {
        $.once.removeSafely(idOnce, sForm, context);
      }
    }
  };

})(dBlazy, Drupal, drupalSettings, GridStack, this, this.document);
