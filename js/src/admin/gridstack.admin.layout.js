/**
 * @file
 * Provides GridStack admin utilities for Layout Builder inline.
 *
 * Cheaper alt than full-blown class overrides via hook_element_plugin_alter
 * aside from the fact other modules might also race overriding that class.
 */

(function ($, Drupal, dOffCanvas, GridStyle, _win, _doc) {

  'use strict';

  var _id = 'gridstack';
  var _lb = 'layout-builder';
  var _dgs = 'data-gs-';
  var _isgs = 'is-gs';
  var elBody = _doc.body;
  var dRegion = _dgs + 'region';
  var dGsSection = _dgs + 'section';
  var _ddsel = 'data-drupal-selector';
  var cGsActionAjax = _id + '__action--ajax-styles';
  var cGsActionStylesJs = _id + '__action--js-styles';
  var cLbLinkConfig = _lb + '__link--configure';
  var cLbHighligted = 'is-' + _lb + '-highlighted';
  var cGridstackAction = _id + '__action';
  var cGsRegionActive = _isgs + '-region-active';
  var cFormSubmitActive = 'form-submit--gs-active';
  // var cLoading = 'is-b-loading';
  var cVisuallyHidden = 'visually-hidden';
  var sLbLinkConfig = '.' + cLbLinkConfig;
  var idLbSection = 'gslbsection';
  var cGsSection = _isgs + '-section';
  var sLbSection = '.' + _lb + '__section:not(.' + cGsSection + ')';
  var sGsSection = sLbSection + '[' + _dgs + 'section]';
  var sGsLb = '.is-gs-lb';
  var sGsLbActivated = sGsLb + '.' + cLbHighligted;
  var sGridstackAction = '.' + cGridstackAction;
  var idLbTopLink = 'onLbTopLink';
  var sLbTopLink = sLbLinkConfig + ':not(.' + cGsActionAjax + ')';
  var sContainer = '.is-gs-lb';
  var sMainSettings = '.is-gs-main-settings';
  var sOffcanvas = '#drupal-off-canvas';
  var sOffcanvasForm = '#layout-builder-configure-section';
  var idOffcanvas = 'gsoffcanvas';
  var sOffcanvasSubmit = sOffcanvasForm + ' .js-form-submit';
  var _attached = false;
  var _eLbTopLink = 'mousedown.' + idLbTopLink;
  var _eOffcanvas = 'mousedown.' + idOffcanvas;

  /**
   * Prepare region markers.
   *
   * @param {Event} e
   *   The mousedown event
   */
  function fnOnLbTopLink(e) {
    fnCleanOut();
    fnSectionActivate(e);
  }

  /**
   * Cleans up region markers.
   *
   * @param {Event} e
   *   The mousedown event
   */
  function fnOnModalClosed(e) {
    // dialog:afterclose is called after ML AJAX upload, too.
    // dialog:beforeclose is called when the form is not saved.
    setTimeout(function () {
      $.removeAttr(elBody, _dgs + 'variant-body');
      $.removeClass(elBody, _isgs + '-lb-saving');

      if (!$(sOffcanvas).length) {
        fnCleanOut();
      }
    }, 100);
  }

  // Reacts on dialog open.
  function fnOnModalOpened(e, dialog, $element) {
    var el = $element.length ? $element[0] : $element;
    fnShowActiveRegion(el, 'dialog:aftercreate');

    setTimeout(function () {
      if (dOffCanvas && dOffCanvas.isOffCanvas($element) &&
        $.hasClass(_doc.body, _isgs + '-lb-saving')) {

        var elVid = $.find(el, '[' + _ddsel + '="edit-layout-settings-settings-global-vid"]');
        var elUpdate = $.find(el, '[' + _ddsel + '="edit-actions-submit"]');

        if ($.isElm(elUpdate)) {
          var vidAjax = $.attr(_doc.body, _dgs + 'variant-body');

          elVid.value = vidAjax;
          $.trigger(elUpdate, 'mousedown');
        }
      }

    }, 100);
  }

  /**
   * Cleans up region markers.
   */
  function fnCleanOut() {
    var elSection = $(sGsSection)[0];
    var $gsRegionActive = $('.' + cGsRegionActive);

    $.removeAttr(elBody, dRegion);

    if ($.isElm(elSection)) {
      $.removeAttr(elSection, dGsSection);
    }

    if ($gsRegionActive && $gsRegionActive.length) {
      $gsRegionActive.removeClass(cGsRegionActive).attr('id', '');

      $gsRegionActive.each(function (item) {
        $(item).removeClass(cGsRegionActive).attr('id', '');
      });
    }

    $('.' + cFormSubmitActive).removeClass(cFormSubmitActive);
  }

  /**
   * Prepares region markers.

   * @param {Event} e
   *   The click/ mousedown event.
   */
  function fnSectionActivate(e) {
    var elSection = $(sGsSection)[0];
    var elGridstack = $(sGsLbActivated)[0];
    var link = e.target;

    if (!$.isElm(elSection)) {
      elSection = $.closest(link, sLbSection);
    }

    if (!$.isElm(elSection) && $.isElm(elGridstack)) {
      elSection = $.closest(elGridstack, sLbSection);
    }

    if ($.isElm(elSection) && $.isElm(elGridstack)) {
      $.attr(elBody, dRegion, true);
      $.attr(elSection, dGsSection, true);
    }
  }

  /**
   * Reacts on clicking gridstack__action--ajax-styles link.
   *
   * @param {Event} e
   *   The mousedown event.
   */
  function fnRegionActivate(e) {
    var link = e.target;
    var regionId = link.parentNode.dataset.gsRegion;

    fnCleanOut();
    fnSectionActivate(e);

    var box = $.closest(link, '.gridstack__box');
    $.addClass(box, cGsRegionActive);
    $.attr(box, 'id', cGsRegionActive);
    $.attr(elBody, dRegion, regionId);
  }

  /**
   * GridStack functions.
   *
   * @param {HTMLElement} modal
   *   The modal HTML element.
   * @param {String} type
   *   The current event type.
   */
  function fnShowActiveRegion(modal, type) {
    var $modal = $(modal);
    var activeRid = $(elBody).attr(dRegion);

    if (!$.isUnd(activeRid)) {
      var activeRegion = '[' + dRegion + '="' + activeRid + '"]';
      var elActiveRegion = $modal.find(activeRegion);

      if ($.isElm(elActiveRegion)) {
        var $activeRegion = $(elActiveRegion);
        var container = elActiveRegion.dataset.gsRegionContainer;
        var elContainer = $modal.find('[' + dRegion + '="' + container + '"]');
        var isCn = $activeRegion.hasClass('form-wrapper--cn');

        $activeRegion.attr('open', true).removeClass(cVisuallyHidden);
        if (isCn) {
          $activeRegion.removeAttr('open');
        }

        $(sMainSettings, $modal).removeAttr('open');
        var regions = $modal.findAll('[' + dRegion + ']:not(' + activeRegion + ', ' + sMainSettings + ')');

        if (regions.length) {
          $.each(regions, function (region) {
            $.addClass(region, cVisuallyHidden);
          });
        }

        var elAside = $modal.find('.is-gs-aside');
        $.addClass(elAside, cVisuallyHidden);

        if ($.isElm(elContainer)) {
          $.removeAttr(elContainer, 'open');
          $.removeClass(elContainer, cVisuallyHidden);
        }
      }
    }
  }

  /**
   * Reacts on button `Edit variant` AJAX events.
   *
   * @param {Event} e
   *   The event triggered by a `mousedown` event.
   */
  // function fnOffcanvasSubmit(e) {
  // var dialog = $.find(_doc, '.ui-dialog-off-canvas');
  // if ($.isElm(dialog)) {
  // $.addClass(dialog, cLoading);
  // }
  //
  /**
   * GridStack functions.
   *
   * @param {HTMLElement} lb
   *   The #layout-builder HTML element.
   */
  function fnContainer(lb) {
    var container = $.find(lb, sContainer);

    if (!$.isElm(container)) {
      return;
    }

    var elLbSection = $.closest(container, sLbSection);
    var elLbConfigLink = $.find(elLbSection, '>' + sLbLinkConfig);

    // In case we have another wrapper (.gridstack-wrapper) above .gridstack.
    // We didn't hard-code class name previously since they might change.
    // This time is a willy-nilly. Also to support LB_UX.
    if (!$.isElm(elLbConfigLink)) {
      // layout-builder__actions.
      elLbConfigLink = $.find(elLbSection, '> div:first-child ' + sLbLinkConfig);
    }

    /**
     * Reacts on clicking non use-ajax link, fallback to AJAX.
     *
     * @param {Event} e
     *   The click event.
     */
    function fnActionStylesJs(e) {
      e.preventDefault();

      var link = e.target;
      // var cn = $.closest(link, sGridstackAction);
      var isOffCanvas = $(sOffcanvas).length;
      var $offCanvasForm;
      var elSection;
      var update = false;
      var ajax = true;

      if (isOffCanvas) {
        // $.removeClass(cn, cLoading);
        elSection = $.closest(link, sGsSection);

        // Clicking inside active section, show region quickly, no need an AJAX.
        if ($.isElm(elSection)) {
          ajax = false;
          update = true;
        }
        // Clicking outside active section, trying to edit cross-section.
        else {
          update = $.isElm(elLbConfigLink);
        }
      }
      else {
        // $.addClass(cn, cLoading);
        // setTimeout(function () {
        // $.removeClass(cn, cLoading);
        // }, 1000);

        update = $.isElm(elLbConfigLink);
      }

      fnRegionActivate(e);

      if (update) {
        if ($.isElm(elLbConfigLink) && ajax) {
          elLbConfigLink.href = link.href;
          elLbConfigLink.click();
        }

        setTimeout(function () {
          $offCanvasForm = $(sOffcanvasForm);
          if ($offCanvasForm.length) {
            $offCanvasForm.addClass(_isgs + '-form');
            fnShowActiveRegion($offCanvasForm[0], 'click');
          }
        }, isOffCanvas ? 1000 : 2000);
      }
    }

    /**
     * GridStack functions.
     *
     * @param {HTMLElement} elm
     *   The .gridstack__action HTML element.
     */
    function fnActionItem(elm) {
      var $elm = $(elm);
      var regionId = $elm.attr(dRegion);

      var addAttributes = function (link) {
        var href = link.pathname;
        if (href && !$.contains(href, 'gs=')) {
          var part = $.contains(href, '?') ? '&gs=' : '?gs=';
          href = href + part + regionId;
          $.attr(link, 'href', href);
          $.attr(link, dRegion, regionId);
          link.textContent = Drupal.t('Styles');
        }
      };

      if ($.isElm(elLbConfigLink)) {
        var cloned = elLbConfigLink;
        var elLinkJs = $.find(elm, '.' + cGsActionStylesJs);

        if (!$.isElm(elLinkJs)) {
          var elClone2 = cloned.cloneNode(false);

          $.addClass(elClone2, _lb + '__link--add ' + cGsActionStylesJs);
          $.removeClass(elClone2, 'use-ajax');
          $.removeAttr(elClone2, 'data-once data-dialog-type data-dialog-renderer');

          // Drupal.detachBehaviors(elClone2);
          elm.appendChild(elClone2);
        }

        elLinkJs = $.find(elm, '.' + cGsActionStylesJs);
        addAttributes(elLinkJs);

        $.on(lb, 'click.gsActionJs', '.' + cGsActionStylesJs, fnActionStylesJs, false);
      }
    }

    /**
     * Provides inline styles.
     */
    function fnInlineStyles() {
      var stylesId;
      var curStyles;
      var _prefix = _id + '--style-';
      var config = container.dataset.gsInlineStyles || {};

      function initStyles() {
        var opts = {
          id: stylesId,
          prefix: _prefix
        };
        var gridStyle = GridStyle.init(opts, container);

        stylesId = gridStyle.id;
        curStyles = gridStyle.styles;
      }

      function fnUpdateStyles() {
        if ($.isNull(curStyles) || $.isUnd(curStyles)) {
          initStyles();
        }

        if ($.isNull(curStyles) || $.isUnd(curStyles)) {
          return;
        }

        initStyles();

        if (config) {
          var i = 0;
          $.each(config, function (rule, selector) {
            GridStyle.insertCSSRule(curStyles, selector, rule, i);
            i++;
          });
        }
      }

      fnUpdateStyles();
    }

    fnInlineStyles();

    //  + ':not(.' + cGridstackAction + '--on)'
    var actions = $.findAll(container, sGridstackAction);
    if (actions.length) {
      $.each(actions, fnActionItem);
    }

    _attached = true;
    $.addClass(lb, cGsSection);
  }

  /**
   * Attaches gridstack behavior to HTML element .gridstack.is-gs-lb.
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.gridStackAdminLayout = {
    attach: function (context) {

      elBody = _doc.body;
      var $body = $(elBody);

      $body.off(_eLbTopLink).on(_eLbTopLink, sLbTopLink, fnOnLbTopLink);
      // $(sOffcanvasSubmit).off(_eOffcanvas).on(_eOffcanvas, fnOffcanvasSubmit);
      $.once(fnContainer, idLbSection, sLbSection, context);
    },
    detach: function (context, settings, trigger) {
      if (trigger === 'unload') {
        if (_attached) {
          elBody = _doc.body;
          var $body = $(elBody);
          $body.off(_eLbTopLink);

          $(sOffcanvasSubmit).off(_eOffcanvas);

          $.once.removeSafely(idLbSection, sLbSection, context);
        }
      }
    }
  };

  // @todo update when core removes jQuery UI dialog, likey D10+.
  // Drupal off-canvas register these events as jQuery object, not native.
  if (jQuery) {
    jQuery(_win).on({
      'dialog:afterclose': fnOnModalClosed,
      'dialog:aftercreate': fnOnModalOpened
    });
  }

})(dBlazy, Drupal, Drupal.offCanvas, GridStyle, this, this.document);
