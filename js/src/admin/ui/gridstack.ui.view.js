/**
 * @file
 * Provides GridStack admin UI utilities using backbone.
 */

(function ($, Drupal) {

  'use strict';

  var _dgs = 'data-gs-';

  $.gsa = $.gsa || {};
  $.gsa.views = $.gsa.views || {};
  $.gsa.models = $.gsa.models || {};

  /**
   * The GridStack box item View.
   */
  $.gsa.views.Box = $.gsa.View.extend({
    model: $.gsa.models.Box,
    className: 'gridstack__box box box--root grid-stack-item',
    tagName: 'div',
    props: [
      'isDefault',
      'isMain',
      'isNested',
      'isRegion',
      'gid',
      'id',
      'index'
    ],
    isMain: false,
    isNested: false,
    isRegion: false,

    events: {
      change: 'render'
    },

    render: function () {
      var me = this;
      var box = me.model;
      var elBox = me.el;
      var content = $.find(elBox, '> .box__content');

      if (!$.isElm(content)) {
        $.append(elBox, Drupal.theme('gridStackContent', {
          id: box.id,
          isMain: me.isMain,
          isNested: me.isNested,
          isRegion: me.isRegion,
          type: 'root'
        }));
      }

      // @todo remove index for bid.
      $.each(['bid', 'index'], function (key) {
        $.attr(elBox, _dgs + key, box.index);
      });

      $.attr(elBox, 'id', box.id);

      // No need to re-do GridStack engine here on.
      var btns = $.findAll(elBox, '.btn');
      $.each(btns, function (btn) {
        $(btn).attr(_dgs + 'bid', box.index);
      });

      // Nested boxes are not models, provides context to its model via box ID.
      var nesteds = $.findAll(elBox, '.gridstack--nested');
      if (nesteds.length) {
        $.each(nesteds, function (nested) {
          $.attr(nested, _dgs + 'bid', box.index);
        });
      }

      return me;
    }
  });

})(dBlazy, Drupal);
