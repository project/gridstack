/**
 * @file
 * Provides GridStack CRUD.
 */

(function ($, Drupal, _doc) {

  'use strict';

  var _dgs = 'data-gs-';

  $.gsa = $.gsa || {};

  /**
   * The GridStack region methods using HTML5 autocomplete.
   *
   * @see https://developer.mozilla.org/en-US/docs/Web/HTML/Element/datalist
   */
  var regionMethods = {
    selector: '.form-text--region',
    templateContent: null,

    template: function () {
      var me = this;

      if (!me.templateContent) {
        var template = _doc.getElementById('gridstack-regions');
        var regionData = template.dataset.gsRegions.trim().split(' ');
        var fragment = _doc.createDocumentFragment();
        var select = _doc.createElement('select');

        template.appendChild(select);
        $.each(regionData, function (region) {
          var option = _doc.createElement('option');
          option.textContent = region;
          fragment.appendChild(option);
        });

        select.appendChild(fragment);

        me.templateContent = select;
      }
    },

    // See https://developer.mozilla.org/en-US/docs/Web/HTML/Element/datalist
    onInput: function (el) {
      var me = this;
      var datalist = el.nextElementSibling;
      var templateContent = me.templateContent;

      // SO 15097563.
      var keyup = function (e) {
        if (me.host.isValid(datalist)) {
          while (datalist.children.length) {
            datalist.removeChild(datalist.firstChild);
          }

          var term = me.sanitize(el.value);
          var clonedOptions = templateContent.cloneNode(true);
          var startsWithMatcher = new RegExp('^' + term, 'i');

          var set = Array.prototype.reduce.call(clonedOptions.children, function searchFilter(frag, el) {
            if (startsWithMatcher && startsWithMatcher.test(el.textContent) && frag.children.length < 60) {
              frag.appendChild(el);
            }
            return frag;
          }, _doc.createDocumentFragment());

          datalist.appendChild(set);
        }
      };

      $.on(el, 'keyup', Drupal.debounce(keyup, 100));
    },

    initialize: function (host) {
      var me = this;

      me.host = host;

      me.template();
      setTimeout(function () {

        if (host && host.el) {
          var selectors = $.findAll(host.el, me.selector);
          $.each(selectors, me.autocomplete.bind(me));
        }
      });
    },

    event: function (el, elBox) {
      var me = this;

      if ($.isElm(elBox)) {
        me.setId(elBox);
      }

      me.onInput(el);
      me.onBlur(el);
    },

    autocomplete: function (el) {
      this.event(el, $.closest(el, '.box'));
    },

    getInput: function (elBox) {
      return $.find(elBox, '> .box__content > .ui-widget--region ' + this.selector);
    },

    update: function (elBox, node, type) {
      var me = this;
      var elRegion = me.getInput(elBox);
      var host = me.host;

      type = type || 'root';

      if (host && host.isRegion && $.isElm(elRegion) && host.isValid(node.region)) {
        elRegion.value = node.region;
        $.attr(elRegion, _dgs + 'region-value', node.region);

        me.updatePlaceholder(elBox);
      }
    },

    setId: function (elBox) {
      var me = this;

      var elRegion = me.getInput(elBox);
      if ($.isElm(elRegion)) {
        var id = 'region' + elBox.dataset.gsBid;
        var datalist = $.next(elRegion, 'datalist');

        $.attr(elRegion, 'list', id);
        $.attr(datalist, 'id', id);
      }
    },

    updatePlaceholder: function (elBox, hasChildren) {
      var me = this;
      var elNested = $.find(elBox, '.gridstack--nested');

      me.setId(elBox);

      if ($.isUnd(hasChildren)) {
        hasChildren = $.isElm(elNested) &&
          elNested.children &&
          elNested.children.length;
      }

      if ($.isElm(elNested)) {
        var elRegion = me.getInput(elBox);
        $.attr(elRegion, 'placeholder', hasChildren ? 'container' : 'region');
      }
    },

    onBlur: function (el) {
      var me = this;

      $.on(el, 'blur.gsac', function (e) {
        var value = me.sanitize(e.target.value);
        e.target.value = value;
        me.onBlurOrSelected(e, value);
        return false;
      });
    },

    onBlurOrSelected: function (e, value) {
      var me = this;
      var host = me.host;
      var target = e.target;
      var root = $.closest(target, '.box--root');
      var elNestedBox = $.closest(target, '.is-box-nested');
      var box = host.getModel(root);

      if (host.isValidModel(box) && !$.isEmpty(value)) {
        value = me.sanitize(value);
        value = host.isMain ? value : null;

        if ($.isElm(elNestedBox)) {
          var index = $.index(elNestedBox);

          if (box.nested && host.isValid(box.nested[index])) {
            box.nested[index].region = value;
          }
        }
        else {
          box.region = value;
        }
      }
    },

    sanitize: function (value) {
      // SO 23187013.
      // value = value.replace(/[^A-Za-z]/g, '');
      value = value.replace(/[^a-z0-9áéíóúñü .,_-]/gim, '').toLowerCase().trim();
      value = value.split('-').join('_');
      value = value.split(' ').join('_');
      value = Drupal.checkPlain(value);
      return value;
    }

  };

  $.gsa.models.Region = $.gsa.Model.extend(regionMethods);

  /**
   * The GridStack nested methods.
   */
  var nestedMethods = {

    onNestedRemoveMultiple: function (e, box, elBox) {
      var me = this;
      var delta = $.index(elBox);

      if (box && me.gid === box.gid) {
        if (box.nested.length) {
          box.nested.splice(delta, 1);
        }
        // Content box is not a model, no need to remove box model.
        me.removeWidget(elBox, 'nested', box);
      }
    },

    onNestedAddMultiple: function (e, box, elBox) {
      var me = this;

      me.isRendered = true;
      if (me.isValidModel(box)) {
        var el = $.find(elBox, '.gridstack--nested');

        if ($.isElm(el) && me.gid === box.gid) {
          me.addNestedWidget(box, el, true);
        }
      }
    },

    // See ::addWidget(node, el, isDefault, gridStack)
    addNestedWidget: function (box, elm, isDefault, data) {
      var me = this;
      var index = box.index;
      var gridStack = GridStack.init(me.nestedOptions, elm);
      var len = gridStack.engine.nodes.length || 0;

      data = data || [];

      var addNestedItem = function (node, i) {
        var indexNested = isDefault ? len + 1 : i + 1;

        // Nested box is not a model, only has similar attributes.
        if (me.isValidNode(node)) {
          var nestedBox = me.getFakeModel(node, index, indexNested);

          var elBox = me.addWidget(nestedBox, elm, isDefault, gridStack);
          me.updateBoxNested(elBox, nestedBox, box, i);
        }
      };

      if (isDefault) {
        data = box.defaults;
        data.gid = box.gid;
        data.id = box.id;
        data.index = box.index;
        data.pindex = box.index;
        data.indexNested = len + 1;

        addNestedItem(data);
      }
      else if (data.length && me.isValidNode(data[0])) {
        gridStack.batchUpdate();

        $.each(data, addNestedItem);

        gridStack.commit();
      }

      me.toggleNestedEmpty(elm);
      me.emit('change', box);

      // Nested boxes are not models, hook into gridstack events.
      gridStack.on('added removed', function (e, items) {
        me.onNestedAddedRemoved(e, items, gridStack);
      });

      gridStack.on('resizestop', function (e, el) {
        var node = el.gridstackNode;

        me.updateBoxNested(el, node, box);
      });

      return gridStack;
    },

    toggleNestedEmpty: function (el) {
      var me = this;
      var hasChildren = el.children.length;
      var elBox = $.closest(el, '.box--root');

      if ($.hasClass(el, 'gridstack--nested')) {
        $[hasChildren ? 'addClass' : 'removeClass'](el, 'is-nested-not-empty');

        if ($.isElm(elBox) && me.region && me.region.updatePlaceholder) {
          me.region.updatePlaceholder(elBox, hasChildren);
        }
      }
    },

    onNestedAddedRemoved: function (e, items, gridStack) {
      this.toggleNestedEmpty(e.target);
    },

    addNestedWidgetMultiple: function (box, i, data) {
      var me = this;
      var id = box.id;

      var el = $.find(me.el, '> #' + id + ' .gridstack--nested');

      return $.isElm(el) ? me.addNestedWidget(box, el, false, data) : null;
    },

    updateBoxNestedMultiple: function (box, i, root, grids) {
      var me = this;

      if (!me.isValidModel(box)) {
        return;
      }

      var index = box.index;
      var elNested = $.find(root, '.gridstack--nested');
      var elBoxes = $.isElm(elNested) ?
        $.findAll(elNested, '> .is-box-nested') : [];

      // Might be empty, even if not empty here due to sequence, or delay.
      if (elBoxes.length) {
        $.each(elBoxes, function (el, j) {
          var node = el.gridstackNode;
          var elRegion;

          if (me.isValidNode(node)) {
            var data = me.getFakeModel(node, index, (j + 1));
            var region = null;

            if ($.isUnd(box.nested[j])) {
              data.index = index;
              data.indexNested = (j + 1);
              box.nested[j] = data;
            }

            var valid = box.nested.length && me.isValidNode(box.nested[j]);
            if (me.isMain && me.isRegion) {
              elRegion = $.find(el, me.region.selector);
              var value = $.isElm(elRegion) ? elRegion.value : null;

              if (valid && !$.isUnd(box.nested[j].region)) {
                region = box.nested[j].region;
              }

              // Value is unreliable, must rely on box model data, if any.
              data.region = region || value;
            }

            if (valid) {
              box.nested[j] = data;
            }

            me.updateBoxNested(el, data, box, i);
          }
        });
      }
    },

    updateBoxNested: function (elBox, node, box, i) {
      var me = this;
      var $box = $(elBox);
      var btns = $box.findAll('.btn');

      var indexNested = node.indexNested;

      $box.attr(_dgs + 'bid', node.nid);
      $box.attr(_dgs + 'index', indexNested);
      $box.addClass('is-box-nested');

      if (me.region && me.region.update) {
        me.region.update(elBox, node, 'nested');
      }

      $.each(btns, function (btn) {
        $.attr(btn, _dgs + 'bid', node.nid);

        if (!$.isUnd(node.mid)) {
          $.attr(btn, _dgs + 'mid', node.mid);
        }
      });
    }
  };

  /**
   * The GridStack crud methods.
   */
  $.gsa.crud = $.extend(nestedMethods, {
    region: {},
    events: {
      resizestop: 'onResizeStop',
      drag: 'onDrag'
    },

    add: function (index) {
      var me = this;
      var data = {
        gid: me.gid,
        index: index
      };

      me.extra(data, 'root', true);

      var box = new $.gsa.models.Box(data);
      me.collection.add(box);

      return me.collection;
    },

    remove: function (box) {
      var me = this;
      if (me.gid === box.gid) {
        me.collection.remove(box);
      }
      return me.collection;
    },

    onChange: function (box) {
      var me = this;
      if (box && (me.gid === box.gid)) {
        this.save(box);
      }
    },

    onSave: function () {
      var me = this;

      me.options.updateIcon = true;
      me.save();
    },

    _onDragResize: function (e) {
      var me = this;
      var elBox = e.target;
      var node = elBox.gridstackNode;
      var box = me.getModel(elBox, me.collection);

      if (box && (me.gid !== box.gid)) {
        return;
      }

      // This is GridStack library selector.
      // Only a nested box needs manual update.
      // @todo move it to view along with the model if you can.
      if (!$.hasClass(elBox, 'is-box-nested')) {
        if (me.isValidModel(box) && me.isValidNode(node)) {
          box.w = node.w;
          box.h = node.h;
        }
      }

      // Manually trigger change since this event is not backbone's.
      me.emit('change', box);
    },

    onDrag: function (e) {
      $.throttle(this._onDragResize(e), 200, this);
    },

    onResizeStop: function (e) {
      var me = this;

      // @todo $.throttle(this._onDragResize(e), 200, this);
      me._onDragResize(e);
    },

    onAdd: function (boxes) {
      var me = this;

      $.each(boxes, function (box) {
        if (me.gid === box.gid) {
          var elBox = me.addWidgetDefault(box);

          box = me.getModel(elBox);
          $.each(['id', 'x', 'y', 'w', 'h'], function (key) {
            box.attributes[key] = box[key];
          });

          me.save(box);
        }
      });
    },

    widgetOptions: function (node, extra) {
      var me = this;
      // v0.5.2, https://github.com/gridstack/gridstack.js/issues/907
      return $.extend({
        x: node.x,
        y: node.y,
        w: node.w,
        h: node.h,
        // Without autoPosition, new widgets are prepended, not appended.
        autoPosition: me.nodeConfig.autoPosition || true,
        // Dont! The box can not get smaller than 12 columns.
        // minW: me.nodeConfig.minW || 1
        // This can get absurd to 225+, add a max.
        // minH: me.nodeConfig.minH || 1,
        maxH: me.nodeConfig.maxH || 60
      }, extra || {});
    },

    widgetHtml: function (box, el, type, isDefault, bid, asElm) {
      var me = this;
      var rendered;

      box.gid = me.gid;
      box.id = me.gid + '-' + box.index;
      box.isDefault = isDefault;
      box.isNested = me.isNested;

      if (type === 'root') {
        box.isMain = me.isMain;
        box.isRegion = me.isRegion;

        var view = me.getCurrentView(box);
        if (me.isValid(view)) {
          rendered = view.render().el;

          return asElm ? rendered : rendered.outerHTML;
        }
      }

      // Prevents infinite nests.
      var nested = me.isNested && (el && !$.hasClass(el, 'gridstack--nested'));
      rendered = Drupal.theme('gridStackBox', {
        id: box.id || bid,
        isMain: me.isMain,
        isNested: nested,
        isRegion: me.isRegion
      });

      var cn = $.create('div', false, rendered);
      return asElm ? cn.firstChild : rendered;
    },

    getWidgetElement: function (box) {
      return this.addWidget(box, null, false, null, true);
    },

    addWidgetDefault: function (box) {
      return this.addWidget(box, null, true);
    },

    addWidget: function (box, el, isDefault, gridStack, asElm) {
      var me = this;
      var node = box;
      var extra = {};
      var widget;
      var isFakeModel = false;
      var bid = 0;
      var type;

      el = el || me.el;

      // Unlike nested, the root boxes are valid backbone models.
      if (me.isValidModel(box)) {
        type = 'root';
        bid = box.id;
      }
      else {
        type = 'nested';
        isFakeModel = true;
      }

      extra = me.extra(node, type, isDefault);
      widget = me.widgetHtml(node, el, type, isDefault, bid, asElm);

      // var params = {
      // x: node.x,
      // y: node.y,
      // w: node.w,
      // h: node.h,
      // autoPosition: true
      // };
      // if (!gridStack.willItFit(params)) {
      // return;
      // }
      gridStack = gridStack || me.getGridStack(el);
      if (!me.isValid(gridStack)) {
        return widget;
      }

      var elBox = gridStack.addWidget(widget, me.widgetOptions(node, extra));
      if (!$.isUnd(el) && $.isUnd(elBox.context)) {
        elBox.context = el.length ? el[0] : el;
      }

      // The ::addWidget returns non-jquery object without length.
      if (me.region && me.region.selector) {
        var elRegion = $.find(elBox, me.region.selector + ':first-child');

        if (isDefault && $.isElm(elRegion)) {
          me.region.event(elRegion, elBox);
        }
      }

      if (!isFakeModel) {
        if (me.region && me.region.update) {
          me.region.update(elBox, node, 'root');
        }
      }

      // Disable movable, as too complex to handle with breakpoints for now.
      var root = $.closest(elBox, '.gridstack--root');
      if (me.isNested && $.isElm(root)) {
        gridStack.movable(elBox, false);
      }

      // Must trigger manually to apply attributes on added widgets.
      if (isDefault || me.isRendered) {
        gridStack.compact();
        me.emit('change', box);
      }
      return elBox;
    },

    extra: function (node, type, isDefault) {
      var me = this;
      // var bid = 0;
      var defaultWidth = me.nodeConfig.defaultMinWidth || 2;
      var extra = {};

      if (type === 'root') {
        extra.minW = me.isNested ?
          me.nodeConfig.nestedMinWidth : me.nodeConfig.rootMinWidth;

        defaultWidth = me.isNested ? 12 : defaultWidth;

        if (me.isSmallest) {
          defaultWidth = 1;

          if (me.isNested) {
            extra.minW = 12;
          }
        }

        if (me.column < 12 && me.column > 4) {
          extra.maxW = me.column;
        }
      }
      else {
        if (me.isSmallest) {
          extra.minW = 6;

          if (me.isNested) {
            defaultWidth = 12;
          }
        }
      }

      if (isDefault) {
        node.h = me.nodeConfig.defaultMinHeight || 2;
        node.w = defaultWidth;
      }

      return extra;
    },

    shouldRemove: function (box) {
      return box.deleted === true;
    },

    removeWidget: function (elBox, context, box) {
      var me = this;

      context = $.isUnd(context) ? 'root' : context;

      if (!$.isElm(elBox)) {
        return;
      }

      box = box || me.getModel(elBox);
      var el = $.closest(elBox, '.gridstack');

      // Do not remove directly, instead set properties, and collect it later
      // to avoid potential memory leaks.
      if (me.isValidModel(box)) {
        box.deleted = context === 'root';
      }

      // @todo move it into model if you can.
      // This can be nested or main gridstack.
      var gridStack = me.getGridStack(el);

      if (me.isValid(gridStack)) {
        gridStack.removeWidget(elBox, true);
      }
    },

    onRootRemoveMultiple: function (e, box, elBox) {
      var me = this;

      // Destroy the box including all nested instances.
      if (me.gid === box.gid) {
        me.removeWidget(elBox, 'root', box);
      }
    },

    updateBoxModel: function (box, i, reIndex, grids) {
      var me = this;
      var index = (i + 1);

      box = me.isValidModel(box) ? box : me.collection.models[i];
      if (!me.isValidModel(box)) {
        return;
      }

      var id = box.id;
      var elBox = me.getBoxById(id);

      if (!$.isElm(elBox)) {
        return;
      }

      var node = elBox.gridstackNode;
      var data = me.getParsedNode(node);

      if (reIndex) {
        data.index = index;
        $.attr(elBox, _dgs + 'index', index);
      }

      if (!$.isEmpty(data)) {
        $.each(data, function (value, key) {
          box[key] = value;
        });
      }

      // Update nested boxes if required.
      if (me.isNested) {
        me.updateBoxNestedMultiple(box, i, elBox, grids);
      }

      me.emit('change', box);
    },

    updateBoxModelMultiple: function () {
      var me = this;
      var removes = [];
      var reIndex = false;
      var grids = $.gsa.base.save(me.gridStack);

      $.each(me.collection.models, function (box, i) {
        if (me.shouldRemove(box)) {
          removes.push(box);
        }
      }, me);

      if (removes.length) {
        reIndex = true;
        me.collection.remove(removes);
      }

      $.each(me.collection.models, function (box, i) {
        me.updateBoxModel(box, i, reIndex, grids);
      }, me);

      $.gsa.base.lastBoxIndex = me.collection.length;
    },

    save: function (box) {
      var me = this;

      if (box) {
        if (me.gid === box.gid) {
          me.updateBoxModelMultiple();
        }
      }
      else {
        me.updateBoxModelMultiple();
      }

      if (me.saveCallback && me.isRendered) {
        var o = me.options || {};

        me.saveCallback.callback(me.collection, o);
      }

      me.options.updateIcon = false;
    }

  });

})(dBlazy, Drupal, this.document);
