/**
 * @file
 * Provides GridStack admin UI utilities using backbone.
 */

(function ($, GridStack, eventify) {

  'use strict';

  $.gsa = $.gsa || {};
  $.gsa.views = $.gsa.views || {};

  /**
   * The GridStack views.
   */
  $.gsa.views.GridStack = $.gsa.View.extend($.extend({
    region: {},
    gridStack: null,
    wrapper: '.gridstack-wrapper',

    listenTo: function (e, cb, bound) {
      var me = this;
      var obj = me.collection;
      if ($.isUnd(bound)) {
        bound = true;
      }
      if (obj && obj.on) {
        obj.on(e, bound ? cb.bind(me) : cb);
      }
    },

    initialize: function (o) {
      o = o || {};

      var me = this;
      var el = me.el = o.el;
      var base = $.gsa.base;

      // Merge all options as properties for quick access.
      me = $.extend(me, o.options);

      // @fixme false causes failing style being applied.
      o.config.styleInHead = true;
      if (!o.config.resizable) {
        o.config.resizable = {
          handles: 'e, se, s, sw, w'
        };
      }

      var config = me.config = o.config;

      me.gid = me.id.replace('gridstack-', '');
      base.options = $.extend({}, o.options, config);
      me.saveCallback = o.saveCallback;

      // Provides common shortcuts.
      me.options = base.options;
      me.gridStackOptions = config;

      var nestedConfig = o.configNested;
      nestedConfig.styleInHead = true;
      me.nestedOptions = nestedConfig;

      me.column = config.column;
      me.isRendered = false;

      // me.gridStack = GridStack.init(config, el);
      // me.nodes = GridStack.Utils.sort($.gsa.base.newData(el));
      // me.oldNodes = GridStack.Utils.sort($.gsa.base.oldData(el));
      me.$wrapper = $.closest(el, me.wrapper);

      me.checkCollection();
      me.init();

      if (me.collection && me.collection.length) {
        // Provides context to collection.
        me.addCollectionHint(me.collection);

        me.listenTo('add', me.onAdd);
        me.listenTo('change', $.throttle(me.onChange.bind(me), 150, me), false);

        // Main buttons.
        me.listenTo('gridstack:main:save', $.throttle(me.onSave.bind(me), 200, me), false);

        // Root box buttons.
        me.listenTo('gridstack:root:remove', me.onRootRemoveMultiple);

        // Nested grids are css-driven: Bootstrap + Foundation.
        if (me.isNested) {
          me.listenTo('gridstack:root:add', me.onNestedAddMultiple);

          // Content has no .btn--add to avoid complication with deep nesting.
          me.listenTo('gridstack:nested:remove', me.onNestedRemoveMultiple);
        }
      }
    },

    addCollectionHint: function (collection) {
      var me = this;

      // Provides context to collection.
      collection._gid = me.id;
      collection._isMain = me.isMain;
      collection._isNested = me.isNested;
      collection._isRegion = me.isRegion;
    },

    build: function () {
      var me = this;
      var root = me.el;

      if (!me.collection) {
        return;
      }

      if (me.isRegion) {
        me.region = new $.gsa.models.Region(me);
        me.region.initialize(me);
      }

      $.gsa.base.lastBoxIndex = me.collection.length;
      var models = me.collection.models;

      if (models && models.length) {
        $.each(models, function (box, i) {
          if (box.id) {
            var isNested = me.isNested && me.nestedNodes.length;
            if (isNested) {
              box.nested = me.nestedNodes[i] || [];
            }

            // Build the main boxes for each breakpoint: xs, sm, md, lg, xl.
            me.addWidget(box);

            // Build nested data boxes, if any.
            if (isNested) {
              me.addNestedWidgetMultiple(box, i, me.nestedNodes[i]);
            }
          }
        });
      }
      return root;
    },

    checkCollection: function () {
      var me = this;
      var el = me.el;

      me.collection = $.gsa.ui.collect(el, false, false);
      me.collection.id = me.gid;

      me.nestedNodes = me.isNested ? $.gsa.base.newData(el, true) : [];
    },

    init: function () {
      var me = this;
      var el = me.el;

      // Initialize GridStack root instances.
      var grid = me.gridStack = GridStack.init(me.config, el);

      // Update grid column.
      $.addClass(el, 'grid-stack-' + me.column);
      grid.column(me.column);

      grid.on('resizestop', me.onResizeStop.bind(me));

      return grid;
    },

    render: function (root) {
      var me = this;

      me.build();

      me.isRendered = true;
      return me;
    },

    isValid: function (object) {
      return !$.isNull(object) && !$.isUnd(object);
    },

    isValidModel: function (box) {
      return this.isValid(box) && box instanceof $.gsa.models.Box;
    },

    getModel: function (el, collection) {
      return this.getModelById(this.getModelId(el), collection);
    },

    getModelId: function (el) {
      return $.gsa.base.getModelId(el);
    },

    getModelById: function (id, collection) {
      collection = collection || this.collection;

      return collection.models.find(function (item) {
        return item.id === id;
      });
    },

    getBoxById: function (id, el) {
      return $.find(el || this.el, '> #' + id);
    },

    isValidNode: function (node) {
      return this.isValid(node) && (!$.isUnd(node.x) || !$.isUnd(node.w));
    },

    getParsedNode: function (node) {
      return $.gsa.base.node(node);
    },

    getFakeModel: function (node, index, indexNested) {
      if (index && $.isNum(index)) {
        node.index = index;
      }

      if (indexNested && $.isNum(indexNested)) {
        node.indexNested = indexNested;
      }

      return $.extend({}, this.getParsedNode(node), $.gsa.base.attributes(node));
    },

    hasGridStack: function (el) {
      el = $.toElm(el);
      return el && el.gridstack;
    },

    getGridStack: function (el) {
      el = $.toElm(el);
      return this.hasGridStack(el) ? el.gridstack : this.gridStack;
    },

    getCurrentView: function (box) {
      return this.isValidModel(box) ? new $.gsa.views.Box({
        model: box
      }) : null;
    }

  }, $.gsa.crud));

})(dBlazy, GridStack, Eventify);
