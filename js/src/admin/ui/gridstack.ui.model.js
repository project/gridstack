/**
 * @file
 * Provides GridStack Model.
 */

(function ($, eventify) {

  'use strict';

  $.gsa = $.gsa || {};
  $.gsa.models = $.gsa.models || {};
  $.gsa.collections = $.gsa.collections || {};
  var noop = function () {};

  var _defaults = {
    initialize: function (o) {},
    attributes: {},
    collection: {},
    options: {},
    preinitialize: noop
  };

  // An internal function for creating assigner functions.
  function createAssigner(keysFunc, defaults) {
    return function (obj) {
      var length = arguments.length;
      if (defaults) {
        obj = Object(obj);
      }

      if (length < 2 || obj == null) {
        return obj;
      }

      for (var index = 1; index < length; index++) {
        var source = arguments[index];
        var keys = keysFunc(source);
        var l = keys.length;
        for (var i = 0; i < l; i++) {
          var key = keys[i];
          if (!defaults || obj[key] === void 0) {
            obj[key] = source[key];
          }
        }
      }
      return obj;
    };
  }
  var extendOwn = createAssigner(Object.keys);

  function create(prototype, props) {
    var result = prototype ? Object.create(prototype) : {};
    if (props) {
      extendOwn(result, props);
    }
    return result;
  }

  // Helper function to correctly set up the prototype chain for subclasses.
  // Similar to `goog.inherits`, but uses a hash of prototype properties and
  // class properties to be extended.
  var extend = function (protoProps, staticProps) {
    var parent = this;
    var child;

    // The constructor function for the new subclass is either defined by you
    // (the "constructor" property in your `extend` definition), or defaulted
    // by us to simply call the parent constructor.
    if (protoProps && $.hasProp(protoProps, 'constructor')) {
      child = protoProps.constructor;
    }
    else {
      child = function () {
        return parent.apply(this, arguments);
      };
    }

    // Add static properties to the constructor function, if supplied.
    $.extend(child, parent, staticProps || {});

    // Set the prototype chain to inherit from `parent`, without calling
    // `parent`'s constructor function and add the prototype properties.
    child.prototype = create(parent.prototype, protoProps);
    child.prototype.constructor = child;

    // Set a convenience property in case the parent's prototype is needed
    // later.
    child.__super__ = parent.prototype;

    return child;
  };

  var Model = $.gsa.Model = function (attributes, options) {
    var attrs = attributes || (attributes = {});
    options = options || (options = {});

    var me = eventify(this);

    if (me.preinitialize) {
      me.preinitialize.apply(me, arguments);
    }

    if (options.collection) {
      me.collection = $.extend({}, me.collection, options.collection);
    }

    var defts = $.isFun(me.defaults) ? me.defaults() : me.defaults;
    if (defts) {
      $.extend(me, defts || {}, options);
    }

    if (me.initialize) {
      me.initialize.apply(me, arguments);

      if (attrs.x) {
        me.attributes = attrs;
        me.changedAttributes = {};

        var change = function () {
          $.each(attrs, function (val, key) {
            me.changedAttributes[key] = me[key];
          });
        };

        me.on('change', $.throttle(change, 200));
      }
    }

    return me;
  };

  var mp = Model.prototype;
  $.extend(mp, _defaults);

  var Collection = function (models, options) {
    options = options || (options = {});

    var me = eventify($.extend(this, options));

    me.preinitialize.apply(me, arguments);
    if (options.model) {
      me.model = options.model;
    }

    me._reset();

    me.initialize.apply(me, arguments);
    me.models = models;
    me.length = models.length;

    return me;
  };

  var cp = Collection.prototype;

  $.extend(cp, _defaults, {
    add: function (models) {
      var me = this;

      models = $.toArray(models);

      Array.prototype.push.apply(me.models, models);

      me.length = me.models.length;

      me.emit('add', models);
      return me.models;
    },
    remove: function (removes) {
      var me = this;
      var models = me.models;

      if ($.isArr(removes)) {
        models = models.filter(function (item) {
          return !removes.includes(item);
        });
      }
      else {
        models = models.filter(function (item) {
          return item !== removes;
        });
      }

      me.emit('remove', removes);

      me.length = models.length;
      me.models = models;

      return models;
    },
    _reset: function () {
      this.length = 0;
      this.models = [];
    }

  });

  $.gsa.Collection = Collection;

  var View = $.gsa.View = function (options) {
    options = options || (options = {});

    var me = eventify($.extend(this, options));

    var defts = $.isFun(me.defaults) ? me.defaults() : me.defaults;
    me.preinitialize.apply(me, arguments);
    me.options = $.extend({}, defts || {}, options.options);

    me._ensureElement();

    me.initialize.apply(me, arguments);

    var box = me.model || false;

    if (box && me.props) {
      $.each(me.props, function (prop) {
        if (!$.isUnd(box[prop])) {
          me[prop] = box[prop];
        }
      });
    }

    return me;
  };

  var vp = View.prototype;

  $.extend(vp, _defaults, {
    render: function () {
      return this;
    },
    _ensureElement: function () {
      var me = this;
      if (!me.el) {
        var attrs = {};
        var id = me.id || me.attributes.id;
        if (id) {
          attrs.id = id;
        }
        if (me.className) {
          attrs['class'] = me.className;
        }

        me.el = $.create(me.tagName || 'div', attrs);
      }
    }
  });

  // Set up inheritance for the model, collection, and view.
  Model.extend = Collection.extend = View.extend = extend;

  /**
   * The GridStack box item models.
   */
  var ModelBox = Model.extend({
    defaults: $.extend($.gsa.base.getDefaultBox(), {
      id: null,
      index: 1,
      nested: [],
      gid: null,
      region: null,
      deleted: false
    }),

    getDataToSave: function (node) {
      var me = this;

      node = $.extend(me, node || {});

      if (
        me.collection &&
        !$.isUnd(me.collection._isMain) &&
        !me.collection._isMain
      ) {
        node.region = null;
      }

      return $.gsa.base.node(node);
    },

    getDataNestedToSave: function () {
      var me = this;
      var nested = [];
      var nodes = me.nested || [];

      if (nodes.length) {
        nested = nodes.map(function (node) {
          return me.getDataToSave(node) || [];
        });
      }

      return nested;
    },

    initialize: function (o) {
      var me = this;

      o = o || {};

      var attrs = $.gsa.base.attributes(o);
      me.attributes = $.extend(o, attrs);
      me = $.extend(me, attrs);
    }
  });

  $.gsa.models.Box = ModelBox;

})(dBlazy, Eventify);
