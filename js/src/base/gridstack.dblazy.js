/**
 * @file
 * Provides dBlazy plugin.
 */

(function ($, _win, _doc) {

  'use strict';

  $.hide = function (el) {
    if ($.isElm(el)) {
      el.style.display = 'none';
    }
  };

  $.show = function (el) {
    if ($.isElm(el)) {
      el.style.display = '';
    }
  };

  $.toIntAttr = function (el, attr) {
    return parseInt($.attr(el, attr), 0);
  };

  $.toIntCss = function (el, attr) {
    return parseInt($.css(el, attr), 0);
  };

})(dBlazy, this, this.document);
