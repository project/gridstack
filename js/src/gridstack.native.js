/**
 * @file
 * Provides utilities for native browser CSS Grid layout.
 */

(function ($, Drupal) {

  'use strict';

  var _idOnce = 'gsnative';
  var _nick = 'gn';
  var _mounted = 'is-' + _nick + '-on';
  var _selector = '.gridstack--native:not(.' + _mounted + ')';

  $.gridstack = $.gridstack || {};

  /**
   * GridStack native front-end public methods.
   *
   * @namespace
   */
  $.gridstack.native = $.extend({}, $.gridstack.fe || {}, {

    onClass: _nick,

    fixAspectRatio: function (el, e) {
      var me = this;
      var ch = me.options.cellHeight;
      var ok = ch === 'auto' || (me.cellHeight >= (ch / 2) && me.cellHeight < (ch * 2));
      var vm = me.vm > 0 ? (me.vm / 2) : 0;

      if (me.isEnabled(el)) {
        if (me.vm > 0) {
          el.style.gap = me.vm + 'px';
        }

        // Best if the gap or me.vm = 0.
        // @todo bad: var chvm = +(Math.round((me.cellHeight - vm) / 10).toFixed(2));
        var chvm = (me.cellHeight - vm) / 10;

        // Prevents weird calc during resizing.
        chvm = $.isUnd(e) ? chvm : (me.cellHeight / 10);

        // The magic .5, surprisingly making perfect square with gap 0.
        var magic = me.vm > 0 ? 1 : .5;
        me.cellHeight = ok ? (chvm * 10) + magic : ch;
        el.style.gridAutoRows = me.cellHeight + 'px';
      }
    },

    postUpdateGrid: function (el, e) {
      var me = this;

      if (me.isEnabled(el)) {
        // As usual, IE is always a PITA. Give em some love.
        var fr = [];
        $.each(me.range(0, me.column(el), 1), function () {
          fr.push(['1fr']);
        });

        // -ms-grid-columns: 1fr 1fr 1fr 1fr;
        // grid-template-columns: repeat(4, 1fr);
        el.style.msGridColumns = fr.join(' ');
        el.style.gridTemplateColumns = 'repeat(' + me.column(el) + ', 1fr)';
      }
    },

    updateAttribute: function (box, item, el, e) {
      $.attr(box, 'gs-w', item[2]);
      $.attr(box, 'gs-h', item[3]);
    },

    init: function (el) {
      var me = this;

      me.$el = el;

      me.prepare(el);
      me.buildOut(el);
      me.cleanUp(el);
    }

  });

  /**
   * Attaches behavior to HTML element identified by .gridstack--native.
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.gridStackNative = {
    attach: function (context) {
      var me = $.gridstack.native;
      $.once(me.init.bind(me), _idOnce, _selector, context);
    },
    detach: function (context, settings, trigger) {
      if (trigger === 'unload') {
        $.once.removeSafely(_idOnce, _selector, context);
      }
    }
  };

})(dBlazy, Drupal);
