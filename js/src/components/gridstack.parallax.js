/**
 * @file
 * Provides dead-simple parallax utilities.
 */

(function ($, Drupal, Bio, _doc) {

  'use strict';

  var _hit = 0;
  var _idOnce = 'gsparallax';
  var _isgs = 'is-gs-';
  var _mounted = _isgs + 'parallax-on';
  var _selector = '.' + _isgs + 'parallax:(.' + _mounted + ')';
  var _hidden = _isgs + 'hidden';
  var _visible = _isgs + 'visible';
  var _start = 'transitionstart';
  var _end = 'transitionend';
  var _isTransitioning = _isgs + 'transitioning';
  var _isEnd = _isgs + 'transitionend';

  $.gridstack = $.gridstack || {};

  /**
   * GridStack front-end public methods.
   *
   * @namespace
   */
  $.gridstack.parallax = $.extend({}, $.gridstack, {
    bio: null,
    count: 0,
    items: [],
    options: {},
    animSelector: '.b-gs:not(.is-b-loading)',

    onObserving: function (entry, visible) {
      var me = this;
      var el = entry.target || entry;

      // Element enters the visible area of its parent element.
      if (entry.isIntersecting) {
        $.removeClass(el, _hidden);
        $.addClass(el, _visible);

        // Bio is only executed once, destroy and reinit to keep observing.
        if (_hit === (me.count - 1)) {
          _hit = 0;
          me.bio.disconnect(true);
          me.bio.reinit();
        }

        _hit++;
      }
      else {
        $.addClass(el, _hidden);
        $.removeClass(el, _visible + ' ' + _isEnd);
      }
    }

  });

  /**
   * GridStack utility functions.
   *
   * @param {HTMLElement} elm
   *   The .is-gs-parallax container.
   */
  function doParallax(elm) {
    var me = $.gridstack.parallax;

    if ($.hasClass(elm, _isgs + 'parallax-fs')) {
      $.addClass(_doc.body, _isgs + 'parallax-fs-body');
    }

    me.options = {
      observing: me.onObserving.bind(me),
      selector: '.box__content'
    };

    me.items = $.findAll(elm, me.options.selector);
    me.count = me.items.length;

    if (me.count === 0) {
      return;
    }

    var onTransitionStart = function (e) {
      var el = e.target;
      var cn = $.closest(el, me.options.selector) || el;

      $.addClass(cn, _isTransitioning);
    };

    var onTransitionEnd = function (e) {
      var el = e.target;
      var cn = $.closest(el, me.options.selector) || el;

      $.removeClass(cn, _isTransitioning);
      $.addClass(cn, _isEnd);

      $.off(cn, _end, onTransitionEnd);
      $.off(cn, _start, onTransitionStart);
    };

    $.each(me.items, function (item) {
      var bg = $.find(item, '.b-gs');

      if ($.isElm(bg)) {
        bg.style.minHeight = (item.offsetHeight + 120) + 'px';

        $.on(item, _start, me.animSelector, Drupal.debounce(onTransitionStart, 100));
        $.on(item, _end, me.animSelector, Drupal.debounce(onTransitionEnd, 100));
      }
    });

    me.bio = new Bio(me.options);

    $.addClass(elm, _mounted);
  }

  /**
   * Attaches behavior to HTML element identified by .is-gs-parallax.
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.gridStackParallax = {
    attach: function (context) {
      $.once(doParallax, _idOnce, _selector, context);
    },
    detach: function (context, settings, trigger) {
      if (trigger === 'unload') {
        $.once.removeSafely(_idOnce, _selector, context);
      }

    }
  };

})(dBlazy, Drupal, Bio, this.document);
