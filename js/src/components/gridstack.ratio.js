/**
 * @file
 * Provides utilities for aspect ratio fixes.
 *
 * Blazy formatter Aspect ratio doesn't work out well with gapless grid.
 * _The reason: Aspect ratio is based on image dimensions. While Gridstack
 * wants images to fill in the entire box to look gapless ignoring the actual
 * dimensions. Layout design compactness is more important than image sizes.
 *
 * The warning was provided at TROUBLESHOOTING, however sometimes blazy contents
 * are also embedded outside gridstacks and on the contrary might need aspect
 * ratio to work correctly. To allow such re-usability for a blazy/ slick block,
 * hence we only concern about those embedded inside gridstack.
 * We'll provide own solution to respect the gapless grid instead.
 *
 * This file is run before blazy.load.js so that blazy doesn't have to work.
 */

(function ($, Drupal) {

  'use strict';

  var _id = 'gridstack';
  var _nick = 'gs';
  var _idOnce = _nick + '-ratio';
  var _isgs = 'is-' + _nick;
  var _mounted = _isgs + '-ratio';
  var _selector = '.' + _id + '--js:not(.' + _mounted + ')';
  var _isContentless = _isgs + '-contentless';

  $.gridstack = $.gridstack || {};

  /**
   * Attaches behavior to HTML element identified by .gridstack--js.
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.gridStackRatio = {
    attach: function (context) {
      var fixRatio = function (el) {
        var skip = $.isElm($.closest(el, '.grid'))
        || $.isElm($.closest(el, '.' + _isContentless))
        || $.hasClass(el, _isContentless)
        || $.isElm($.closest(el, '.slide'))
        || $.hasClass(el.parentNode, _id);

        if (skip) {
          return;
        }

        $.addClass(el, 'b-noratio');
        el.className = el.className.replace(/media--ratio--(\d+)/g, '');
        $.removeClass(el, 'media--ratio media--ratio--fluid');
        $.removeAttr(el, 'data-b-ratio data-ratio');
        el.style.paddingBottom = '';

        var lb = $.closest(el, '.block-layout-builder');
        if (lb) {
          $.addClass(lb, 'box__blazy');
        }
      };

      var checkRatio = function (el) {
        var items = $.findAll(el, '.media--ratio, .b-bg');
        if (items.length) {
          $.each(items, fixRatio, el);
        }
        $.addClass(el, _mounted);
      };

      $.once(checkRatio, _idOnce, _selector, context);
    },
    detach: function (context, settings, trigger) {
      if (trigger === 'unload') {
        $.once.removeSafely(_idOnce, _selector, context);
      }
    }
  };

})(dBlazy, Drupal);
