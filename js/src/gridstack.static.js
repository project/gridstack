/**
 * @file
 * Provides basic GridStack front-end utilities without drag and drop.
 */

(function (factory) {

  'use strict';

  var _win = window;

  // Browser globals (root is window).
  factory(_win.dBlazy, _win.GridStyle, _win);

})(function ($, GridStyle, scope) {

  'use strict';

  /**
   * Private variables.
   */
  var _prefix = 'grid-stack-instance-';

  /**
   * Construct GridStatic from the given element and options.
   *
   * @param {HTMLElement} el
   *   The GridStatic HTML element.
   * @param {object} opts
   *   The GridStatic options.
   *
   * @return {GridStatic}
   *   The GridStatic instance.
   *
   * @namespace
   */
  var GridStatic = function (el, opts) {
    var me = this;

    me.destroyed = true;

    if (me.destroyed) {
      me.destroyed = false;

      me._init(el, opts);
    }

    me._updateHeightsOnResize = $.throttle(function () {
      me.cellHeight(me.cellWidth(), false);
    }, 100);

    /**
     * Called when we are being resized.
     */
    me.onResizeHandler = function () {
      me.windowWidth = $.windowWidth();

      me.prepareNodes();

      if (me.opts.cellHeight === 'auto') {
        me._updateHeightsOnResize();
      }

      var maxHeight = me.getMaxHeight();
      me._updateStyles(maxHeight + 10);
    };

    // Must be similar to original GridStack, 100 for consistent usage.
    $.on(scope, 'resize orientationchange', Drupal.debounce(me.onResizeHandler, 100));
    me.onResizeHandler();

    return me;
  };

  // Cache our prototype.
  var _proto = GridStatic.prototype;
  _proto.constructor = GridStatic;

  // Prepare prototype.
  _proto.defaults = function () {
    return {
      _class: _prefix + (Math.random() * 10000).toFixed(0),
      dataset: [],
      column: 12,
      minRow: 0,
      maxRow: 0,
      itemClass: 'gridstack__box',
      placeholderClass: 'grid-stack-placeholder',
      cellHeight: 'auto',
      cellHeightThrottle: 100,
      margin: 20,
      auto: true,
      minW: 768, // @todo recheck API.
      oneColumnSize: 768,
      float: false,
      rtl: 'auto',
      staticGrid: true,
      animate: false,
      disableDrag: true,
      disableResize: true,
      // @todo remove at v4?
      verticalMarginUnit: 'px', // @todo recheck API.
      marginUnit: 'px',
      cellHeightUnit: 'px'
    };
  };

  // Dummy BC for cross-compatibility with original library methods.
  _proto.update = function (el, x, y, w, h) {};

  _proto.prepareNodes = function (nodes) {
    var me = this;
    var opts = me.opts;

    nodes = nodes || opts.dataset;

    if (nodes) {
      me.nodes = [];
      $.each(nodes, function (item) {
        var node = {};
        $.each(['x', 'y', 'w', 'h'], function (pos, i) {
          node[pos] = item[i];
        });

        me.nodes.push(node);
      });
    }

    // Only query DOM if no dataset are passed above.
    if ($.isEmpty(me.nodes)) {
      me.items = $.findAll(me.el, '.' + opts.itemClass + ':not(.' + opts.placeholderClass + ')');
      $.each(me.items, function (item, i) {
        var node = {};
        $.each(['x', 'y', 'w', 'h'], function (pos, j) {
          node[pos] = $.attr(item, 'gs-' + pos);
        });
        me.nodes.push(node);
      });
    }

    if (opts.cellHeight === 'auto') {
      // Make the cell square initially.
      me.cellHeight(me.cellWidth(), true);
    }
    else {
      me.cellHeight(opts.cellHeight, true);
    }

    var margin = opts.margin;
    me.margin(margin, true);
  };

  _proto.getRow = function () {
    return this.nodes.reduce(function (memo, n) { return Math.max(memo, n.y + n.h); }, 0);
  };

  _proto.getMaxHeight = function () {
    var me = this;
    var maxHeight = 0;

    $.each(me.nodes, function (n) {
      maxHeight = Math.max(maxHeight, n.y + n.h);
    });

    return maxHeight;
  };

  _proto.margin = function (val, noUpdate) {
    var me = this;
    var opts = me.opts;
    var margin = opts.margin;
    if ($.isUnd(val)) {
      return margin;
    }

    var heightData = GridStyle.parseHeight(val);

    if (opts.marginUnit === heightData.unit && opts.maxRow === heightData.height) {
      return;
    }

    opts.marginUnit = heightData.unit;

    opts.margin = heightData.height;

    if (!noUpdate) {
      me._updateStyles();
    }
  };

  _proto.cellHeight = function (val, noUpdate) {
    var me = this;
    var opts = me.opts;

    // Getter - returns the opts stored height else compute it...
    if ($.isUnd(val)) {
      if (opts.cellHeight && opts.cellHeight !== 'auto') {
        return opts.cellHeight;
      }

      // Compute the height taking margin into account (each row has margin
      // other than last one).
      var o = $.find(me.el, '.' + opts.itemClass + ':first-child');

      var height = $.attr(o, 'gs-h');
      var margin = opts.margin;

      return Math.round((o.offsetHeight - (height - 1) * margin) / height);
    }

    // Setter - updates the cellHeight value if they changed.
    var heightData = GridStyle.parseHeight(val);
    // This allows correct row calc for multiple instances.
    // @todo recheck if any issue other than resize.
    if (opts.cellHeightUnit === heightData.unit && opts.cellHeight === heightData.height) {
      return;
    }
    opts.cellHeightUnit = heightData.unit;
    opts.cellHeight = heightData.height;

    if (!noUpdate) {
      me._updateStyles();
    }
  };

  _proto.cellWidth = function () {
    var me = this;

    return Math.round(me.el.offsetWidth / me.opts.column);
  };

  _proto._initStyles = function () {
    var me = this;
    var el = me.el;
    var opts = {id: me._stylesId, prefix: _prefix};

    var elm = 'length' in el ? el[0] : el;
    var gridStyle = GridStyle.init(opts, elm);

    me._stylesId = gridStyle.id;
    me._styles = gridStyle.styles;
  };

  _proto._updateStyles = function (maxHeight) {
    var me = this;
    var opts = me.opts;
    var emptyStyles = $.isNull(me._styles) || $.isUnd(me._styles);

    if (emptyStyles) {
      me._initStyles();
    }

    if (emptyStyles) {
      return;
    }

    var prefix = '.' + opts._class + ' .' + opts.itemClass;
    var getHeight;

    if ($.isUnd(maxHeight)) {
      maxHeight = me._styles._max;
    }

    me._initStyles();
    me._updateContainerHeight();

    if (!opts.cellHeight) {
      // The rest will be handled by CSS.
      return;
    }

    if (me._styles === null || (me._styles._max !== 0 && maxHeight <= me._styles._max)) {
      // Keep me._styles._max increasing.
      return;
    }

    var margin = opts.margin;
    if (!margin || opts.cellHeightUnit === opts.marginUnit) {
      getHeight = function (nbRows, nbMargins) {
        return (opts.cellHeight * nbRows + margin * nbMargins) +
          opts.cellHeightUnit;
      };
    }
    else {
      getHeight = function (nbRows, nbMargins) {
        if (!nbRows || !nbMargins) {
          return (opts.cellHeight * nbRows + margin * nbMargins) +
            opts.cellHeightUnit;
        }

        return 'calc(' + ((opts.cellHeight * nbRows) + opts.cellHeightUnit) + ' + ' +
          ((margin * nbMargins) + opts.marginUnit) + ')';
      };
    }

    if (me._styles._max === 0) {
      GridStyle.insertCSSRule(me._styles, prefix, 'min-height: ' + getHeight(1, 0) + ';', 0);
    }

    if (maxHeight > me._styles._max) {
      for (var i = me._styles._max; i < maxHeight; ++i) {
        GridStyle.insertCSSRule(me._styles,
          prefix + '[gs-h="' + (i + 1) + '"]',
          'height: ' + getHeight(i + 1, i) + ';',
          i
        );
        GridStyle.insertCSSRule(me._styles,
          prefix + '[gs-min-h="' + (i + 1) + '"]',
          'min-height: ' + getHeight(i + 1, i) + ';',
          i
        );
        GridStyle.insertCSSRule(me._styles,
          prefix + '[gs-max-h="' + (i + 1) + '"]',
          'max-height: ' + getHeight(i + 1, i) + ';',
          i
        );
        GridStyle.insertCSSRule(me._styles,
          prefix + '[gs-y="' + i + '"]',
          'top: ' + getHeight(i, i) + ';',
          i
        );
      }
      me._styles._max = maxHeight;
    }
  };

  _proto._updateContainerHeight = function () {
    var me = this;
    var row = me.getRow();
    var el = me.el;
    var opts = me.opts;

    if (row < opts.minRow) {
      row = opts.minRow;
    }

    // Check for css min height. Each row is cellHeight + verticalMargin,
    // until last one which has no margin below.
    var cssMinHeight = parseInt(me.style.getPropertyValue('min-height'), 0);
    if (cssMinHeight > 0) {
      var margin = opts.margin;
      // var minRow = Math.round((cssMinHeight + margin) / (me.cellHeight() + margin));
      var minRow = Math.round((cssMinHeight + margin) / (me.cellHeight() + margin));
      if (row < minRow) {
        row = minRow;
      }
    }

    $.attr(el, 'data-gs-current-row', row);

    if (!opts.cellHeight) {
      return;
    }

    var _margin = opts.margin;

    if (!_margin) {
      el.style.height = (row * (opts.cellHeight)) + opts.cellHeightUnit;
    }
    else if (opts.cellHeightUnit === opts.marginUnit) {
      el.style.height = (row * (opts.cellHeight + _margin) - _margin) + opts.cellHeightUnit;
    }
    else {
      el.style.height = 'calc(' + ((row * (opts.cellHeight)) + opts.cellHeightUnit) + ' + ' + ((row * (_margin - 1)) + opts.marginUnit) + ')';
    }
  };

  /**
   * Set/get number of columns in the grid.
   *
   * Will attempt to update existing widgets to conform to new number of
   * columns. Requires `gridstack-extra.css` or `gridstack-extra.min.css`
   * for [2-11], else you will need to generate correct CSS
   * (see https://github.com/gridstack/gridstack.js#change-grid-columns).
   *
   * @param {int} column
   *  Integer > 0 (default 12).
   *
   * @return {int|mixed}
   *   The column if as a getter, else setting its value.
   */
  _proto.column = function (column) {
    var me = this;
    var el = me.el;
    var opts = me.opts;

    // Getter - returns the opts stored mode.
    if ($.isUnd(column)) {
      return opts.column;
    }

    // Setter.
    if (opts.column === column) {
      return;
    }

    var oldColumn = opts.column;

    // If we go into 1 column mode (which happens if we're sized less than
    // oneColumnSize unless disableOneColumnMode is on)
    // then remember the original columns so we can restore.
    if (column === 1) {
      me._prevColumn = oldColumn;
    }
    else {
      delete me._prevColumn;
    }

    $.removeClass(el, 'grid-stack-' + oldColumn);
    $.addClass(el, 'grid-stack-' + column);

    opts.column = column;
  };

  _proto._init = function (el, opts) {
    var me = this;

    me.style = window.getComputedStyle(el);
    me.items = [];
    me.nodes = [];

    opts = GridStyle.defaults({}, opts, me.defaults());
    opts.column = parseInt(el.dataset.gsColumn, 0) || opts.column;
    opts.maxRow = parseInt(el.dataset.gsMaxRow, 0) || opts.maxRow;

    if (opts.row) {
      opts.minRow = opts.maxRow = opts.row;
      delete opts.row;
    }

    if (opts.rtl === 'auto') {
      opts.rtl = me.style.getPropertyValue('direction') === 'rtl';
    }

    if (opts.rtl) {
      $.addClass(el, 'grid-stack-rtl');
    }

    $.addClass(el, opts._class);
    me.el = el;
    me.opts = opts;

    me._initStyles();
  };

  _proto.destroy = function () {
    var me = this;
    var el = me.el;

    $.off(scope, 'resize orientationchange', me.onResizeHandler);

    $.removeClass(el, me.opts._class);
    delete el.gridstack;

    GridStyle.removeStylesheet(me._stylesId);
    me.destroyed = true;
  };

  scope.GridStatic = GridStatic;

  /**
   * Initializing the HTML element into a grid.
   *
   * Will return the grid. Calling it again will simply return the existing
   * instance (ignore any passed options).
   *
   * @param {object} opts
   *   The GridStatic options.
   * @param {HTMLElement} el
   *   The GridStatic HTML element.
   *
   * @return {function}
   *   The GridStatic instance.
   */
  GridStatic.init = function (opts, el) {
    if (!el.gridstack) {
      el.gridstack = new GridStatic(el, opts);
    }
    return el.gridstack;
  };

  return scope.GridStatic;

});
