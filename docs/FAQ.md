***
***
# <a name="faq"> </a>FAQ

## GRIDSTACK IN A NUTSHELL
Gridstack is a layout factory/ creator. Similar to how you create layouts by
code, only with Gridstack, you create layouts with a drag and drop.

It is capable of building the most complex layouts you can imagine visually
right in the browser. Call it designing layouts on the browser.

Even if you love coding everything by hand, you will soon find Gridstack handy
for various reasons:

* configurability,
* exportability,
* easy integration with various layout builders,
* easy modification to suit your heart contents,
* can be colorized and stylized easily via Layout Builder,
* etc.

### Gridstack layouts come in three flavors:

1. **The css-driven layouts** aka one-dimensional or linear layouts  
   Integrates with Bootstrap or Foundation CSS frameworks.
   Hence Gridstack is useless till you install one of the layout builders which
   can make use of Gridstack layouts:

   Display Suite, Layout Builder, Widget, etc.

   No Gridstack CSS and JS are loaded. You must have a theme or module which
   load the grid CSS framework for you.

2. **The js-driven layouts** aka two-dimensional layouts  
   Gridstack has formatters and Views style plugins to build magazine layouts.

3. **Native browser CSS Grid layouts** aka two-dimensional layouts  
   Gridstack has formatters and Views style plugins to build native CSS Grid
   via option **Use native CSS Grid**.

All layout flavors are usable for Display Suite, Layout Builder, etc.
Region or wrapper attributes, including their HTML tags are configurable via
Layout builder for all flavors. All these 3 flavors may have variants as
configured via Layout Builder pages. Variants are not registered as layouts.
Variants are just derivatives or subsets of the original registered layouts.

More layout engines or flavors are provided via a sub-module
[Outlayer](https://drupal.org/project/outlayer)


## REGISTERING SKINS
1. Copy `\Drupal\gridstack\Plugin\gridstack\Skin\GridStackSkin` into your module
  `/src/Plugin/gridstack` directory.
2. Adjust everything accordingly: rename the file, change GridStackSkin ID and
  label, change class name and its namespace, define skin name, and its CSS and
  JS assets.

The `GridStackSkin` object has 1 supported method: `::setSkins()`.

The declared skins will be available for custom coded, or UI selections.
Be sure to clear cache since skins are permanently cached!

## REGISTERING LAYOUT ENGINES
Adding custom layout engines like `Material`, `Bulma`, etc. is supported.

1. Copy any similar `\Drupal\gridstack\Plugin\gridstack\engine\` file into your
   module `/src/Plugin/gridstack/engine` directory as a sample. Check out the
   rest.
2. Adjust everything accordingly: rename the file, change plugin ID and
   label, change class name and its namespace, define relevant methods, and its
   CSS and JS assets. Use `GridBase`, or  `GridStackBase` as a base class for
   one-dimensional or two-dimensional layouts respectively. Know their CSS
   classes and rules.
3. Select your layout engine at GridStack UI.   
4. Check out [Outlayer](https://drupal.org/project/outlayer) for samples.

## CURRENT DEVELOPMENT STATUS
A full release should be reasonable after proper feedback from the community,
some code cleanup, and optimization where needed. Patches are very much welcome.

Alpha, Beta, DEV releases are for developers only. Beware of possible breakage.

However if it is broken, unless an update is provided, running `drush cr` during
DEV releases should fix most issues as we add new services, or change things.
If you don't drush, before any module update, always open:

[Admin Performance](/admin/config/development/performance)

And so you are ready to hit **Clear all caches** if any issue.
Only at worst case, know how to run
https://www.drupal.org/project/registry_rebuild safely.
