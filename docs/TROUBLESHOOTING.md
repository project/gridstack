
***
***
## <a name="troubleshooting"></a>TROUBLESHOOTING
* `/admin/config/development/performance`  
  Be sure to clear cache if any display issue, like skins not displayed, layouts
  broken, new layouts not displayed, etc.
  If still an issue, press F12 at any browser. Find Console and fix any JS
  errors. If unsure, create an issue, and be sure to include the screenshot in
  your issue.
* Be sure the amount of Views results are matching the amount of the grid boxes
  **minus** stamp. The same rule applies to either GridStack or custom-defined
  grids.  
  *Solutions*: This is easily fixed via Views UI under **Pager**.
* Follow the natural order keyed by index, or save the form anytime, if trouble.
* Clear cache when updating cached Gridstack otherwise no changes are visible
  immediately. You can also disable the Gridstack cache during work.
* Save the optionset first if things are messed up at admin UI due to incorrect
  orders. Once saved, orders will be re-indexed, and re-edit.  
* If Blazy formatter Aspect ratio option breaks the grid, simply leave it empty.
  **The reason**: Aspect ratio is based on image dimensions. While Gridstack
  wants images to fill in the entire box to look gapless ignoring the actual
  dimensions. Layout design compactness is more important than image dimensions.
* I don't see my new regions at core Layout Builder.   
  *Solutions*: Be sure to clear cache.
* I cannot save my Layout Builder with a message `You have unsaved changes`?
  + It appears to happen whenever a `ksm()` is put at those Layout Builder pages
    for development purposes. Deleting the layout solves the problem. Be sure to
    not put any `ksm()` in there to avoid issues. Use `Devel` task menu, or the
    front-end pages instead.  
  + Based on core issues, it was triggered when having forms, such as Search
    form inside a layout.
* My contents inside boxes with native and js-driven layouts need to have a
  fixed height matching its box. This is a rare case as long as you have images.  
  *Solutions*: add a class `is-gs-fh` to have a fixed height to the troubled
  elements. Currently only few are fixed: `.b-bg` and `.media--ratio` for
  Blazy CSS background and Aspect ratio respectively. Alternatively use CSS
  mediaqueries with min-height accordingly. Also available as options at Layout
  Builder pages.
* My layout icon is not updated at Layout Builder. You don't always change icons
  at productions, no worries.  
  *Solutions*: Clearing Drupal and browser cache should do.
* Clearing cache is required whenever adding new layouts/ optionsets to register
  them for Layout Discovery/ Layout Builder. Not needed for layout variants as
  variants are not registered as layouts, just handy overrides.  
* Always test out and verify against core Bartik for all blazy ecosystem, in
  case your theme has JS errors.
* I cannot edit certain breakpoints identified by cursor `not allowed`.  
  *Solutions*: Input relevant breakpoint value for the `Width` option at the
  bottom of layout form to make it editable. Empty means disabled (non-editable)
  relevant such as for Bootstrap 3/ Foundation layouts since both have
  no XS or XL breakpoints.  
* Do not simply delete a layout once registered, else Layout Builder complains
  losing it _only_ if it is being used by Layout Builder.  
  *Solutions*: Just re-create one with the same layout ID.  
* Avoid **animation** options at large regions (containers) due to high CPU
  consumption. They are best for small regions/ containers.  

## No longer issues
* Check **Use CSS background** if images are not filling out the grids,
  otherwise you may have to define a min-height to each IMG, configurable via
  Layout Builder pages.  
  **Update**: The latest supports more medium, IMG, IFRAME, PICTURE than BG.
  Use blazy-related formatters for the best results with supported solutions.


## v0.2.5 below, skip
Below are valid for v0.2.5 below, for the sake of documentation:

* At admin UI, some grid/box may be accidentally hidden at smaller width. If
  that happens, try giving column and width to large value first to bring
  them back into the viewport. And when they are composed, re-adjust them.
  Or hit Clear, Load Grid, Save & Continue buttons to do the reset.
  At any rate saving the form as is with the mess can have them in place till
  further refinement.
* Use the resizable handlers to snap to grid if box dragging doesn't snap.
  Be gentle with it to avoid abrupt mess.
* If trouble at frontend till Gridstack library is decoupled from jQuery UI or
  at least till jQuery related issues resolved, for its static grid, check the
  option **Load jQuery UI**. If you are a JS guy, you know where the problem is.
  A temporary hacky solution is also available:  
  `/admin/structure/gridstack/ui`


## KNOWN ISSUES
* When `devel` module is installed, the GridStack variant page is broken at:
  `/admin/structure/gridstack/variant`. `Solution`: temporary uninstall `devel`
  till further fixes. Alternatively, use `Layout Builder` to add/ remove new
  variants.
* Having the exact amount of items to match the optionset grids is a must.
* If you have more items than designated optionset grids, the surplus will not
  be displayed as otherwise broken grid anyway.
* If you have less items, the GridStack will not auto-fix it for you.
  Simply providing the exact amount of items should make it gapless grid again.
  Use **Views pager** or **Upload image** option to match the grid blueprint.
* Nested grids are only supported by static grid Bootstrap/ Foundation, not JS.
* When editing an optionset at XS breakpoint, the grid shows 1x1 but looks like
  12x1. Ignore preview limitations. Trust the numbers in dark.
* Grids may have weird aspect ratio with margins.
   **Solutions**:

   * `Vertical margin` = 0
   * `No horizontal margin` enabled
   * Adjust `Cell height`
   * If spaces are needed, use CSS `padding` rules at inner DIVITIES instead.
* I want the correct Aspect ratio. Solutions: see above this line, or check `Use
  native CSS Grid` option for modern solutions.
