# Schema for the configuration files of the GridStack module.
gridstack.settings:
  type: config_object
  label: 'GridStack settings'
  mapping:
    framework:
      type: string
      label: 'Grid framework'
    fw_classes:
      type: string
      label: 'Framework classes'
    no_classes:
      type: boolean
      label: 'No classes'
    library:
      type: string
      label: 'Grid library'
    debug:
      type: boolean
      label: Debug
    optimized:
      type: boolean
      label: Optimized
    palettes:
      type: string
      label: 'Color palettes'
    palettes_pos:
      type: string
      label: 'Palettes position'
    excludes:
      type: string
      label: 'Exclude optionsets'
    gridstatic:
      type: boolean
      label: 'Use minimal GridStack'
    helpless:
      type: boolean
      label: 'Remove Help section'
    skinless:
      type: boolean
      label: 'Remove Skin option'
    animationless:
      type: boolean
      label: 'Remove Animation option'
    editor_pos:
      type: string
      label: 'Layout editor position'

gridstack_json:
  type: mapping
  label: JSON
  mapping:
    settings:
      type: string
      label: Settings
    breakpoints:
      type: string
      label: Breakpoints

gridstack_breakpoint:
  type: mapping
  mapping:
    breakpoint:
      type: string
      label: Breakpoint
    column:
      type: integer
      label: Column
    width:
      type: integer
      label: Width
    grids:
      type: string
      label: Grids
    nested:
      type: string
      label: 'Nested grids'

gridstack_breakpoints:
  type: mapping
  label: Breakpoints
  mapping:
    xs:
      type: gridstack_breakpoint
      label: XS
    sm:
      type: gridstack_breakpoint
      label: SM
    md:
      type: gridstack_breakpoint
      label: MD
    lg:
      type: gridstack_breakpoint
      label: LG
    xl:
      type: gridstack_breakpoint
      label: XL

# GridStack JS settings managed by UI.
gridstack_optionset:
  type: mapping
  label: 'Options'
  mapping:
    breakpoints:
      type: gridstack_breakpoints
    icon:
      type: string
      label: Icon
    type:
      type: string
      label: Type
    use_framework:
      type: boolean
      label: 'Use static Grid framework'
    settings:
      type: mapping
      label: Settings
      mapping:
        auto:
          type: boolean
          label: Auto
        cellHeight:
          type: integer
          label: 'Cell height'
        float:
          type: boolean
          label: Float
        minW:
          type: integer
          label: 'Min width'
        rtl:
          type: boolean
          label: RTL
        margin:
          type: integer
          label: 'Vertical margin'
        noMargin:
          type: boolean
          label: 'No horizontal margin'
        # Introduced at v0.5.3.
        column:
          type: integer
          label: 'Amount of columns'
        maxRow:
          type: integer
          label: 'Maximum rows'

# Individual GridStack JS settings managed by UI.
gridstack.optionset.*:
  type: config_entity
  label: 'GridStack optionset'
  mapping:
    id:
      type: string
      label: ID
    name:
      type: string
      label: Name
    weight:
      type: integer
      label: Weight
    label:
      type: label
      label: Label
    uuid:
      type: string
      label: UUID
    description:
      type: string
      label: Description
    json:
      type: gridstack_json
    options:
      type: gridstack_optionset
      label: Options

# Individual GridStack variant settings managed by UI.
gridstack.variant.*:
  type: config_entity
  label: 'GridStack optionset'
  mapping:
    id:
      type: string
      label: ID
    name:
      type: string
      label: Name
    weight:
      type: integer
      label: Weight
    label:
      type: label
      label: Label
    uuid:
      type: string
      label: UUID
    description:
      type: string
      label: Description
    source:
      type: string
      label: Source
    options:
      type: mapping
      label: Options
      mapping:
        breakpoints:
          type: gridstack_breakpoints
        icon:
          type: string
          label: Icon
        use_framework:
          type: boolean
          label: 'Use static Grid framework'
        settings:
          type: mapping
          label: Settings
          mapping: {}

# HTML content/layout-related settings managed by GridStack plugins.
gridstack_vanilla:
  type: blazy_base
  label: 'GridStack display format vanilla settings'
  mapping: {}

gridstack_base:
  type: blazy_base
  label: 'GridStack display format base settings'
  mapping:
    caption:
      type: sequence
      label: Captions
      sequence:
        type: string
        label: 'Caption field'
    stamp:
      type: sequence
      label: Stamps
      sequence:
        type: string

field.formatter.settings.gridstack_image:
  type: gridstack_vanilla
  label: 'GridStack Image display format settings'

field.formatter.settings.gridstack_file:
  type: gridstack_vanilla
  label: 'GridStack File display format settings'

field.formatter.settings.gridstack_media:
  type: gridstack_vanilla
  label: 'GridStack Media display format settings'

field.formatter.settings.gridstack_paragraphs:
  type: gridstack_vanilla
  label: 'GridStack Paragraphs display format settings'
