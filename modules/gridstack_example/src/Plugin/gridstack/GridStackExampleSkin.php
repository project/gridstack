<?php

namespace Drupal\gridstack_example\Plugin\gridstack;

use Drupal\gridstack\Skin\GridStackSkinPluginBase;

/**
 * Provides gridstack example skins.
 *
 * @GridStackSkin(
 *   id = "gridstack_example_skin",
 *   label = @Translation("GridStack skin")
 * )
 */
class GridStackExampleSkin extends GridStackSkinPluginBase {

  /**
   * Sets the gridstack skins.
   */
  protected function setSkins() {
    // If you copy this file, be sure to add base_path() before any asset path
    // (css or js) as otherwise failing to load the assets. Your module can
    // register paths pointing to a theme.
    $path = $this->getPath('module', 'gridstack_example');

    return [
      'zoe' => [
        'name' => 'X: Zoe',
        'provider' => 'gridstack_example',
        'css' => [
          'theme' => [
            $path . '/css/gridstack.theme--zoe.css' => [],
          ],
        ],
      ],
    ];
  }

}
