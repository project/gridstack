<?php

/**
 * @file
 * Hooks and preprocess functions for the GridStack UI module.
 */

use Drupal\Core\Template\Attribute;

/**
 * Prepares variables for gridstack-ui-admin.html.twig templates.
 */
function template_preprocess_gridstack_ui_admin(&$variables) {
  if (!function_exists('template_preprocess_gridstack')) {
    \Drupal::moduleHandler()->loadInclude('gridstack', 'inc', 'templates/gridstack.theme');
  }

  template_preprocess_gridstack($variables);
  $element = $variables['element'];

  foreach (['content', 'preview', 'wrapper'] as $key) {
    $variables[$key . '_attributes'] = $element["#$key" . '_attributes'] ?? [];
  }

  $settings = &$variables['settings'];
  $content_attributes = &$variables['content_attributes'];
  $preview_attributes = &$variables['preview_attributes'];
  $wrapper_attributes = &$variables['wrapper_attributes'];

  $settings['id'] = 'gridstack-' . $settings['breakpoint'];
  $content_attributes['id'] = $settings['id'];

  $wrapper_attributes['id'] = 'gridstack-wrapper-' . $settings['breakpoint'];
  $wrapper_attributes['data-breakpoint'] = $settings['breakpoint'];

  $variables['content_attributes'] = new Attribute($content_attributes);
  $variables['preview_attributes'] = new Attribute($preview_attributes);
  $variables['wrapper_attributes'] = new Attribute($wrapper_attributes);

  $variables['main_buttons'] = [];

  $settings['is_main_preview'] = FALSE;
  if ($settings['breakpoint'] == $settings['icon_breakpoint']) {
    $settings['is_main_preview'] = TRUE;

    $variables['main_buttons'] = [
      'save' => [
        'attributes' => new Attribute([
          'class' => ['button btn btn--gridstack btn--main btn--primary btn--save'],
          'data-message' => 'save',
          'data-breakpoint' => $settings['breakpoint'],
          'data-icon' => $settings['breakpoint'],
          'data-storage' => $settings['storage'],
          'type' => 'button',
        ]),
        'text' => t('Update icon'),
      ],
      'add'  => [
        'attributes' => new Attribute([
          'class' => ['button btn btn--gridstack btn--main btn--add'],
          'data-message' => 'add',
          'data-breakpoint' => $settings['breakpoint'],
          'type' => 'button',
        ]),
        'text' => t('Add grid'),
      ],
    ];
  }
}
