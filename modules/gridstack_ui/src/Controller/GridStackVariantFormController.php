<?php

namespace Drupal\gridstack_ui\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Controller\ControllerBase;
use Drupal\gridstack\Entity\GridStackVariant;
use Drupal\gridstack\GridStackDefault;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides controller to load GridStackVariantForm.
 */
class GridStackVariantFormController extends ControllerBase {

  /**
   * The request service.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * The gridstack manager.
   *
   * @var \Drupal\gridstack\GridStackManagerInterface
   */
  protected $manager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);

    $instance->request = $container->get('request_stack')->getCurrentRequest();
    $instance->manager = $container->get('gridstack.manager');
    return $instance;
  }

  /**
   * Adds a gridstack variant.
   */
  public function add($gridstack, $gid, $gridstack_variant) {
    $name = str_replace(['-'], '_', $gridstack_variant);
    $entity = GridStackVariant::load($name);

    if (empty($entity)) {
      $values = $gridstack->toArray();
      $options = $values['options'];
      $options['icon'] = 'public://gridstack/' . $name . '.png';

      $entity = GridStackVariant::create([
        'name'    => $name,
        'label'   => str_replace(['-', '_'], ' ', $gridstack_variant),
        'options' => $options,
        'source'  => $gridstack->id(),
      ]);

      $entity->set('id', $name);
      $entity->save();
    }
    return $this->edit($gridstack, $gid, $entity);
  }

  /**
   * Duplicates a gridstack variant.
   */
  public function duplicate($gridstack, $gid, $gridstack_variant) {
    $request = $this->request;

    // Satisfy phpstan.
    if (!isset($request->query)) {
      return NULL;
    }

    $name = $request->query->get('dup', NULL);
    $label = $request->query->get('label', NULL);
    if (empty($name)) {
      return NULL;
    }

    $values = $gridstack_variant->toArray();
    $options = $values['options'];
    $options['icon'] = 'public://gridstack/' . $name . '.png';

    $entity = GridStackVariant::load($name);
    if (empty($entity)) {
      $entity = GridStackVariant::create([
        'name'    => $name,
        'label'   => $label ?: str_replace(['-', '_'], ' ', $name),
        'options' => $options,
        'source'  => $gridstack->id(),
      ]);

      $entity->set('id', $name);
      $entity->save();
    }

    return $this->edit($gridstack, $gid, $entity);
  }

  /**
   * Deletes a gridstack variant.
   */
  public function delete($gridstack, $gid, $gridstack_variant) {
    $this->messenger()->addMessage($this->t('Variant %label was deleted', ['%label' => $gridstack_variant->label()]));
    $gridstack_variant->delete();
  }

  /**
   * Returns the variant editor.
   */
  public function edit($gridstack, $gid, $gridstack_variant) {
    $request = $this->request;
    $response = new AjaxResponse();

    // Satisfy phpstan.
    if (!isset($request->query)) {
      return $response;
    }

    $name = $request->query->get('dup', NULL);
    $label = $request->query->get('label', NULL);

    if ($name) {
      $values = $gridstack_variant->toArray();
      $options = $values['options'];
      $options['icon'] = 'public://gridstack/' . $name . '.png';

      $entity = GridStackVariant::load($name);
      if (empty($entity)) {
        $entity = GridStackVariant::create([
          'name'    => $name,
          'label'   => $label ?: str_replace(['-', '_'], ' ', $name),
          'options' => $options,
          'source'  => $gridstack->id(),
        ]);

        $entity->set('id', $name);
        $entity->save();
      }
      $gridstack_variant = $entity;
    }

    $form = $this->entityFormBuilder()->getForm($gridstack_variant, 'edit') ?: [];

    $response->addCommand(new HtmlCommand('#' . GridStackDefault::variantWrapperId($gid), $form));
    return $response;
  }

  /**
   * Cancels layout changes.
   */
  public function cancel($gridstack, $gid) {
    $request = $this->request;
    $response = new AjaxResponse();

    // Satisfy phpstan.
    if (!isset($request->query)) {
      return $response;
    }

    $pub    = $request->query->get('pub', NULL);
    $vid    = $request->query->get('vid', NULL);
    $id     = $gridstack->id();
    $config = ['gid' => $gid, 'optionset' => $id, 'vid' => $vid, 'pub' => $pub];
    $editor = $this->manager->stylizer()->builder()->getVariantEditor($config, $gridstack);

    $response->addCommand(new ReplaceCommand('#' . GridStackDefault::variantWrapperId($gid), $editor['form']));
    return $response;
  }

  /**
   * Returns the variant selection.
   */
  public function selection($gridstack, $gid) {
    $request = $this->request;
    $response = new AjaxResponse();

    // Satisfy phpstan.
    if (!isset($request->query)) {
      return $response;
    }

    $pub = $request->query->get('pub', NULL);
    $vid = $request->query->get('vid', NULL);
    $id  = $gridstack->id();

    $args = [
      'id' => $id,
      'gid' => $gid,
      'vid' => $vid,
      'pub' => $pub,
    ];
    /* @phpstan-ignore-next-line */
    $form = $this->formBuilder()->getForm('Drupal\gridstack_ui\Form\GridStackVariantSelectionForm', $args);

    $response->addCommand(new HtmlCommand('#' . GridStackDefault::variantWrapperId($gid), $form));
    return $response;
  }

}
