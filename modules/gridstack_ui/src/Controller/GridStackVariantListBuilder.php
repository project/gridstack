<?php

namespace Drupal\gridstack_ui\Controller;

use Drupal\Core\Entity\EntityInterface;
use Drupal\gridstack\Entity\GridStackVariantInterface;

/**
 * Provides a listing of GridStack optionsets.
 */
class GridStackVariantListBuilder extends GridStackListBuilder {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'gridstack_variant_list_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header = [
      'source' => $this->t('Source'),
    ];

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $rows = parent::buildRow($entity);

    // Satisfy phpstan.
    if (!($entity instanceof GridStackVariantInterface)) {
      return $rows;
    }

    $row['source'] = ['#markup' => $entity->source()];

    return $row + $rows;
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $build[] = parent::render();
    $build[0]['description'] = [
      '#markup' => '<p>' . $this->t("Manage the GridStack layout variants. Variants are normally minor re-composition for desktop version, subset deskto layouts, based on a particular dominant layout to keep sensible grouping or sanity on layout managements. Leaving the mobile versions (XS, SM, MD) inherited from the source layout. For example, a GridStack Twain may have 12 variants, or so. Add variants via Layout Builder, not here. Use this page as a convenient way to apply changes globally to all pages. Each variant may be re-used at different pages, and their change will apply globally. To have unique variant per page, simply create memorable labels, such as: <code>Twain: About</code> to apply at About page. To edit original layouts, hit <code>Source</code> under <code>Operations</code>.") . '</p>',
    ];
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultOperations(EntityInterface $entity) {
    $operations = parent::getDefaultOperations($entity);

    // Satisfy phpstan.
    if (!($entity instanceof GridStackVariantInterface)) {
      return $operations;
    }

    if (isset($operations['duplicate'])) {
      unset($operations['duplicate']);
    }

    $operations['edit'] = [
      'title' => $this->t('Configure'),
      'url'   => $entity->toUrl('edit-form'),
    ];

    $operations['delete'] = [
      'title' => $this->t('Delete'),
      'url'   => $entity->toUrl('delete-form'),
    ];

    $access = $this->currentUser->hasPermission('administer gridstack');
    if ($access && $gridstack = $entity->getOptionset()) {
      $operations['source'] = [
        'title' => $this->t('Source'),
        'url'   => $gridstack->toUrl('edit-form'),
      ];
    }

    return $operations;
  }

}
