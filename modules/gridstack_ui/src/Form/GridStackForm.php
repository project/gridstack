<?php

namespace Drupal\gridstack_ui\Form;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\gridstack\Entity\GridStackInterface;

/**
 * Extends base form for gridstack instance configuration form.
 */
class GridStackForm extends GridStackFormBase {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    // Add layered forms.
    $form = $this->initForm($form, $form_state);
    $this->optionsForm($form, $form_state);
    $this->settingsForm($form, $form_state);
    $this->previewForm($form, $form_state);

    $form['#prefix'] = '<div id="gsapp">';
    $form['#suffix'] = '</div>';

    if (!$this->useNested) {
      $this->massageSettings($form);
    }

    return parent::form($form, $form_state);
  }

  /**
   * Returns form item description.
   */
  protected function getDescription($key) {
    $url1 = Url::fromRoute('gridstack.settings')->toString();
    $url2 = 'https://gridstackjs.com/demo/float.html';
    $url3 = 'https://gridstackjs.com/demo/rtl.html';
    $description = [
      'use_framework'  => $this->t("If you don't clone the provided Bootstrap/Foundation default, be sure to save the form first before working to set things up correctly. Check to enable static grid framework (Bootstrap/Foundation). Uncheck to use native CSS Grid, or JS layout. Must enable the support <a href=':url'>here</a> first. This requires basic comprehension on how a static grid like Bootstrap, or Foundation, works. And GridStack preview limitation. Basically on how columns and rows build up the grid system. Failing to understand it may result in broken grids (this is no issue if using GridStack JS, unchecked).<br>If checked: <ul><li>No GridStack JS/ CSS assets are loaded at front-end.</li><li>Must have a theme, or module, that loads Bootstrap/ Foundation CSS for you.</li><li>No fixed height, just auto height. Repeat, height is ignored.</li><li>Boxes are floating like normal CSS floats, no longer absolutely positioned. The previews may trick you, bear with it.</li><li>No longer a fixed-height magazine layout. Just a normal floating CSS grid layout.</li><li>This layout is only available for core <strong>Layout Builder, DS, Panelizer, or Widget</strong> modules. Leave it unchecked if not using them, else useless for GridStack alone.</li></ul>Clone an existing Default Bootstrap/ Foundation optionset to begin with. Limitation: pull, push classes are not supported, yet.", [':url' => $url1]),
      'settings'       => $this->t('This only affects GridStack JS and native CSS Grid, not Bootstrap/ Foundation. Only few are applicable for native Grid: Min width, Cell height, margins, and columns.'),
      'auto'           => $this->t("If unchecked, gridstack will not initialize existing items, means broken."),
      'cellHeight'     => $this->t("One cell height. <strong>0</strong> means the library will not generate styles for rows. Everything must be defined in CSS files. <strong>auto (-1)</strong> means height will be calculated from cell width. Default 60. Be aware, auto has issues with responsive displays. Put <strong>-1</strong> if you want <strong>auto</strong> as this is an integer type."),
      'float'          => $this->t("Enable floating widgets. See <a href=':url'>float sample</a>. Default FALSE.", [':url' => $url2]),
      'minW'       => $this->t('If window width is less, grid will be shown in one-column mode, with added class: <strong>grid-stack-1</strong>. Recommended the same as or less than XS below, if provided. Default 768.'),
      'main_column'    => $this->t('The amount of columns. <strong>Important!</strong> This desktop column is overridden and ignored by LG/XL below if provided.'),
      'maxRow'         => $this->t("Maximum rows amount. Default is <strong>0</strong> which means no maximum rows."),
      'rtl'            => $this->t("If <strong>true</strong> turns grid to RTL. Possible values are <strong>true</strong>, <strong>false</strong>, <strong>auto</strong> -- default. See <a href=':url'>RTL</a>.", [':url' => $url3]),
      'margin' => $this->t("Vertical gap size. Default 20."),
      'noMargin'       => $this->t('If checked, be sure to put 0 for Vertical margin to avoid improper spaces.'),
      'breakpoints'    => $this->t('Responsive multi-breakpoint grids<small><span class="visible-js">XS is expected for disabled state defined by <strong>Min width</strong>.<br>The column will be updated at the given breakpoint. <br>Fooled not by incorrect aspect ratios. Trust the numbers in dark.</span><span class="visible-css"><strong>Note!</strong> If using static Bootstrap/ Foundation grid, this is a normal min-width, or mobile first layout. <br>Height is determined by the actual content. Here is just illustration to make nice icon.</span><br>If the layout is messed up for some reason, simply save and edit it back. It should re-arrange and correct the indices after saving. <br>Input relevant breakpoint value for the <b>Width</b> option below to make it editable, empty means disabled (non-editable).</small>'),
      'column'         => $this->t('The minimum column for this breakpoint. Try changing this if some grid/box is accidentally hidden to bring them back into the viewport. <br><strong>Important!</strong> This must be 12 if using static Bootstrap/ Foundation. Avoid odd number, else headaches. Column is the best way to make <b>native CSS Grid</b> responsive.'),
      'column_icon'    => $this->t('Once provided, this will override the <strong>Amount of columns</strong> option above. Update the <strong>Amount of columns</strong> to match this new value if confused.'),
      'lg'             => $this->t('<small><span class="visible-js">Grids are managed at the topmost settings. </span>This breakpoint is to generate icon. Best with total rows at maximum 16.</small>'),
      'sm'             => $this->t('<small>Grids are in one column mode here, disabled. Best height is 1x1 or 1x2 at most, else absurd.</small>'),
      'width'          => $this->t('For static Bootstrap/Foundation, it only affects this very admin page for preview. The actual breakpoint is already defined by their CSS. Adjust it for correct preview.<ul><li>For GridStack JS, the minimum value must be <code>&gt;= Min width</code> above.</li><li>EM to PX conversion is based on 16px base font-size:<br>Bootstrap 3:<br><code>SM >= 768px, MD >= 992px, LG >= 1200px</code><br>Bootstrap 4: <br><code>XS < 576px, SM >= 576px, MD >= 768px, LG >= 992px, XL >= 1200px</code><br>Foundation:<br><code>SM >= 0px, MD >= 641px (40.063em), LG >= 1025px (64.063em)</code></li></ul>Leave it empty to disable/ ignore this breakpoint. <br><strong>Tips:</strong> Temporarily increase this to 768+ if trouble to expand shrinked boxes. Input relevant breakpoint value to make it editable.'),
    ];
    return $description[$key] ?? NULL;
  }

  /**
   * Sets up the options form.
   */
  protected function optionsForm(array &$form, FormStateInterface $form_state) {
    $entity = $this->entity;

    // Satisfy phpstan.
    if (!($entity instanceof GridStackInterface)) {
      return;
    }

    $classes = ['details--settings', 'b-tooltip'];
    $form['options'] = [
      '#type'          => 'container',
      '#tree'          => TRUE,
      '#title'         => $this->t('Options'),
      '#title_display' => 'invisible',
      '#attributes'    => ['class' => $classes, 'id' => 'edit-options'],
      '#access'        => $this->gridStack()->id() == 'default' ? FALSE : TRUE,
    ];

    $form['options']['icon'] = [
      '#type' => 'hidden',
      '#default_value' => $this->options['icon'] ?? '',
      '#attributes' => [
        'id' => 'gridstack-icon',
        'data-url' => $entity->getIconUrl(TRUE),
      ],
    ];

    $class = ['form-item--options-use-framework', 'form-item--tooltip-bottom'];
    $form['options']['use_framework'] = [
      '#type'               => 'checkbox',
      '#title'              => $this->t('Use static <span>@framework</span>', ['@framework' => $this->framework]),
      '#default_value'      => $this->gridStack()->getOption('use_framework') ?? FALSE,
      '#description'        => $this->getDescription('use_framework'),
      '#wrapper_attributes' => [$class],
      '#disabled'           => $this->useNested,
    ];

    if ($this->adminCss) {
      $form['options']['use_framework']['#title_display'] = 'before';
    }
  }

  /**
   * Sets up the settings form.
   */
  protected function settingsForm(array &$form, FormStateInterface $form_state) {
    $states['visible'][':input[name*="[staticGrid]"]'] = ['checked' => FALSE];
    $form['options']['settings'] = [
      '#type'        => 'details',
      '#tree'        => TRUE,
      '#open'        => FALSE,
      '#title'       => $this->t('GridStack JS or Native Grid Settings'),
      '#description' => $this->getDescription('settings'),
      '#attributes'  => ['class' => ['form-wrapper--gridstack-settings']],
      '#access'      => !$this->useNested,
    ];

    $form['options']['settings']['auto'] = [
      '#type'        => 'checkbox',
      '#title'       => $this->t('Auto'),
      '#description' => $this->getDescription('auto'),
    ];

    $form['options']['settings']['cellHeight'] = [
      '#type'        => 'textfield',
      '#title'       => $this->t('Cell height'),
      '#description' => $this->getDescription('cellHeight'),
    ];

    $form['options']['settings']['float'] = [
      '#type'        => 'checkbox',
      '#title'       => $this->t('Float'),
      '#description' => $this->getDescription('float'),
      '#states'      => $states,
    ];

    $form['options']['settings']['minW'] = [
      '#type'         => 'textfield',
      '#title'        => $this->t('Min width'),
      '#field_suffix' => 'px',
      '#description'  => $this->getDescription('minW'),
    ];

    $form['options']['settings']['column'] = [
      '#type'         => 'select',
      '#title'        => $this->t('Amount of columns'),
      '#options'      => $this->getColumnOptions(),
      '#attributes'   => [
        'class'       => ['form-select--column'],
        'data-target' => '#gridstack-' . $this->iconBreakpoint,
      ],
      '#description'  => $this->getDescription('main_column'),
    ];

    $form['options']['settings']['maxRow'] = [
      '#type'         => 'textfield',
      '#title'        => $this->t('Maximum rows'),
      '#field_suffix' => 'px',
      '#description'  => $this->getDescription('maxRow'),
    ];

    $form['options']['settings']['rtl'] = [
      '#type'        => 'checkbox',
      '#title'       => $this->t('RTL'),
      '#description' => $this->getDescription('rtl'),
    ];

    $form['options']['settings']['margin'] = [
      '#type'         => 'textfield',
      '#title'        => $this->t('Margin'),
      '#field_suffix' => 'px',
      '#description'  => $this->getDescription('margin'),
    ];

    $form['options']['settings']['noMargin'] = [
      '#type'        => 'checkbox',
      '#title'       => $this->t('No horizontal margin'),
      '#description' => $this->getDescription('noMargin'),
    ];

    // @todo use <b>Revert</b> button to the last saved states, or
    $fg = $this->manager->moduleExists('field_group');
    $form['options']['breakpoints'] = [
      '#type'        => $fg ? 'container' : 'vertical_tabs',
      '#tree'        => TRUE,
      '#group'       => 'breakpoints',
      '#parents'     => ['breakpoints'],
      '#default_tab' => 'edit-options-breakpoints-' . $this->iconBreakpoint,
      '#prefix'      => $this->isVariant ? '' : '<h2 class="form__title form__title--responsive">' . $this->getDescription('breakpoints') . '</h2>',
      '#attributes'  => [
        'class' => ['vertical-tabs__edit-breakpoints'],
        'id' => 'edit-options-breakpoints',
      ],
    ];

    // Temp fix for Field Group, see #3395093.
    if ($fg) {
      foreach (['default_tab', 'group', 'parents'] as $key) {
        unset($form['options']['breakpoints']["#$key"]);
      }
    }

    foreach (['auto', 'cellHeight', 'float', 'minW'] as $key) {
      $form['options']['settings'][$key]['#wrapper_attributes']['class'][] = 'form-item--tooltip-bottom';
    }
  }

  /**
   * Sets up the preview form.
   */
  protected function previewForm(array &$form, FormStateInterface $form_state) {
    $nodes = [
      'autoPosition'     => TRUE,
      'maxH'             => 60,
      'minH'             => 1,
      'defaultMinWidth'  => 2,
      'defaultMinHeight' => 2,
      'rootMinWidth'     => 1,
      'nestedMinWidth'   => 2,
    ];

    $gridstack = $this->gridStack();
    foreach ($this->breakpointElements() as $column => $elements) {
      $storage        = 'edit-options-breakpoints-' . $column . '-grids';
      $nested_storage = 'edit-options-breakpoints-' . $column . '-nested';
      $columns        = $this->options['breakpoints'][$column] ?? [];

      // Fallbacks to source for variants.
      if ($this->isVariant && empty($columns) && $gridstack) {
        $breakpoints = $gridstack->getOptions('breakpoints');
        if (isset($breakpoints[$column])) {
          $columns = $breakpoints[$column];
        }
      }

      $active_column = $columns['column'] ?? 12;
      $active_width  = $columns['width'] ?? '';

      // $active_column === -1 ? 'auto' : $active_column;
      if ($this->useNested) {
        $active_column = 12;
      }

      // Details.
      $form[$column]['#type']    = $this->isDefault ? 'container' : 'details';
      $form[$column]['#tree']    = TRUE;
      $form[$column]['#open']    = TRUE;
      $form[$column]['#group']   = 'breakpoints';
      $form[$column]['#parents'] = ['options', 'breakpoints', $column];
      $form[$column]['#title']   = $column;

      // Prevents AJAX from adding unique ID, since we have limited known ones.
      $form[$column]['#attributes']['id'] = 'edit-options-breakpoints-' . $column;

      if ($column == $this->iconBreakpoint) {
        $form[$column]['#attributes']['class'][] = 'last';
      }

      // Settings.
      $this->jsSettings['breakpoint']       = $column;
      $this->jsSettings['display']          = 'responsive';
      $this->jsSettings['storage']          = $storage;
      $this->jsSettings['nested_storage']   = $nested_storage;
      $this->jsSettings['icon_breakpoint']  = $this->iconBreakpoint;
      $this->jsSettings['breakpoint_width'] = $active_width;

      // Fallback for totally empty before any string inserted.
      $json_column_grids = empty($columns['grids']) ? Json::encode($this->grids) : $columns['grids'];
      $column_grids      = is_string($json_column_grids) ? Json::decode($json_column_grids) : $json_column_grids;
      $nested_check      = array_filter($this->nestedGrids);
      $nested_grids_end  = empty($nested_check) ? '' : Json::encode($this->nestedGrids);
      $json_nested_grids = empty($columns['nested']) ? $nested_grids_end : $columns['nested'];

      // Fallback for not so empty when json grids deleted leaving to string.
      if (empty($column_grids)) {
        $json_column_grids = Json::encode($this->grids);
        $column_grids = $this->grids;
      }

      // Preview.
      $preview = $common = [];
      $disabled = FALSE;
      $preview_description = '';
      $config_js = $this->gridStack()->getOptions('settings') ?: $this->default->getOptions('settings');
      $config_css = $this->getNestedSettings();

      // Auto is not bad at preview, provide the exact cellHeight instead.
      if (isset($config_js['cellHeight']) && $config_js['cellHeight'] == -1) {
        $config_js['cellHeight'] = 60;
      }

      // Beware to keep defaults, the library appears to hard-code it regardless
      // the options at some places like nested. Meaning customs are ignored.
      // Do not add spaces to itemClass, it wants a single class.
      $common['breakpoint'] = $column;
      $common['itemClass']  = 'box';
      $common['handle']     = '.box__content';
      $common['column']     = $active_column;

      if ($column == $this->iconBreakpoint) {
        $preview_description = $this->getDescription('lg');
      }

      if ($column == 'xs' && !$this->useNested) {
        $disabled = TRUE;
        $preview_description = $this->getDescription('sm');
      }

      if ($preview_description && !$this->isVariant) {
        $preview['#prefix'] = '<h3 class="form__title form__title--preview form-item--lead form-item--fullwidth">' . $preview_description . '</h3>';
      }
      // Handles n,ne,e,se,s,sw,w,nw.
      if ($disabled || $column == $this->smallestBreakpoint) {
        $config_js['resizable'] = $config_css['resizable'] = (object) ['handles' => 's'];

        if ($this->useNested) {
          $config_css['resizable'] = (object) ['handles' => 'e, se, s, sw, w'];
        }
        else {
          $config_js['column'] = 1;
        }
      }
      else {
        $config_js['resizable'] = $config_css['resizable'] = (object) ['handles' => 'e, se, s, sw, w'];
      }

      $config_nested = array_merge($config_css, ['margin' => 4]);

      $preview['#theme'] = 'gridstack_ui_admin';
      $preview['#items'] = [];
      $preview['#settings'] = $this->jsSettings;
      $preview['#weight'] = -10;
      $preview['#content_attributes'] = [
        'data-breakpoint'       => $column,
        'data-config'           => Json::encode(array_merge($common, $config_js)),
        'data-config-nested'    => Json::encode(array_merge($common, $config_nested)),
        'data-is-nested'        => $this->useNested ? 1 : 0,
        'data-preview-grids'    => $this->getNodes($json_column_grids),
        'data-nested-grids'     => $this->getNodesNested($json_column_grids, $json_nested_grids),
        'data-storage'          => $storage,
        'data-nested-storage'   => $nested_storage,
        'data-gs-column'        => $disabled ? 1 : $active_column,
        'data-gs-node-config'   => Json::encode($nodes),
        'data-responsive-width' => $active_width,
      ];

      $classes = [
        'gridstack--root',
        'gridstack--' . $column,
        'is-gs-enabled',
        $column == $this->iconBreakpoint ? 'gridstack--main' : 'gridstack--sub',
      ];

      if ($column == $this->smallestBreakpoint) {
        $classes[] = 'gridstack--smallest';
      }

      if ($this->isVariant) {
        $classes[] = 'gridstack--variant';
      }

      $preview['#content_attributes']['class'] = array_merge(['gridstack'], $classes);

      // Build preview.
      $form[$column]['preview'] = $preview;

      // Breakpoint elements.
      foreach ($elements as $key => $element) {
        if ($key == 'breakpoint') {
          continue;
        }

        // Elements.
        $form[$column][$key] = $element;
        $form[$column][$key]['#title_display'] = 'before';
        $form[$column][$key]['#wrapper_attributes']['class'][] = 'form-item--' . str_replace('_', '-', $key);
        $form[$column]['breakpoint']['#wrapper_attributes']['class'][] = 'visually-hidden';

        // Overrides base.
        $_widths = &$form[$column]['width'];
        $_widths['#weight'] = 10;
        $_widths['#title'] = $this->t('@breakpoint width', ['@breakpoint' => $column]);
        $_widths['#field_suffix'] = 'px';
        $_widths['#attributes']['data-target'] = '#gridstack-' . $column;
        $_widths['#description'] = $this->getDescription('width');

        $form[$column][$key]['#default_value'] = $columns[$key] ?? '';

        $form[$column]['column'] = [
          '#type'          => 'select',
          '#title'         => $this->t('Column'),
          '#options'       => $this->getColumnOptions(),
          '#empty_option'  => $this->t('- None -'),
          '#default_value' => $active_column ?: '',
          '#weight'        => 11,
          '#attributes'    => [
            'class'       => ['form-select--column'],
            'data-target' => '#gridstack-' . $column,
          ],
          '#description'        => $this->getDescription('column'),
          '#wrapper_attributes' => ['class' => ['form-item--column']],
          '#access'             => !$this->useNested,
          '#disabled'           => $this->isVariant,
        ];

        if ($column == 'xs' && !$this->useNested) {
          $form[$column]['column']['#type'] = 'hidden';
          $form[$column]['column']['#default_value'] = 1;
        }

        if ($column == $this->iconBreakpoint) {
          $form[$column]['column']['#description'] .= ' ' . $this->getDescription('column_icon');
        }

        // The actual grid elements which store the settings.
        $form[$column]['grids'] = [
          '#type'          => 'hidden',
          '#default_value' => $this->getNodes($json_column_grids) ?: '',
          '#weight'        => 20,
          '#wrapper_attributes' => ['class' => ['visually-hidden']],
        ];

        $form[$column]['nested'] = [
          '#type'          => 'hidden',
          '#default_value' => empty($columns['nested'])
            ? '' : ($this->getNodesNested($json_column_grids, $columns['nested']) ?: ''),
          '#weight'        => 20,
          '#wrapper_attributes' => ['class' => ['visually-hidden']],
        ];

        // Hard-code the default to avoid changes.
        if ($this->isDefault) {
          $form[$column][$key]['#wrapper_attributes'] = ['class' => ['visually-hidden']];
          $form[$column][$key]['#description']        = '';
          $form[$column]['column']['#type']           = 'hidden';
          $form[$column]['column']['#default_value']  = 12;
          $form[$column]['width']['#default_value']   = 1200;
        }

        if ($this->isVariant) {
          $form[$column]['column']['#wrapper_attributes'] = ['class' => ['visually-hidden']];
          $form[$column]['width']['#wrapper_attributes'] = ['class' => ['visually-hidden']];
        }
      }
    }
  }

}
