<?php

namespace Drupal\gridstack_ui\Form;

use Drupal\Core\Url;
use Drupal\blazy\Form\BlazyDeleteFormBase;

/**
 * Builds the form to delete a GridStack optionset.
 */
class GridStackDeleteForm extends BlazyDeleteFormBase {

  /**
   * Defines the nice anme.
   *
   * @var string
   */
  protected static $niceName = 'GridStack';

  /**
   * Defines machine name.
   *
   * @var string
   */
  protected static $machineName = 'gridstack';

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.gridstack.collection');
  }

}
