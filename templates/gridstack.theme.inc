<?php

/**
 * @file
 * Hooks and preprocess functions for the GridStack module.
 */

use Drupal\Core\Template\Attribute;
use Drupal\gridstack\GridStackDefault;

/**
 * Prepares variables for gridstack.html.twig templates.
 */
function template_preprocess_gridstack(array &$variables) {
  $element = $variables['element'];
  foreach (GridStackDefault::themeProperties() as $key => $default) {
    $variables[$key] = $element['#' . $key] ?? $default;
  }

  $settings = &$variables['settings'];
  gridstack()->verifySafely($settings);

  $attributes                      = &$variables['attributes'];
  $custom_classes                  = empty($attributes['class']) ? [] : $attributes['class'];
  $attributes['class']             = array_merge(['gridstack'], $custom_classes);
  $variables['wrapper']            = empty($settings['wrapper']) ? 'div' : $settings['wrapper'];
  $variables['wrapper_attributes'] = new Attribute($variables['wrapper_attributes']);
  $variables['content_attributes'] = empty($variables['content_attributes']) ? [] : new Attribute($variables['content_attributes']);
  $variables['blazies']            = $settings['blazies']->storage();
  $variables['config']             = $settings['gridstacks']->storage();
}

/**
 * Prepares variables for gridstack-box.html.twig templates.
 */
function template_preprocess_gridstack_box(&$variables) {
  // Content attributes are required by Layout Builder to make Sortable work.
  // Unlike jQuery.UI, Sortable doesn't work with outer wrapper.
  foreach (GridStackDefault::itemProperties() as $key => $default) {
    $variables[$key] = $variables['element']["#$key"] ?? $default;
  }

  $item       = &$variables['item'];
  $settings   = &$variables['settings'];
  $attrs      = &$variables['attributes'];
  $cn_attrs   = &$variables['content_attributes'];
  $config     = $settings['gridstacks'];
  $nested_id  = $config->get('nested.id');
  $root_delta = $config->get('root.delta');
  $use_inner  = $config->use('inner');
  $use_fw     = $config->use('framework');

  // Boxes may have captions.
  foreach (['alt', 'category', 'data', 'link', 'title'] as $key) {
    $item['caption'][$key] = $item['caption'][$key] ?? [];
  }

  $item['box']      = $item['box'] ?? [];
  $item['caption']  = array_filter($item['caption']);
  $item['preface']  = $item['preface'] ?? [];
  $has_content      = !empty($item['box']) || !empty($item['caption']) || !empty($item['preface']);
  $settings['type'] = $type = $config->get('box.type') ?: ($settings['type'] ?? '');
  $is_rich_stamp    = $item['box'] && $type && in_array($type, ['rich', 'stamp']);

  // Provides debug attributes if so configured.
  if ($config->is('ipe') || $config->get('ui.debug')) {
    $has_content = TRUE;
    $delta = $root_delta ?: $variables['delta'];
    $delta = (int) $delta + 1;
    if ($nested_id) {
      $delta = str_replace('-', ':', $nested_id);
    }
    $attrs['data-gs-delta'] = $delta;

    if ($region = $config->get('region.name')) {
      $attrs['data-gs-region-name'] = $region;
    }
  }

  // Provides inner DIV if so required (.row.gridstack--nested > DIV).
  // When using CSS frameworks to have row + inner divities display correctly.
  // Not needed by JS engines, just CSS engines.
  $check = $use_fw ? $use_inner && $has_content : $use_inner;
  $config->set('use.inner', $check);

  // Provides optional classes if so configured.
  $classes = (array) ($attrs['class'] ?? []);
  $index = $variables['delta'] + 1;

  if (!$config->get('ui.no_classes')) {
    if ($item['box']) {
      if (!empty($settings['background'])) {
        $classes[] = 'box--background';
      }
      if ($type) {
        $classes[] = 'box--' . str_replace('_', '-', $type);
      }
    }

    if ($nested_id) {
      $classes[] = 'box--nested--' . $index;
    }
  }
  else {
    // Required by JS to work to fix aspect ratio.
    if ($is_rich_stamp) {
      $classes[] = 'box--' . str_replace('_', '-', $type);
    }

    // Removed classes if so configured.
    $removed = ['box--nester', 'box--nested'];
    $classes = array_diff($classes, $removed);
  }

  // Required by option dynamic custom BG color and Text color.
  $classes[] = empty($nested_id) ? 'box--' . $index : 'box--' . $nested_id;

  // Prevents collapsing container without media/ images.
  // Re-uses the same .is-gs-ratio class for ungridstack, not needed by native.
  if ($config->use('js')) {
    $cn_attrs['class'][] = 'grid-stack-item-content';
    $caption_only = empty($item['box']) && !empty($item['caption']);
    if ($caption_only || $config->is('ungridstack')) {
      // The .is-gs-ratio is to disable Blazy aspect ratio for inhouse solution.
      // Basically replacing padding-hack with exact height if applicable.
      $classes[] = 'is-gs-ratio';
    }
  }

  // The main box container attributes for consistency.
  $attrs['class'] = array_merge(['gridstack__box', 'box'], $classes);

  // Content attributes are required for Layout Builder draggable to work.
  $classes = (array) ($cn_attrs['class'] ?? []);
  $cn_attrs['class'] = array_merge(['box__content'], $classes);
  $variables['caption_attributes'] = new Attribute($variables['caption_attributes']);

  // Provides extra variables.
  $empty = $has_content ? FALSE : TRUE;
  $config->set('is.empty', $empty);

  $variables['wrapper'] = empty($settings['wrapper']) ? 'div' : $settings['wrapper'];

  if ($is_rich_stamp) {
    $item['box']['#theme_wrappers'][] = 'container';
    $item['box']['#attributes']['class'][] = 'box__' . $type;
    $item['box']['#attributes']['class'][] = 'is-gs-fh';
  }

  $variables['blazies'] = $settings['blazies']->storage();
  $variables['config']  = $config->storage();
}
